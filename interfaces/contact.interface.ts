export interface formContact {
  name?: string;
  email?: string;
  content?: string;
  displayOrder?: number;
  phoneNumber?: any;
}
