export interface formInternational {
  note?: any;
  abandonPackage?: any;
  conditionPackage?: any;
  holidaySchedule?: any;
  maximumQuantity?: any;
  maximumSize?: any;
}
