export interface EstimateCharge {
  countryCode: string;
  fromProvince: string;
  fromDistrict: string;
  toProvince: string;
  toDistrict: string;
  weight: string;
  totalAmount: any;
  isType: string;
  language: number;
}

export interface Portal {
  info: any;
  status: any;
  delivery: any;
  deliveryList: any;
  error: any;
  visible: boolean;
}

interface Country {
  country?: string;
  provinceCode?: string;
  communeCode?: string;
  districtCode?: string;
}

export interface FormState {
  values: Country;
}

interface CountryInfo {
  countryCode: string;
  countryName: string;
}

interface Package {
  abandonPackage?: any[];
  conditionPackage?: any[];
  note?: any[];
  holidaySchedule?: any[];
}

export interface List {
  countries: CountryInfo[];
  packageInfo: Package;
  visible?: boolean;
  countryCode?: string;
}

export interface AppTypes {
  isShowHeaderSideBar: boolean;
}

export interface getPosition {
  lat: any;
  lng: any;
}
