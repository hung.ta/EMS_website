import { useEffect, useState } from "react";
import {
  Accordion,
  Breadcrumb,
  Col,
  Container,
  Row,
  NavLink,
  Spinner,
} from "react-bootstrap";

import IconInternational from "public/img/icon/ic-international.png";
import { withTranslation } from "i18n";
import "styles/OrderSearch.module.css";
import { BreadCrumb, ContextAwareToggle, SeoMeta } from "components";
import { isEmpty, get } from "lodash";
import { Select } from "antd";
import {
  getInternationalPackage,
  getAllCountryList,
} from "services/look-up.service";
import { formInternational } from "interfaces/international.interface";
import { FormState, List } from "interfaces/look-up.interface";
import { getCookieByName, getJsonLodash } from "utils/helper";

const { Option } = Select;

const initFormValue: formInternational = {
  note: "",
  abandonPackage: "",
  conditionPackage: "",
  holidaySchedule: "",
  maximumQuantity: "",
  maximumSize: "",
};

const initList: List = {
  countries: [],
  packageInfo: {},
  visible: false,
  countryCode: "",
};

const initFormState: FormState = {
  values: {},
};

const International: any = ({ t, langId }) => {
  const [currentKeyActive, setCurrentKeyActive] = useState<any>(1000);
  const [isSpinner, setIsSpinner] = useState<boolean>(false);
  const [formState, setFormState] = useState<FormState>(initFormState);
  const [list, setList] = useState<List>(initList);
  const [isEnable, setIsEnable] = useState<boolean>(false);
  const [internationalPackage, setInternationalPackage] = useState<
    formInternational
  >(initFormValue);

  const handleOnChange = (value: string) => {
    setFormState({
      ...formState,
      values: {
        country: value,
      },
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    if (get(formState, "values.country")) {
      handleSearch();
    }
  }, [langId]);

  // handle search
  const handleSearch = async () => {
    try {
      setIsSpinner(true);
      const { values } = formState;
      const result = await getInternationalPackage(values.country, langId);
      setIsSpinner(false);
      const { data } = result.data.response;
      const {
        holidaySchedule,
        countryCode,
        abandonPackage,
        note,
        maximumQuantity,
        maximumSize,
        conditionPackage,
      } = data[0];
      setList({
        ...list,
        visible: true,
        countryCode,
        packageInfo: {
          holidaySchedule,
        },
      });
      setInternationalPackage({
        ...internationalPackage,
        abandonPackage: abandonPackage,
        holidaySchedule: holidaySchedule,
        note: note,
        maximumQuantity: maximumQuantity,
        maximumSize: maximumSize,
        conditionPackage: conditionPackage,
      });

      setTimeout(() => {
        let tables = Array.from(document.querySelectorAll("table"));
        tables.forEach((table) => {
          table.outerHTML =
            '<div class="wr_table" >' + table.outerHTML + "</div>";
        });
      }, 20);

      setCurrentKeyActive("0");
    } catch (error) {
      setIsSpinner(false);
    }
  };

  // get country
  const handleGetCountryList = async () => {
    const result = await getAllCountryList();
    if (result) {
      setList({
        ...list,
        countries: result.data.response.data,
      });
    }
  };

  useEffect(() => {
    setCurrentKeyActive("1000");
    handleGetCountryList();
  }, []);

  useEffect(() => {
    if (!isEmpty(formState.values)) {
      if (isEmpty(formState.values.country)) {
        setIsEnable(false);
      } else {
        setIsEnable(true);
      }
    }
  }, [formState]);

  // handle change key active
  const handleChangeCurrentKeyActive = (activeKey: any) => (e: any) => {
    if (activeKey == currentKeyActive) {
      setCurrentKeyActive("1000");
    } else {
      setCurrentKeyActive(activeKey);
    }
  };

  return (
    <div className="order-search international">
      <SeoMeta title={t("international-seo-text")} />
      <BreadCrumb>
        <Breadcrumb.Item active>
          <span>{t("international-shipping-information")}</span>
        </Breadcrumb.Item>
      </BreadCrumb>
      <Container>
        <div className="search-holiday">
          <div className="search-holiday-text d-flex align-items-center">
            <img src={IconInternational} alt="" />
            <div>{t("export-conditions")}</div>
          </div>
          <div className="search-holiday-action">
            <Row>
              <Col sm={9}>
                <Select
                  virtual={false}
                  showSearch
                  style={{ width: "100%" }}
                  placeholder={t("choose-country")}
                  optionFilterProp="children"
                  onChange={handleOnChange}
                  value={formState.values.country}
                  filterOption={(input, option) =>
                    option.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  <Option value="">{t("choose-country")}</Option>
                  {list.countries.map((country: any) => {
                    return (
                      <Option
                        key={country.countryCode}
                        value={country.countryCode}
                      >
                        {country.countryName}
                      </Option>
                    );
                  })}
                </Select>
              </Col>
              <Col sm={3}>
                <button
                  onClick={handleSearch}
                  disabled={isEnable ? false : true}
                  className="d-flex justify-content-center align-items-center"
                >
                  {t("text-search")}
                  {isSpinner && <Spinner animation="border" className="ml-3" />}
                </button>
              </Col>
            </Row>
          </div>
          <div className="general-rules">
            <NavLink
              href="/quy-dinh-chung-ve-hang-cam-gui"
              target="_blank"
              style={{ display: "inline", padding: 0 }}
            >
              {t("general-provisions")}
            </NavLink>
          </div>
        </div>

        <div className="content-international">
          <Accordion activeKey={currentKeyActive}>
            <div>
              <ContextAwareToggle
                eventKey="0"
                callback={handleChangeCurrentKeyActive("0")}
              >
                {t("goods-banned-from-sending")}
              </ContextAwareToggle>
              <Accordion.Collapse eventKey="0">
                <div
                  className="content-inline"
                  dangerouslySetInnerHTML={{
                    __html: internationalPackage.abandonPackage,
                  }}
                ></div>
              </Accordion.Collapse>
            </div>
            <div>
              <ContextAwareToggle
                eventKey="1"
                callback={handleChangeCurrentKeyActive("1")}
              >
                {t("conditional-shipping")}
              </ContextAwareToggle>
              <Accordion.Collapse eventKey="1">
                <div
                  className="content-inline"
                  dangerouslySetInnerHTML={{
                    __html: internationalPackage.conditionPackage,
                  }}
                ></div>
              </Accordion.Collapse>
            </div>
            <div>
              <ContextAwareToggle
                eventKey="2"
                callback={handleChangeCurrentKeyActive("2")}
              >
                {t("note")}
              </ContextAwareToggle>
              <Accordion.Collapse eventKey="2">
                <div
                  className="content-inline"
                  dangerouslySetInnerHTML={{
                    __html: internationalPackage.note,
                  }}
                ></div>
              </Accordion.Collapse>
            </div>

            <div>
              <ContextAwareToggle
                eventKey="3"
                callback={handleChangeCurrentKeyActive("3")}
              >
                {t("maximum-size-and-weight")}
              </ContextAwareToggle>
              <Accordion.Collapse eventKey="3">
                <div
                  className="content-inline"
                  dangerouslySetInnerHTML={{
                    __html: internationalPackage.maximumSize,
                  }}
                ></div>
              </Accordion.Collapse>
            </div>

            <div>
              <ContextAwareToggle
                eventKey="4"
                callback={handleChangeCurrentKeyActive("4")}
              >
                {t("holiday-schedule")}
              </ContextAwareToggle>
              <Accordion.Collapse eventKey="4">
                <div
                  className="content-inline"
                  dangerouslySetInnerHTML={{
                    __html: internationalPackage.holidaySchedule,
                  }}
                ></div>
              </Accordion.Collapse>
            </div>
          </Accordion>
        </div>
      </Container>
    </div>
  );
};
export async function getServerSideProps(context: any) {
  const cookie = get(context, "req.headers.cookie", "");
  const langConvert = getCookieByName("langConvert", cookie);

  const languageI8 = get(context, "req.language", "vn");
  const langId = get(getJsonLodash(langConvert), `${languageI8}`, 1);

  return {
    props: { langId: langId },
  };
}

export default withTranslation()(International);
