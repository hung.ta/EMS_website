import React, { useEffect, useState } from "react";
import { notification } from "antd";
import { withTranslation } from "./../../i18n";
import "./DangKyThongTin.module.css";
import SeoMeta from "./../../components/SeoMeta";
import { useRouter } from "next/router";
import { useAppContext } from "contexts/app";
import _ from "lodash";
import { registerInformation } from "services/register.service";

const DangKyThongTin: any = ({ t }) => {
  const appContext = useAppContext();
  const router = useRouter();
  const [phone, setPhone] = useState("");
  const [province, setProvince] = useState("");
  const [orderPerMonth, setOrderPerMonth] = useState("");
  const handleSubmit = async (e) => {
    e.preventDefault();
    if (phone === "") {
      notification.error({
        message: t("noti_empty_phone"),
        placement: "bottomLeft",
      });
      return;
    } else if (province === "") {
      notification.error({
        message: t("noti_empty_province"),
        placement: "bottomLeft",
      });
      return;
    } else if (orderPerMonth === "") {
      notification.error({
        message: t("noti_empty_order_per_month"),
        placement: "bottomLeft",
      });
      return;
    }
    try {
      const resp = await registerInformation({
        Phone: phone,
        Province: province,
        Count: orderPerMonth,
      });
      if (_.get(resp, "data.Code") === "00") {
        notification.success({
          message: t("success"),
          description: t("noti_register_info_success"),
          placement: "bottomLeft",
        });
        setPhone("");
        setProvince("");
        setOrderPerMonth("");
        router.push("/");
      } else {
        notification.error({
          description:
            _.get(resp, "data.Message") ?? t("noti_register_info_error"),
          message: t("failed"),
          placement: "bottomLeft",
        });
      }
    } catch (err) {
      notification.error({
        description: t("noti_register_info_error"),
        message: t("failed"),
        placement: "bottomLeft",
      });
    }
  };

  useEffect(() => {
    appContext.setIsShowPopupRegister(false);
  }, []);

  return (
    <div className="wr_info_register">
      <SeoMeta title={`EMS | ${t("register_of_information")}`} />
      <div className="wr_info_register_form">
        <form onSubmit={handleSubmit} action="">
          <h1>{t("register_of_information")}</h1>
          <p>{t("please_fill_in")}</p>
          <input
            type="text"
            name="input_inforegister_phone"
            value={phone}
            onChange={(e) => {
              let regPhone = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;
              if (e.target.value === "" || regPhone.test(e.target.value)) {
                setPhone(e.target.value);
              }
            }}
            id="input_inforegister_phone"
            placeholder={t("phone_number")}
          />

          <input
            value={province}
            onChange={(e) => setProvince(e.target.value)}
            type="text"
            name="input_inforegister_province"
            id="input_inforegister_province"
            placeholder={t("province_send_order")}
          />

          <input
            value={orderPerMonth}
            onChange={(e) => {
              let userType = e.target.value[e.target.value.length - 1];
              let arr = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
              if (arr.includes(userType) || e.target.value.length === 0) {
                setOrderPerMonth(e.target.value);
              }
            }}
            type="text"
            name="input_inforegister_order_per_month"
            id="input_inforegister_order_per_month"
            placeholder={t("order_per_month")}
          />

          <button className="btn_submit_info_register" type="submit">
            Đăng ký ngay
          </button>
        </form>
      </div>
    </div>
  );
};

DangKyThongTin.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});
export default withTranslation("common")(DangKyThongTin);
