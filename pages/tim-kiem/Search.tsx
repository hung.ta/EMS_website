import React, { Fragment, useEffect } from "react";
import { NextSeo } from "next-seo";
import { Container, Breadcrumb } from "react-bootstrap";
import { withTranslation } from "./../../i18n";

import "./Search.module.css";
import { ApiGoogle, BreadCrumb } from "./../../components";
import SeoMeta from "./../../components/SeoMeta";

const Search: any = ({ t }) => {
  useEffect(() => {
    window.scrollTo(0, 0);
   }, [])
  
  return (
    <Fragment>
      <SeoMeta
        title={`EMS | ${t("search-title-text")}`}
        description={t("search-title-text")}
      />
      <NextSeo
        title={`EMS | ${t("search-title-text")}`}
        description={t("search-description-text")}
      />
      <BreadCrumb>
          <Breadcrumb.Item active>
            <span>{t('search-title-text')}</span>
          </Breadcrumb.Item>
        </BreadCrumb>
      <Container>
        <div className="page-search">
          <div className="gcse-search"></div>
          <div className="gcse-searchresults-only"></div>
        </div>
      </Container>
    </Fragment>
  );
};

Search.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});
export default withTranslation()(ApiGoogle(Search));
