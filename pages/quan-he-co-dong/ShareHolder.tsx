import { BreadCrumb, ContextAwareToggle, SeoMeta } from "components";
import LearnMore from "components/About-us/Learn-more/LearnMore";
import Link from "next/link";
import React, { Fragment, useEffect, useState } from "react";
import {
  Accordion,
  Breadcrumb,
  Col,
  Container,
  Form,
  Row,
} from "react-bootstrap";
import {
  rangeYear,
  removeVietnameseTones,
  getLangIdByName,
} from "utils/helper";
import img1 from "../../public/img/history/imglearn-more1.png";
import img2 from "../../public/img/history/imglearn-more2.png";
import img4 from "../../public/img/history/imglearn-more4.png";
import "./ShareHolder.module.css";
import moment from "moment";
import { Select } from "antd";
import { useAppContext } from "../../contexts/app";
import { baseURL } from "configs";
import _, { get } from "lodash";
import { withTranslation } from "i18n";
import {
  getAllShareHolder,
  checkFileShareHolder,
} from "services/share-holder.service";
const { Option } = Select;

function ShareHolder({ t, i18n, i18n: { language } }) {
  const appContext = useAppContext();
  const minYear = moment().subtract(10, "years").year();
  const maxYear = moment().year();
  const [year, setYear] = useState(`${maxYear}`);

  // client fetching
  const langId = getLangIdByName(appContext, i18n);
  const { data: shareHoldersData } = getAllShareHolder(langId, year);
  const shareHolders = get(shareHoldersData, "data.response", []);

  const onChangeYear = (value: any) => {
    setYear(value);
  };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const accordionProps = {
    shareHolders,
    t,
    year,
    language,
  };

  return (
    <div className="wr-shareholder">
      <SeoMeta title={`EMS | ${t("shareholder-relations")}`} />
      <BreadCrumb>
        <Breadcrumb.Item active>
          <span>{t("shareholder-relations")}</span>
        </Breadcrumb.Item>
      </BreadCrumb>
      <Container>
        <div className="content-share-holder">
          <Row>
            <Col sm={12}>
              <div className="show-year d-flex align-items-center">
                <div className="show-title">{t("displayed-by-year")}</div>
                <Form.Group className="choose-year">
                  <Select
                    virtual={false}
                    style={{ width: "100%" }}
                    placeholder={t("select-year")}
                    onChange={onChangeYear}
                    value={year}
                  >
                    <Option value="">{t("select-year")}</Option>
                    {rangeYear(minYear, maxYear).map((year, i) => {
                      return (
                        <Option key={i} value={`${year}`}>
                          {year}
                        </Option>
                      );
                    })}
                  </Select>
                </Form.Group>
              </div>
            </Col>
          </Row>
          <Row className="list-share-holder">
            <Col sm={12}>
              <AccordionComponent {...accordionProps} />
            </Col>
          </Row>

          <LearnMore>
            <Col sm={12} md={6} xl={4} className="block-redirect">
              <Link
                as="/lich-su-hinh-thanh"
                href="/[...pages]"
                // shallow
              >
                <a>
                  <div className="img-top">
                    <img src={img1} alt="" />
                  </div>
                  <div className="name-lean-more">{t("history-formation")}</div>
                  <div className="lean-more-redirect">{t("see-more")}</div>
                </a>
              </Link>
            </Col>
            <Col sm={12} md={6} xl={4} className="block-redirect">
              <Link
                as="/co-cau-to-chuc"
                href="/[...pages]"
                // shallow
              >
                <a>
                  <div className="img-top">
                    <img src={img2} alt="" />
                  </div>
                  <div className="name-lean-more">
                    {t("organizational-structure")}
                  </div>
                  <div className="lean-more-redirect">{t("see-more")}</div>
                </a>
              </Link>
            </Col>

            <Col sm={12} md={6} xl={4} className="block-redirect">
              <Link
                as="/tam-nhin-su-menh"
                href="/[...pages]"
                // shallow
              >
                <a>
                  <div className="img-top">
                    <img src={img4} alt="" />
                  </div>
                  <div className="name-lean-more">{t("vision-mission")}</div>
                  <div className="lean-more-redirect">{t("see-more")}</div>
                </a>
              </Link>
            </Col>
          </LearnMore>
        </div>
      </Container>
    </div>
  );
}

const AccordionComponent = ({ shareHolders, t, year, language }) => {
  const [currentKeyActive, setCurrentKeyActive] = useState(null);

  const handleClickAccordion = (name: any) => {
    if (currentKeyActive === name) {
      setCurrentKeyActive(100);
    } else {
      setCurrentKeyActive(name);
    }
  };

  useEffect(() => {
    const filteredShareHolders = _.filter(
      shareHolders,
      (holder) => !_.isEmpty(holder?.posts)
    );
    const initialKey = removeVietnameseTones(
      get(filteredShareHolders, "[0].name")
    );
    setCurrentKeyActive(initialKey);
  }, [year, get(shareHolders, "[0].name"), language]);

  return (
    <Fragment>
      {_.map(shareHolders, (shareHolder: any, i) => {
        const keyShareHolder = removeVietnameseTones(
          _.get(shareHolder, "name", "")
        );
        return (
          <Accordion activeKey={currentKeyActive} key={i}>
            {shareHolder.posts && shareHolder.posts.length > 0 ? (
              <>
                <ContextAwareToggle
                  callback={() => handleClickAccordion(keyShareHolder)}
                  eventKey={keyShareHolder}
                >
                  {shareHolder.name}
                </ContextAwareToggle>
                <Accordion.Collapse eventKey={keyShareHolder}>
                  <Row>
                    {shareHolder.posts.map((post: any, index: number) => {
                      return (
                        post.postDetail && (
                          <ShareHolderItem
                            post={post}
                            t={t}
                            key={index}
                            year={year}
                            language={language}
                          />
                        )
                      );
                    })}
                  </Row>
                </Accordion.Collapse>
              </>
            ) : (
              ""
            )}
          </Accordion>
        );
      })}
    </Fragment>
  );
};
const ShareHolderItem = ({ post, t, year, language }) => {
  const name = _.get(post, "postDetail.name", "");
  const urlDocumentation = _.toString(
    _.get(post, "postDetail.documentation", "")
  );
  const fileName = _.last(_.split(urlDocumentation, "/"));
  const fullUrlDocument = `${baseURL}${urlDocumentation}`;

  const { data: status } = checkFileShareHolder(
    () => (urlDocumentation ? fullUrlDocument : null),
    async (url) => {
      const res = await fetch(url, { method: "HEAD" });
      return res.status;
    }
  );
  const isShow = status !== 404;

  return (
    <Col sm={12} key={post.id}>
      <div className="item-share-holder d-flex justify-content-between align-items-center">
        <div className="name-item">{name}</div>
        {isShow && (
          <a
            href={fullUrlDocument}
            className="item-share-link"
            download={fileName}
            target="_blank"
          >
            {t("shareholder-download-text")}
          </a>
        )}
        <a
          href={fullUrlDocument}
          target="_blank"
          className="item-share-link-phone"
          download={fileName}
          style={{
            color: isShow ? "#f7941e" : "#313131",
            textDecoration: isShow ? "underline" : "none",
            pointerEvents: isShow ? "auto" : "none",
          }}
        >
          {name}
        </a>
      </div>
    </Col>
  );
};

export default withTranslation()(ShareHolder);
