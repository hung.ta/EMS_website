import { Fragment, useEffect, useState } from "react";
import moment from "moment";
import App from "next/app";
import { useRouter } from "next/router";
import "antd/dist/antd.css";
import { set, get, find, toString, isEmpty } from "lodash";
import "bootstrap/dist/css/bootstrap.min.css";
import "styles/globals.css";

import { getCookieByName } from "./../utils/helper";
import {
  getAllLanguages,
  getCompany,
  getMenuById,
  swrGetConfiguration,
} from "../services/app.services";
import { appWithTranslation } from "../i18n";
import { AppProvider } from "./../contexts/app";
import loadable from "@loadable/component";
import Head from "next/head";
import RequestPickupOfLanding from "../components/RequestPickupOfLanding/RequestPickupOfLanding";

const Fab = loadable(() => import("././../components/Fab/Fab"));

const PhoneNumber = loadable(
  () => import("././../components/PhoneNumber/PhoneNumber")
);
const ElectronicBillOfLanding = loadable(
  () =>
    import("././../components/ElectronicBillOfLanding/ElectronicBillOfLanding")
);
const BannerSub = loadable(
  () => import("././../components/BannerSub/BannerSub")
);
const Header = loadable(() => import("././../components/Header/Header"));
const Footer = loadable(() => import("././../components/Footer/Footer"));

let cacheData = {};

function MyApp({ Component, pageProps, dataResponse }) {
  const router = useRouter();

  const { data: connectEMSData } = swrGetConfiguration();
  dataResponse = {
    ...dataResponse,
    connectEMS: get(connectEMSData, "data.response.data", []),
  };

  // reset Mathematical Formulas
  useEffect(() => {
    const mathJax = (window as any).MathJax;
    mathJax && mathJax.Hub.Typeset();
  }, [dataResponse]);

  let isServiceDetail =
    get(pageProps, "dataProps.pageInfo.IsServicePost") === true;
  let recruitmentAllPage =
    get(pageProps, "dataProps.pageInfo.IsRecruitment") &&
    get(pageProps, "dataProps.pageInfo.CategoryChild") === true;
  let recruitmentPost =
    get(pageProps, "dataProps.pageInfo.IsPost") === true &&
    get(pageProps, "dataProps.pageInfo.Post.isRecruitment") === true;
  let newsAllPage =
    get(pageProps, "dataProps.pageInfo.IsNews") === true &&
    get(pageProps, "dataProps.pageInfo.CategoryChild") === true;

  let newsDetail =
    (get(pageProps, "dataProps") ?? {}).hasOwnProperty("relativePost") &&
    get(pageProps, "dataProps.pageInfo.IsPost") === true;

  let newsCategoryChild =
    get(pageProps, "dataProps.pageInfo.IsNews") &&
    get(pageProps, "dataProps.pageInfo.CategoryChild") === false &&
    (get(pageProps, "dataProps") ?? {}).hasOwnProperty("listPosts");

  let showBanner = true;
  let isHomePage = router.pathname === "/";
  let isRecruitment = recruitmentPost || recruitmentAllPage;
  let isNews = newsAllPage || newsDetail || newsCategoryChild;
  if (isHomePage || isRecruitment || isNews) {
    showBanner = false;
  }

  const [isMobile, setIsMobile] = useState(false);
  const handleWindowSizeChange = () => {
    if (window.innerWidth <= 768) {
      setIsMobile(true);
    } else {
      setIsMobile(false);
    }
  };
  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);

  return (
    <Fragment>
      <Head>
        <link
          rel="icon"
          href="https://ems.com.vn/favicon.ico"
          type="image/x-icon"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;0,700;1,400&display=swap"
          rel="stylesheet"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Roboto&display=swap"
          rel="stylesheet"
        />
        <link
          rel="stylesheet"
          type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"
        />
        <link
          rel="stylesheet"
          type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"
        />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0"
        />
        <meta charSet="utf-8" />
        <meta name="robots" content="index,follow,noodp" />
        <meta name="robots" content="noarchive" />
      </Head>

      <AppProvider data={dataResponse}>
        <Header />
        {showBanner && (
          <BannerSub isServiceDetail={isServiceDetail} pageProps={pageProps} />
        )}
        <Component {...pageProps} />
        <ElectronicBillOfLanding />
        <RequestPickupOfLanding />
        <PhoneNumber />
        <Fab isMobile={isMobile} />
        <Footer />
      </AppProvider>
    </Fragment>
  );
}

MyApp.getInitialProps = async (appContext: any) => {
  let languages = get(cacheData, "languages");
  if (isEmpty(languages)) {
    const resAllLanguages = await getAllLanguages();
    languages = get(resAllLanguages, "data.response", []);
    set(cacheData, "languages", languages);
  }

  // set cookie langConvert
  const isExistCookie = !!getCookieByName(
    "langConvert",
    get(appContext, "ctx.req.headers.cookie", "")
  );
  if (!isExistCookie) {
    let langCover = {};
    languages.map((item: any) => {
      langCover[get(item, "code", "").toLowerCase()] = get(item, "id");
    });
    appContext.ctx &&
      appContext.ctx.res &&
      appContext.ctx.res.setHeader(
        "Set-Cookie",
        `langConvert=${JSON.stringify(langCover)}`
      );
  }

  const langCode = toString(get(appContext, "ctx.req.language", "vn"));
  const foundedLang = find(languages, ["code", langCode.toUpperCase()]);
  const langId = get(foundedLang, "id", "1");

  let companyInfo = get(cacheData, `companyInfo-${langId}`, {});
  let menuInfo = get(cacheData, `menuInfo-${langId}`, []);
  if (langId) {
    if (!get(cacheData, "updatedAt")) {
      set(cacheData, `updatedAt`, moment());
    }
    let now = moment();
    let updatedTime = get(cacheData, "updatedAt", moment());
    let minuteCached = now.diff(updatedTime, "second");
    if (minuteCached > 15 || minuteCached <= 1) {
      // first time or over 15 seconds
      const companyInfoData = await getCompany(langId);
      companyInfo = get(companyInfoData, "data.response.data.[0]", {});
      const menuInfoData = await getMenuById(langId);
      menuInfo = get(menuInfoData, "data.response", []);
      set(cacheData, `companyInfo-${langId}`, companyInfo);
      set(cacheData, `menuInfo-${langId}`, menuInfo);
      set(cacheData, `updatedAt`, moment());
    }
  }

  const dataResponse = {
    languages,
    companyInfo,
    menuInfo,
  };

  return {
    ...(await App.getInitialProps(appContext)),
    dataResponse,
  };
};

export default appWithTranslation(MyApp);
