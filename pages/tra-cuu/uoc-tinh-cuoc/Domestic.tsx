import { Table } from "react-bootstrap";

const Domestic = ({
  t,
  list,
  showZone,
  convertDay,
  handleConvertNumber,
  convertService,
}) => {
  const { status, data } = list;

  return (
    <div className="estimate-charge-tab-table" id="estimate-charge-tab-table">
      <div className="estimate-charge-tab-table-title">
        {status ? (
          <h3>{t("EMS-collection")}</h3>
        ) : (
          <h3>{t("EMS-is-not-available-to-collect")}</h3>
        )}
      </div>

      <div>
        <Table responsive>
          <thead>
            <tr>
              <th>{t("service")}</th>
              <th>{t("into-money")}</th>
              <th>{t("total-time")}</th>
            </tr>
          </thead>
          <tbody>
            {Array.isArray(data) &&
              data.map((item: any, i: number) => {
                return (
                  <tr key={i}>
                    <td>
                      {status
                        ? item.Description
                        : convertService(item.Description, item.Type)}
                    </td>
                    <td>
                      {item.Rates === "0"
                        ? t("not-applicable")
                        : handleConvertNumber(item.Rates) + " " + "VND"}
                    </td>
                    <td>{convertDay(item.J, item.Type)}</td>
                  </tr>
                );
              })}
          </tbody>
        </Table>
      </div>
      {showZone && (
        <div className="note text-right">
          {t("price-does-not-include-PPVX")}
        </div>
      )}
    </div>
  );
};

export default Domestic;
