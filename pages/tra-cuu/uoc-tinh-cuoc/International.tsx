import { Table } from "react-bootstrap";
import Link from "next/link";

const International = ({ t, list, convertDay, handleConvertNumber }) => {
  const { data, msgInternational } = list;
  return (
    <div
      className="estimate-charge-tab-table"
      id="estimate-charge-tab-table-international"
    >
      <div className="estimate-charge-tab-table-title">
        <h3 className="text-uppercase">{t("international-EMS")}</h3>
      </div>

      <div>
        <Table responsive>
          <thead>
            <tr>
              <th>{t("service")}</th>
              <th>{t("into-money")}</th>
              <th>{t("total-time")}</th>
            </tr>
          </thead>
          <tbody>
            {Array.isArray(data) &&
              data.map((item: any, i: number) => {
                return (
                  <tr key={i}>
                    <td>{item.Description ? t("international-EMS") : ""}</td>
                    <td>
                      {item.Rates === "0"
                        ? t("not-applicable")
                        : handleConvertNumber(item.Rates) + " " + "VND"}
                    </td>
                    <td>{convertDay(item.J, item.Type)}</td>
                  </tr>
                );
              })}
          </tbody>
        </Table>
      </div>
      {msgInternational && (
        <div className="note text-right">
          {t("note-international")}{" "}
          <Link
            as="/tra-cuu/thoi-gian-toan-trinh"
            href="/tra-cuu/thoi-gian-toan-trinh"
            // shallow
          >
            <a target="_blank">{t("here")}</a>
          </Link>
        </div>
      )}
    </div>
  );
};

export default International;
