import { Fragment, useEffect, useState } from "react";
import {
  Col,
  Container,
  Row,
  Form,
  Tab,
  Nav,
  Breadcrumb,
  Spinner,
  Button,
} from "react-bootstrap";

import { withTranslation } from "../../../i18n";
import "styles/OrderSearch.module.css";
import { BreadCrumb, SeoMeta } from "components";
import styled from "styled-components";
import _ from "lodash";
import NumberFormat from "react-number-format";
import { Select } from "antd";
import { removeVietnameseTones } from "./../../../utils/helper";
import Domestic from "./Domestic";
import International from "./International";
import {
  getDistrictByProvince,
  getEstimateCharge,
  getProvinceList,
  getAllCountryList,
} from "services/look-up.service";
const { Option } = Select;

const ErrorMessage = styled.div`
  width: 100%;
  font-size: 16px;
  line-height: 24px;
  color: #fa292d;
  margin-bottom: 20px;
  margin-top: -20px;
`;

const ErrorMessage1 = styled.div`
  width: 100%;
  font-size: 16px;
  line-height: 24px;
  color: #fa292d;
  margin-top: 20px;
  padding: 0 15px;
`;
const initErrForm = {
  fromProvince: "",
  fromDistrict: "",
  toProvince: "",
  toDistrict: "",
  weight: "",
  totalAmount: "",
  countryCode: "",
  type: "",
};

const initFormState = {
  fromProvince: "",
  fromDistrict: "",
  toProvince: "",
  toDistrict: "",
  weight: "",
  totalAmount: "",
  isType: "1",
  countryCode: "VN",
  language: 0,
};

const initList = {
  formDistrictList: [],
  toDistrictList: [],
  countries: [],
  status: false,
  data: [],
  visible: false,
  visibleInternational: false,
  error: "",
  msgInternational: false,
};

const EstimateCharge: any = ({ t, i18n: { language }, provinceList }) => {
  const [isSpinner, setIsSpinner] = useState<boolean>(false);
  const [isZoneFrom, setIsZoneForm] = useState<boolean>(false);
  const [isZoneTo, setIsZoneTo] = useState<boolean>(false);
  const [showZone, setShowZone] = useState<boolean>(false);
  const [textTab, setTextTab] = useState<string>("first");
  const [errForm, setErrForm] = useState(initErrForm);
  const [formState, setFormState] = useState(initFormState);
  const [list, setList] = useState({
    ...initList,
    fromProvinceList: provinceList,
    toProvinceList: provinceList,
  });

  useEffect(() => {
    if (!_.isEmpty(formState.weight)) {
      setErrForm({
        ...errForm,
        weight: "",
      });
    }
  }, [formState.weight]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  useEffect(() => {
    if ((_.size(list.data) && list.visible) || list.visibleInternational) {
      handleSearchCharge();
    }
  }, [language, list.data.length, list.visible, list.visibleInternational]);

  // handle search
  const handleSearchCharge = async () => {
    if (_.isEmpty(formState.countryCode) && textTab === "second") {
      setErrForm({
        ...errForm,
        countryCode: t("country-blank"),
      });
    } else if (_.isEmpty(formState.fromProvince) && textTab === "first") {
      setErrForm({
        ...errForm,
        fromProvince: t("province-city-blank"),
      });
    } else if (_.isEmpty(formState.fromDistrict) && textTab === "first") {
      setErrForm({
        ...errForm,
        fromDistrict: t("district-blank"),
      });
    } else if (_.isEmpty(formState.toProvince) && textTab === "first") {
      setErrForm({
        ...errForm,
        toProvince: t("province-city-blank"),
      });
    } else if (_.isEmpty(formState.toDistrict) && textTab === "first") {
      setErrForm({
        ...errForm,
        toDistrict: t("district-blank"),
      });
    } else if (_.isEmpty(formState.weight)) {
      setErrForm({
        ...errForm,
        weight: t("weight-blank"),
      });
    } else if (_.isEmpty(formState.isType) && textTab === "second") {
      setErrForm({
        ...errForm,
        type: t("parcel-type-blank"),
      });
    } else {
      const { countryCode, totalAmount, weight } = formState;

      const convertUnits =
        totalAmount === "" ? 0 : convertStringCurrency(totalAmount);

      const newFormState = Object.assign({}, formState);
      newFormState.totalAmount = convertUnits;
      newFormState.language = language === "vn" ? 0 : 1;
      newFormState.weight = convertStringCurrency(weight);

      setIsSpinner(true);
      try {
        const res = await getEstimateCharge(newFormState);
        if (isZoneFrom || isZoneTo) {
          setShowZone(true);
        } else {
          setShowZone(false);
        }
        if (res.data.Code === "00") {
          if (countryCode !== "VN") {
            setList({
              ...list,
              data: res.data.Message,
              visibleInternational: true,
              error: "",
              msgInternational: true,
            });
            setTimeout(() => {
              let el = document.getElementById(
                "estimate-charge-tab-table-international"
              );
              if (el) {
                el.scrollIntoView({
                  behavior: "smooth",
                  block: "center",
                  inline: "nearest",
                });
              }
            }, 100);
          } else {
            if (totalAmount !== "" && totalAmount !== "0") {
              setList({
                ...list,
                data: res.data.Message,
                status: true,
                visible: true,
                error: "",
              });
            } else {
              setList({
                ...list,
                data: res.data.Message,
                status: false,
                visible: true,
                error: "",
              });
            }
            setTimeout(() => {
              let el = document.getElementById("estimate-charge-tab-table");
              if (el) {
                el.scrollIntoView({
                  behavior: "smooth",
                  block: "center",
                  inline: "nearest",
                });
              }
            }, 100);
          }
        } else {
          setList({
            ...list,
            error:
              res.data.Code === "99" ? t("error-estimate") : t("no-record"),
            visible: false,
            visibleInternational: false,
          });
        }
      } catch (error) {
        setList({
          ...list,
          visible: false,
          visibleInternational: false,
          error: error.response.data.Message,
        });
      } finally {
        setIsSpinner(false);
        setErrForm(initErrForm);
      }
    }
  };

  const convertStringCurrency = (totalAmount: any) => {
    return _.toString(totalAmount).replace(/[.,]+/g, "");
  };

  const handleConvertNumber = (number: any) => {
    return Number(Math.round(number))
      .toString()
      .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
  };

  // onSelect tab
  const handleTabSelected = async (e: any) => {
    setTextTab(e);
    setErrForm(initErrForm);
    if (e === "first") {
      setList({
        ...list,
        visible: false,
        error: "",
      });
      setFormState(initFormState);
    } else {
      setFormState({
        ...formState,
        fromProvince: "0",
        fromDistrict: "0",
        toProvince: "0",
        toDistrict: "0",
        weight: "",
        totalAmount: "",
        isType: "1",
        countryCode: "",
      });
      const resCountryList = await getAllCountryList();
      const { data } = resCountryList.data.response;
      setList({
        ...list,
        countries: data,
        visibleInternational: false,
        error: "",
      });
    }
  };

  // onChange
  const handleOnChange = (e: any) => {
    const { name, value } = e.target;

    setFormState({
      ...formState,
      [name]: value,
    });
  };

  // onChange select
  const handleOnChangeForm = async (value: string, type: number) => {
    switch (type) {
      case 1:
        if (!_.isEmpty(value)) {
          const res1 = await getDistrictByProvince(value);
          const { data } = res1.data.response;
          setList({
            ...list,
            formDistrictList: data,
          });
        }

        setFormState({
          ...formState,
          fromProvince: value,
          fromDistrict: "",
        });
        setErrForm({
          ...errForm,
          fromProvince: "",
        });
        break;
      case 2:
        Array.isArray(list.formDistrictList) &&
          list.formDistrictList.map((district) => {
            if (district.districtcode === value) {
              setFormState({
                ...formState,
                fromDistrict: value,
                fromProvince: district.provincecode,
              });
              if (parseInt(district.isZone) === 0) {
                setIsZoneForm(false);
              } else {
                setIsZoneForm(true);
              }
            }
            return district;
          });
        setErrForm({
          ...errForm,
          fromDistrict: "",
          fromProvince: "",
        });
        break;
      case 3:
        if (!_.isEmpty(value)) {
          const res2 = await getDistrictByProvince(value);
          setList({
            ...list,
            toDistrictList: res2.data.response.data,
          });
        }

        setFormState({
          ...formState,
          toProvince: value,
          toDistrict: "",
        });
        setErrForm({
          ...errForm,
          toProvince: "",
        });
        break;
      case 4:
        setErrForm({
          ...errForm,
          toDistrict: "",
          toProvince: "",
        });
        Array.isArray(list.toDistrictList) &&
          list.toDistrictList.map((district) => {
            if (district.districtcode === value) {
              setFormState({
                ...formState,
                toDistrict: value,
                toProvince: district.provincecode,
              });
              if (parseInt(district.isZone) === 0) {
                setIsZoneTo(false);
              } else {
                setIsZoneTo(true);
              }
            }
            return district;
          });
        break;
      case 5:
        setFormState({
          ...formState,
          countryCode: value,
        });
        setErrForm({
          ...errForm,
          countryCode: "",
        });
        break;
      case 6:
        setFormState({
          ...formState,
          isType: value,
        });
        setErrForm({
          ...errForm,
          type: "",
        });
        break;
      default:
        return null;
    }
  };

  // convert service
  const convertService = (text: string, type: string) => {
    // o thu ho
    if (!list.status) {
      switch (type) {
        case "1":
          return t("standard-EMS");
        case "2":
          return t("agree-EMS");
        case "3":
          return t("express-EMS");
        case "4":
          return t("escort-day");
        default:
          return text;
      }
    } else {
      switch (type) {
        case "1":
          return "EMS COD";
        case "2":
          return "EMS E-COD";
        case "3":
          return "EMS S-COD";
        case "4":
          return t("code-agree-EMS");
        default:
          return text;
      }
    }
  };

  // convert day
  const convertDay = (j: any, isType: string) => {
    if (j !== "") {
      if (language === "en") {
        let newItem = j.split("-");
        if (isType !== "4") {
          if (parseFloat(newItem[0]) > 1) {
            return j + " " + t("day") + "s";
          } else {
            return j + " " + t("day");
          }
        } else {
          if (parseFloat(newItem[0]) > 1) {
            return j + " " + t("hour") + "s";
          } else {
            return j + " " + t("hour");
          }
        }
      } else {
        if (isType === "4") {
          return j + " " + t("hour");
        } else {
          return j + " " + t("day");
        }
      }
    } else {
      return t("contact-the-call-center-consulting");
    }
  };

  return (
    <Fragment>
      <SeoMeta title={`EMS | ${t("calculate-postage")}`} />
      <div className="estimate-charge">
        <BreadCrumb>
          <Breadcrumb.Item active>
            <span>{t("calculate-postage")}</span>
          </Breadcrumb.Item>
        </BreadCrumb>
        <Container>
          <div className="estimate-charge-tab">
            <Tab.Container
              id="left-tabs-example"
              defaultActiveKey="first"
              onSelect={handleTabSelected}
            >
              <Row>
                <Col sm={12}>
                  <Nav variant="pills" className="flex-row text-center">
                    <Nav.Item>
                      <Nav.Link eventKey="first">
                        {t("domestic-charges")}
                      </Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link eventKey="second">
                        {t("international-charges")}
                      </Nav.Link>
                    </Nav.Item>
                  </Nav>
                </Col>
                <Col sm={12}>
                  <Tab.Content>
                    {/* domestic */}
                    <Tab.Pane eventKey="first">
                      <div className="estimate-charge-tab-title">
                        <h3>{t("calculate-postage")}</h3>
                      </div>
                      <div className="estimate-charge-tab-content">
                        <Form onSubmit={handleSearchCharge}>
                          <Row className="justify-content-between">
                            <Col md={6}>
                              <Form.Group controlId="exampleForm.ControlSelect1">
                                <Form.Label>
                                  {t("sent-from")}{" "}
                                  <span className="ic-important">*</span>
                                </Form.Label>
                                <Select
                                  virtual={false}
                                  showSearch
                                  style={{ width: "100%" }}
                                  placeholder={t("choose-province-city")}
                                  onChange={(e) => handleOnChangeForm(e, 1)}
                                  value={formState.fromProvince}
                                  optionFilterProp="children"
                                  filterOption={(input, option) => {
                                    const newString = option.children;
                                    const newInput = removeVietnameseTones(
                                      input
                                    );

                                    return (
                                      removeVietnameseTones(newString)
                                        .toLowerCase()
                                        .indexOf(newInput.toLowerCase()) >= 0
                                    );
                                  }}
                                >
                                  <Option value="">
                                    {t("choose-province-city")}
                                  </Option>
                                  {Array.isArray(list.fromProvinceList) &&
                                    list.fromProvinceList.map(
                                      (province: any, i: number) => {
                                        return (
                                          <Option key={i} value={province.code}>
                                            {province.name}
                                          </Option>
                                        );
                                      }
                                    )}
                                </Select>
                                {errForm.fromProvince && (
                                  <ErrorMessage>
                                    {t("province-city-blank")}
                                  </ErrorMessage>
                                )}
                              </Form.Group>

                              <Form.Group controlId="exampleForm.ControlSelect1">
                                <Select
                                  virtual={false}
                                  showSearch
                                  style={{ width: "100%" }}
                                  placeholder={t("choose-district")}
                                  onChange={(e) => handleOnChangeForm(e, 2)}
                                  value={formState.fromDistrict}
                                  optionFilterProp="children"
                                  filterOption={(input, option) => {
                                    const newString = option.children;
                                    const newInput = removeVietnameseTones(
                                      input
                                    );

                                    return (
                                      removeVietnameseTones(newString)
                                        .toLowerCase()
                                        .indexOf(newInput.toLowerCase()) >= 0
                                    );
                                  }}
                                >
                                  <Option value="">
                                    {t("choose-district")}
                                  </Option>
                                  {Array.isArray(list.formDistrictList) &&
                                    list.formDistrictList.map(
                                      (district: any, i: number) => {
                                        return (
                                          <Option
                                            key={i}
                                            value={district.districtcode}
                                          >
                                            {district.districtname}
                                          </Option>
                                        );
                                      }
                                    )}
                                </Select>
                                {errForm.fromDistrict && (
                                  <ErrorMessage>
                                    {t("district-blank")}
                                  </ErrorMessage>
                                )}
                              </Form.Group>
                            </Col>
                            <Col md={6}>
                              <Form.Group controlId="exampleForm.ControlSelect1">
                                <Form.Label>
                                  {t("delivered-at")}{" "}
                                  <span className="ic-important">*</span>
                                </Form.Label>
                                <Select
                                  virtual={false}
                                  showSearch
                                  style={{ width: "100%" }}
                                  placeholder={t("choose-province-city")}
                                  onChange={(e) => handleOnChangeForm(e, 3)}
                                  value={formState.toProvince}
                                  optionFilterProp="children"
                                  filterOption={(input, option) => {
                                    const newString = option.children;
                                    const newInput = removeVietnameseTones(
                                      input
                                    );

                                    return (
                                      removeVietnameseTones(newString)
                                        .toLowerCase()
                                        .indexOf(newInput.toLowerCase()) >= 0
                                    );
                                  }}
                                >
                                  <Option value="">
                                    {t("choose-province-city")}
                                  </Option>
                                  {Array.isArray(list.toProvinceList) &&
                                    list.toProvinceList.map(
                                      (province: any, i: number) => {
                                        return (
                                          <Option key={i} value={province.code}>
                                            {province.name}
                                          </Option>
                                        );
                                      }
                                    )}
                                </Select>
                                {errForm.toProvince && (
                                  <ErrorMessage>
                                    {t("province-city-blank")}
                                  </ErrorMessage>
                                )}
                              </Form.Group>

                              <Form.Group controlId="exampleForm.ControlSelect1">
                                <Select
                                  virtual={false}
                                  showSearch
                                  style={{ width: "100%" }}
                                  placeholder={t("choose-district")}
                                  onChange={(e) => handleOnChangeForm(e, 4)}
                                  value={formState.toDistrict}
                                  optionFilterProp="children"
                                  filterOption={(input, option) => {
                                    const newString = option.children;
                                    const newInput = removeVietnameseTones(
                                      input
                                    );

                                    return (
                                      removeVietnameseTones(newString)
                                        .toLowerCase()
                                        .indexOf(newInput.toLowerCase()) >= 0
                                    );
                                  }}
                                >
                                  <Option value="">
                                    {t("choose-district")}
                                  </Option>
                                  {Array.isArray(list.toDistrictList) &&
                                    list.toDistrictList.map(
                                      (district: any, i: number) => {
                                        return (
                                          <Option
                                            key={i}
                                            value={district.districtcode}
                                          >
                                            {district.districtname}
                                          </Option>
                                        );
                                      }
                                    )}
                                </Select>
                                {errForm.toDistrict && (
                                  <ErrorMessage>
                                    {t("district-blank")}
                                  </ErrorMessage>
                                )}
                              </Form.Group>
                            </Col>

                            <Col md={6}>
                              <Form.Group controlId="exampleForm.ControlSelect1">
                                <Form.Label>
                                  {t("weight")} (Gram){" "}
                                  <span className="ic-important">*</span>
                                </Form.Label>
                                <NumberFormat
                                  thousandSeparator={"."}
                                  decimalSeparator={","}
                                  className="form-control"
                                  name="weight"
                                  value={formState.weight}
                                  onChange={handleOnChange}
                                  placeholder="0 (gram)"
                                  autoComplete="off"
                                />
                                {errForm.weight && (
                                  <ErrorMessage>
                                    {t("weight-blank")}
                                  </ErrorMessage>
                                )}
                              </Form.Group>
                            </Col>

                            <Col md={6}>
                              <Form.Group controlId="exampleForm.ControlSelect1">
                                <Form.Label>
                                  {t("the-amount-is-collected")}
                                </Form.Label>
                                <NumberFormat
                                  thousandSeparator={"."}
                                  decimalSeparator={","}
                                  className="form-control"
                                  name="totalAmount"
                                  value={formState.totalAmount}
                                  onChange={handleOnChange}
                                  placeholder="0 VND"
                                  autoComplete="off"
                                />
                                {errForm.totalAmount && (
                                  <ErrorMessage>
                                    {errForm.totalAmount}
                                  </ErrorMessage>
                                )}
                              </Form.Group>
                            </Col>
                            <div className="estimate-charge-tab-content-btn">
                              <Button
                                type="button"
                                onClick={handleSearchCharge}
                                className="d-flex align-items-center justify-content-center"
                              >
                                {t("text-search")}
                                {isSpinner && (
                                  <Spinner
                                    animation="border"
                                    className="ml-3"
                                  />
                                )}
                              </Button>
                            </div>
                            {list.error && (
                              <ErrorMessage1>{list.error}</ErrorMessage1>
                            )}
                          </Row>
                        </Form>
                      </div>

                      {list.data && list.visible && (
                        <Domestic
                          list={list}
                          showZone={showZone}
                          convertDay={convertDay}
                          handleConvertNumber={handleConvertNumber}
                          convertService={convertService}
                          t={t}
                        />
                      )}
                    </Tab.Pane>

                    {/* international */}
                    <Tab.Pane eventKey="second">
                      <div className="estimate-charge-tab-title">
                        <h3 className="text-uppercase">
                          {t("calculate-postage")}
                        </h3>
                      </div>
                      <div className="estimate-charge-tab-content">
                        <Form>
                          <Row className="justify-content-between">
                            <Col md={12}>
                              <Form.Group controlId="formBasicEmail">
                                <Form.Label>{t("sent-from")}</Form.Label>
                                <Form.Control
                                  type="text"
                                  placeholder={t("choose-country")}
                                  value={t("vn")}
                                  readOnly
                                />
                              </Form.Group>
                            </Col>
                            <Col md={12}>
                              <Form.Group controlId="exampleForm.ControlSelect1">
                                <Form.Label>
                                  {t("delivered-at")}{" "}
                                  <span className="ic-important">*</span>
                                </Form.Label>
                                <Select
                                  virtual={false}
                                  showSearch
                                  style={{ width: "100%" }}
                                  placeholder={t("choose-country")}
                                  onChange={(e) => handleOnChangeForm(e, 5)}
                                  value={formState.countryCode}
                                  optionFilterProp="children"
                                  filterOption={(input, option) => {
                                    const newString = option.children;
                                    const newInput = removeVietnameseTones(
                                      input
                                    );

                                    return (
                                      removeVietnameseTones(newString)
                                        .toLowerCase()
                                        .indexOf(newInput.toLowerCase()) >= 0
                                    );
                                  }}
                                >
                                  <Option value="">
                                    {t("choose-country")}
                                  </Option>
                                  {Array.isArray(list.countries) &&
                                    list.countries.map(
                                      (country: any, i: number) => {
                                        return (
                                          <Option
                                            key={i}
                                            value={country.countryCode}
                                          >
                                            {country.countryName}
                                          </Option>
                                        );
                                      }
                                    )}
                                </Select>
                                {errForm.countryCode && (
                                  <ErrorMessage>
                                    {t("country-blank")}
                                  </ErrorMessage>
                                )}
                              </Form.Group>
                            </Col>

                            <Col md={12}>
                              <Form.Group controlId="exampleForm.ControlSelect1">
                                <Form.Label>
                                  {t("weight")} (Gram){" "}
                                  <span className="ic-important">*</span>
                                </Form.Label>
                                <NumberFormat
                                  thousandSeparator={"."}
                                  decimalSeparator={","}
                                  className="form-control"
                                  name="weight"
                                  value={formState.weight}
                                  onChange={handleOnChange}
                                  placeholder="0 (gram)"
                                  autoComplete="off"
                                />
                                {errForm.weight && (
                                  <ErrorMessage>
                                    {t("weight-blank")}
                                  </ErrorMessage>
                                )}
                              </Form.Group>
                            </Col>

                            <Col md={12}>
                              <Form.Group controlId="exampleForm.ControlSelect1">
                                <Form.Label>
                                  {t("parcel-type")}{" "}
                                  <span className="ic-important">*</span>
                                </Form.Label>

                                <Select
                                  virtual={false}
                                  showSearch
                                  style={{ width: "100%" }}
                                  placeholder={t("choose-the-type-of-parcel")}
                                  onChange={(e) => handleOnChangeForm(e, 6)}
                                  value={formState.isType}
                                  optionFilterProp="children"
                                  filterOption={(input, option) => {
                                    const newString = option.children;
                                    const newInput = removeVietnameseTones(
                                      input
                                    );

                                    return (
                                      removeVietnameseTones(newString)
                                        .toLowerCase()
                                        .indexOf(newInput.toLowerCase()) >= 0
                                    );
                                  }}
                                >
                                  <Option value="">
                                    {t("choose-the-type-of-parcel")}
                                  </Option>
                                  <Option value="1">{t("document")}</Option>
                                  <Option value="2">{t("commodity")}</Option>
                                </Select>
                                {errForm.type && (
                                  <ErrorMessage>
                                    {t("choose-the-type-of-parcel")}
                                  </ErrorMessage>
                                )}
                              </Form.Group>
                            </Col>
                            <div className="estimate-charge-tab-content-btn">
                              <Button
                                type="button"
                                onClick={handleSearchCharge}
                                className="d-flex align-items-center justify-content-center"
                              >
                                {t("text-search")}
                                {isSpinner && (
                                  <Spinner
                                    animation="border"
                                    className="ml-3"
                                  />
                                )}
                              </Button>
                            </div>
                            {list.error && (
                              <ErrorMessage1>{list.error}</ErrorMessage1>
                            )}
                          </Row>
                        </Form>
                      </div>

                      {list.visibleInternational && (
                        <International
                          t={t}
                          list={list}
                          convertDay={convertDay}
                          handleConvertNumber={handleConvertNumber}
                        />
                      )}
                    </Tab.Pane>
                  </Tab.Content>
                </Col>
              </Row>
            </Tab.Container>
          </div>
        </Container>
      </div>
    </Fragment>
  );
};

export async function getServerSideProps() {
  const resProvinceList = await getProvinceList();
  const provinceList = _.get(resProvinceList, "data.response.data", []);

  return { props: { provinceList: provinceList } };
}

export default withTranslation()(EstimateCharge);
