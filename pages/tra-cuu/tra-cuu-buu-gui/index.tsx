import { Fragment, useState, useEffect, useRef } from "react";
import {
  Breadcrumb,
  Col,
  Container,
  Form,
  Row,
  Spinner,
  Table as Table2,
} from "react-bootstrap";

import { BreadCrumb, Table, SeoMeta } from "../../../components";
import { withTranslation } from "i18n";
import "styles/OrderSearch.module.css";
import { useRouter } from "next/router";
import { get, isEmpty, trim } from "lodash";

import { saveAs } from "file-saver";
import * as XLSX from "xlsx";
import Link from "next/link";
import { DownloadOutlined } from "@ant-design/icons";
import { getPostalItemsByCode } from "services/look-up.service";
import { Portal } from "interfaces/look-up.interface";
import { convertStringOffice, formatDate } from "utils/helper";

const initPortal: Portal = {
  info: {},
  status: [],
  delivery: [],
  deliveryList: [],
  error: "",
  visible: false,
};

const OrderSearch = ({ t, i18n: { language } }) => {
  const deliveryInfo = {
    fields: [
      t("search-post-info-stt"),
      t("search-post-info-date"),
      t("search-post-info-hour"),
      t("search-post-info-column-status"),
      t("search-post-info-position"),
    ],
    keys: ["index", "NGAY", "GIO", "TRANG_THAI", "VI_TRI"],
  };
  const deliveryListInfo = {
    fields: [
      t("search-post-info-stt"),
      t("search-post-info-billofladingcode"),
      t("search-post-info-column-status"),
      t("search-post-info-reason"),
      t("search-post-info-date-phat"),
      t("search-post-info-hour-phat"),
      t("search-post-info-position"),
    ],
    keys: [
      "index",
      "MA_E1",
      "TRANG_THAI",
      "NGUOI_NHAN",
      "NGAY_NHAN",
      "GIO_NHAN",
      "VI_TRI",
    ],
  };
  const statusInfo = {
    fields: [
      t("search-post-info-stt"),
      t("search-post-info-date-phat"),
      t("search-post-info-hour-phat"),
      t("search-post-info-position"),
      t("search-post-info-reason"),
    ],
    keys: ["index", "NGAY_PHAT", "GIO_TRANG_THAI", "VI_TRI", "TRANG_THAI"],
  };

  const EXCEL_TYPE =
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
  const router = useRouter();
  const { query } = router;

  const [code, setCode] = useState<string>("");
  const [codeBackup, setCodeBackup] = useState<string>("");
  const [isSpinner, setIsSpinner] = useState<boolean>(false);
  const refPostOffice = useRef(null);

  const [portal, setPortal] = useState<Portal>(initPortal);

  const saveAsExcelFile = (buffer: any, fileName: string) => {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    // FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    saveAs(data, fileName);
  };

  const exportCsv2 = () => {
    let todaysDate = formatDate(new Date());
    let excelFileName = `Tra_cuu_buu_gui_${todaysDate}.xlsx`;
    const data1 = portal.deliveryList.map((o: any, i: number) => {
      o.index = i + 1;
      return o;
    });
    const data = data1.map(
      ({
        index,
        MA_E1,
        TRANG_THAI,
        NGUOI_NHAN,
        NGAY_NHAN,
        GIO_NHAN,
        VI_TRI,
      }) => ({
        [`${t("search-post-info-stt")}`]: index,
        [`${t("search-post-info-billofladingcode")}`]: MA_E1,
        [`${t("search-post-info-column-status")}`]: TRANG_THAI,
        [`${t("search-post-info-reason")}`]: NGUOI_NHAN,
        [`${t("search-post-info-date-phat")}`]: NGAY_NHAN,
        [`${t("search-post-info-hour-phat")}`]: GIO_NHAN,
        [`${t("search-post-info-position")}`]: VI_TRI,
      })
    );

    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
    const workbook: XLSX.WorkBook = {
      Sheets: { Tra_cuu_buu_gui: worksheet },
      SheetNames: ["Tra_cuu_buu_gui"],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "xlsx",
      type: "array",
    });
    // const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    saveAsExcelFile(excelBuffer, excelFileName);
  };

  // handle change code
  const handleChange = (e: any) => {
    const { value } = e.target;
    setCode(value);
  };

  // handle search
  const handleSearchByCode = async (codeOffice: string) => {
    if (isEmpty(codeOffice)) {
      refPostOffice.current.focus();
    } else {
      setCode(codeOffice);
      let langId = 0;
      if (language === "en") {
        langId = 1;
      }

      setIsSpinner(true);
      const res = await getPostalItemsByCode(
        trim(codeOffice).toUpperCase(),
        langId
      );
      setIsSpinner(false);
      if (res.data.Code === "00") {
        const {
          TBL_INFO,
          List_TBL_DINH_VI,
          List_TBL_DELIVERY,
          List_TBL_DELIVERY_lIST,
        } = res.data;
        if (!isEmpty(List_TBL_DELIVERY_lIST)) {
          setCodeBackup(codeOffice);
        } else {
          if (codeBackup.includes(codeOffice)) {
          } else {
            setCodeBackup("");
          }
        }
        setPortal({
          ...portal,
          info: TBL_INFO,
          status: List_TBL_DELIVERY,
          delivery: List_TBL_DINH_VI,
          deliveryList: List_TBL_DELIVERY_lIST,
          visible: true,
          error: "",
        });
      } else if (res.status === 404) {
        setPortal({
          ...portal,
          error: `Chỉ có thể tìm kiếm tối đa 117 mã! Bạn đã tìm: ${
            code.match(/,/g).length
          } mã!`,
          visible: false,
        });
      } else {
        setPortal({
          ...portal,
          error: res.data.Message,
          visible: false,
        });
      }
    }
  };

  // submit form
  const onSubmitForm = (e: any) => {
    e.preventDefault();
    handleSearchByCode(convertStringOffice(code));
  };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  useEffect(() => {
    handleSearchByCode(code);
  }, [language]);

  useEffect(() => {
    if (!isEmpty(query) && !isEmpty(query.code)) {
      setCode(String(convertStringOffice(query.code)));
      handleSearchByCode(String(convertStringOffice(query.code)));
    }
  }, [query.code]);

  let referenceCode = get(portal, "info.MA_THAM_CHIEU") ?? "";
  let solo = get(portal, "info.LO") ?? "";

  return (
    <div className="order-search wr_order_search">
      <SeoMeta title={`EMS | ${t("search-post-title")}`} />
      <BreadCrumb>
        <Breadcrumb.Item active>
          <span>{t("search-post-title")}</span>
        </Breadcrumb.Item>
      </BreadCrumb>
      <Container>
        <div className="search-holiday post-office">
          <div className="search-holiday-text d-flex align-items-center">
            <img src="/img/icon/order-search.png" alt="icon post search" />
            <div>{t("enter-code")}</div>
          </div>
          <div className="search-holiday-action post-office">
            <Form onSubmit={onSubmitForm}>
              <Row>
                <Col sm={9}>
                  <input
                    type="text"
                    onChange={handleChange}
                    placeholder={t("search-post-type-code-placeholder")}
                    value={code}
                    autoComplete="off"
                    ref={refPostOffice}
                  />
                </Col>
                <Col sm={3}>
                  <button className="d-flex align-items-center justify-content-center">
                    {t("search-post-text")}
                    {isSpinner && (
                      <Spinner animation="border" className="ml-3" />
                    )}
                  </button>
                </Col>
              </Row>
              <Row>
                <Col
                  sm={9}
                  className="text-right"
                  style={{ marginTop: 20, fontSize: 14 }}
                >
                  <i>{t("note-postoffice")}</i>
                </Col>
              </Row>
            </Form>
          </div>
          {portal.error && (
            <div className="search-holiday-error">*{portal.error}</div>
          )}
        </div>

        {portal.visible && (
          <Fragment>
            {!isEmpty(portal.info) && (
              <div className="order-search-info">
                <div className="order-search-info-status-title">
                  <h3>{t("search-post-info")}</h3>
                </div>
                <Row className="order-search-info-top">
                  <Col
                    xs={6}
                    sm={3}
                    lg={2}
                    className="order-search-info-top-text"
                  >
                    <div>{t("search-post-info-no")}</div>
                    <p>{portal.info.MAE1}</p>
                  </Col>
                  <Col
                    xs={6}
                    sm={3}
                    lg={2}
                    className="order-search-info-top-text"
                  >
                    <div>{t("search-post-info-weight")}</div>
                    <p>{portal.info.KHOI_LUONG}</p>
                  </Col>
                  <Col
                    xs={6}
                    sm={3}
                    lg={4}
                    className="order-search-info-top-text"
                  >
                    <div>{t("search-post-info-store")}</div>
                    <p>{portal.info.BC_GUI}</p>
                  </Col>
                  <Col
                    xs={6}
                    sm={3}
                    lg={4}
                    className="order-search-info-top-text"
                  >
                    <div>{t("search-post-info-delivery")}</div>
                    <p>{portal.info.BC_PHAT}</p>
                  </Col>
                </Row>

                <Row className="order-search-info-middle">
                  <Col xs={12} sm={6} lg={4}>
                    <div>
                      {t("search-post-info-country-send")}:
                      <span>{portal.info.Nuoc_Chapnhan}</span>
                    </div>
                  </Col>

                  <Col xs={12} sm={6} lg={8}>
                    <div>
                      {t("search-post-info-address-send")}:
                      <span>{portal.info.DIA_CHI_GUI}</span>
                    </div>
                  </Col>
                </Row>
                <Row className="order-search-info-middle">
                  <Col xs={12} sm={6} lg={4}>
                    <div>
                      {t("search-post-info-country-receive")}:
                      <span>{portal.info.Nuoc_Phat}</span>
                    </div>
                  </Col>

                  <Col xs={12} sm={6} lg={8}>
                    <div>
                      {t("search-post-info-address-receive")}:
                      <span>{portal.info.DIA_CHI_NHAN}</span>
                    </div>
                  </Col>
                </Row>

                {((referenceCode !== "" && referenceCode !== "0") || solo) && (
                  <Row className="order-search-info-middle">
                    <Col xs={12} sm={6} lg={4}>
                      <div>
                        {t("search-post-info-refer-no")}:
                        <span>{referenceCode}</span>
                      </div>
                    </Col>

                    <Col xs={12} sm={6} lg={8}>
                      <div>
                        {t("search-post-info-count-no")}:<span>{solo}</span>
                      </div>
                    </Col>
                  </Row>
                )}
                <Row className="order-search-info-bottom">
                  <Col xs={12}>
                    <div>
                      {t("search-post-info-column-status")}:
                      <span>{get(portal, "info.TRANG_THAI") ?? ""}</span>
                    </div>
                  </Col>
                </Row>
              </div>
            )}
            {!isEmpty(portal.delivery) && (
              <div className="order-search-info-status">
                <div className="order-search-info-status-title">
                  <h3>{t("search-post-info-status")}</h3>
                </div>
                <div className="order-search-info-status-content">
                  <Table
                    fields={deliveryInfo.fields}
                    data={portal.delivery}
                    keys={deliveryInfo.keys}
                  />
                </div>
              </div>
            )}

            {!isEmpty(portal.deliveryList) && (
              <div className="order-search-info-status">
                <div className="order-search-info-status-title d-flex justify-content-between align-items-center">
                  <h3>{t("search-post-info-status")}</h3>
                  <button className="btn_download_excel1" onClick={exportCsv2}>
                    <DownloadOutlined className="download_icon1" />
                    {t("shareholder-download-text")}
                  </button>
                </div>
                <div className="order-search-info-status-content">
                  <Table2 responsive>
                    <thead>
                      <tr>
                        {deliveryListInfo.fields.map((field) => {
                          return <th>{field}</th>;
                        })}
                      </tr>
                    </thead>
                    <tbody>
                      {Array.isArray(portal.deliveryList) &&
                        portal.deliveryList.map((item, index) => {
                          return (
                            <tr>
                              {deliveryListInfo.keys.map((key) => {
                                if (key === "index") {
                                  return <td>{index + 1}</td>;
                                } else if (key === "MA_E1") {
                                  return (
                                    <td>
                                      <Link
                                        href={`/tra-cuu/tra-cuu-buu-gui?code=${trim(
                                          item[key]
                                        )}`}
                                      >
                                        <a
                                          target="_blank"
                                          className="code_clickable"
                                        >
                                          {item[key]}
                                        </a>
                                      </Link>
                                    </td>
                                  );
                                } else {
                                  return <td>{item[key]}</td>;
                                }
                              })}
                            </tr>
                          );
                        })}
                    </tbody>
                  </Table2>
                </div>
              </div>
            )}

            {!isEmpty(portal.status) && (
              <div className="order-search-info-play">
                <div className="order-search-info-status-title">
                  <h3>{t("search-post-info-phat")}</h3>
                </div>
                <div className="order-search-info-play-content">
                  <Table
                    fields={statusInfo.fields}
                    data={portal.status}
                    keys={statusInfo.keys}
                  />
                </div>
              </div>
            )}
          </Fragment>
        )}
      </Container>
    </div>
  );
};

OrderSearch.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

export default withTranslation()(OrderSearch);
