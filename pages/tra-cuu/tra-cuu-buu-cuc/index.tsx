import { useEffect, useState } from "react";
import { Col, Container, Row, Breadcrumb, Spinner } from "react-bootstrap";
import { get, isEmpty, size } from "lodash";

import { withTranslation } from "i18n";
import "styles/OrderSearch.module.css";
import { BreadCrumb, Map, SeoMeta } from "components";
import { removeVietnameseTones, checkNull } from "./../../../utils/helper";
import "./SearchPostOffice.module.css";

import { Select } from "antd";
import {
  getDistrictByProvince,
  getProvinceList,
  getCommuneByDistrict,
  getAllPostOffice,
} from "services/look-up.service";
import { getPosition } from "interfaces/look-up.interface";
const { Option } = Select;

const initPosition: getPosition = {
  lat: 21.0260208,
  lng: 105.8280365,
};

const initList = {
  districts: [],
  communes: [],
  postOffice: [],
  maps: [],
};

const initFormState = {
  provinceCode: "",
  districtCode: "",
  communeCode: "null",
};

const SearchPostOffice: any = ({ t, listProvinces }) => {
  const [isSpinner, setIsSpinner] = useState<boolean>(false);
  const [isActive, setIsActive] = useState<boolean>(false);
  const [isEnable, setIsEnable] = useState<boolean>(false);
  const [indexOffice, setIndexOffice] = useState<any>(null);
  const [formState, setFormState] = useState<any>(initFormState);
  const [list, setList] = useState({ ...initList, provinces: listProvinces });
  const [userPosition, setUserPosition] = useState<getPosition>(initPosition);

  useEffect(() => {
    setIsActive(false);
    window.scrollTo(0, 0);
    navigator.geolocation.getCurrentPosition((position: any) => {
      setUserPosition({
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      });
    });
  }, []);

  // handle change province
  const handleOnChangeProvince = async (value: string) => {
    const result = await getDistrictByProvince(value);
    setFormState({
      ...formState,
      provinceCode: value,
      districtCode: "",
      communeCode: "null",
    });

    setList({
      ...list,
      districts: get(result, "data.response.data", []),
    });
  };

  // handle change district
  const handleOnChangeDistrict = async (value: string) => {
    setFormState({
      ...formState,
      districtCode: value,
      communeCode: "null",
    });
    const resData = await getCommuneByDistrict(value);
    setList({
      ...list,
      communes: get(resData, "data.response.data", []),
    });
  };

  // handle change commune
  const handleOnChangeCommune = (value: string) => {
    setFormState({
      ...formState,
      communeCode: value,
    });
  };

  // handle search post office
  const handleSearchPostOffice = async () => {
    const { provinceCode, districtCode, communeCode } = formState;
    try {
      setIsSpinner(true);
      const result = await getAllPostOffice(
        provinceCode,
        districtCode,
        communeCode
      );
      setIsActive(true);
      setIsSpinner(false);
      const data = get(result, "data.response.data", []);
      const newArr = data.filter(
        (item: any) => checkNull(item.latitude) && checkNull(item.longitude)
      );
      setList({
        ...list,
        postOffice: data,
        maps: newArr,
      });
    } catch (error) {
      setIsSpinner(false);
    }
  };

  useEffect(() => {
    if (isEmpty(formState.provinceCode) || isEmpty(formState.districtCode)) {
      setIsEnable(false);
    } else {
      setIsEnable(true);
    }
  }, [formState]);

  // handle change info window
  const handleChangeIndexWindowInfo = (index: any) => (e: any) => {
    setIndexOffice(index);
  };

  let postOfficesSize = size(list.postOffice);
  let textCountPostOffice =
    postOfficesSize > 1
      ? t("search-postal-count-results")
      : t("search-postal-count-result");

  return (
    <div className="search-post-office">
      <SeoMeta title={`EMS | ${t("search-postal-title")}`} />
      <BreadCrumb>
        <Breadcrumb.Item active>
          <span>{t("search-postal-title")}</span>
        </Breadcrumb.Item>
      </BreadCrumb>
      <Container>
        <div className="search-post-office-wrapper">
          <div className="search-post-office-content">
            <Row>
              <Col md={5}>
                <div className="search-post-office-action">
                  <Select
                    virtual={false}
                    showSearch
                    style={{ width: "100%" }}
                    placeholder={t("search-postal-choose-province")}
                    onChange={handleOnChangeProvince}
                    value={formState.provinceCode}
                    optionFilterProp="children"
                    filterOption={(input, option) => {
                      const newString = option.children;
                      const newInput = removeVietnameseTones(input);

                      return (
                        removeVietnameseTones(newString)
                          .toLowerCase()
                          .indexOf(newInput.toLowerCase()) >= 0
                      );
                    }}
                  >
                    <Option value="">
                      {t("search-postal-choose-province")}
                    </Option>
                    {list.provinces.map((province: any) => {
                      return (
                        <Option key={province.code} value={province.code}>
                          {province.name}
                        </Option>
                      );
                    })}
                  </Select>
                  <Select
                    virtual={false}
                    showSearch
                    style={{ width: "100%" }}
                    placeholder={t("search-postal-choose-district")}
                    optionFilterProp="children"
                    onChange={handleOnChangeDistrict}
                    value={formState.districtCode}
                    filterOption={(input, option) => {
                      const newString = option.children;
                      const newInput = removeVietnameseTones(input);

                      return (
                        removeVietnameseTones(newString)
                          .toLowerCase()
                          .indexOf(newInput.toLowerCase()) >= 0
                      );
                    }}
                  >
                    <Option value="">
                      {t("search-postal-choose-district")}
                    </Option>
                    {list.districts.map((district: any) => {
                      return (
                        <Option
                          key={district.districtcode}
                          value={district.districtcode}
                        >
                          {district.districtname}
                        </Option>
                      );
                    })}
                  </Select>
                  <Select
                    virtual={false}
                    showSearch
                    style={{ width: "100%" }}
                    placeholder={t("search-postal-choose-commune")}
                    optionFilterProp="children"
                    onChange={handleOnChangeCommune}
                    value={formState.communeCode}
                    filterOption={(input, option) => {
                      const newString = option.children;
                      const newInput = removeVietnameseTones(input);

                      return (
                        removeVietnameseTones(newString)
                          .toLowerCase()
                          .indexOf(newInput.toLowerCase()) >= 0
                      );
                    }}
                  >
                    <Option value="null">
                      {t("search-postal-choose-commune")}
                    </Option>
                    {list.communes.map((commune: any) => {
                      return (
                        <Option
                          key={commune.communeCode}
                          value={commune.communeCode}
                        >
                          {commune.communeName}
                        </Option>
                      );
                    })}
                  </Select>
                  <button
                    onClick={handleSearchPostOffice}
                    disabled={isEnable ? false : true}
                    className="d-flex align-items-center justify-content-center"
                  >
                    {t("search-postal-button-text")}
                    {isSpinner && (
                      <Spinner animation="border" className="ml-3" />
                    )}
                  </button>
                </div>

                {isActive ? (
                  <div className="search-post-office-list">
                    <div className="search-post-office-list-title">
                      <h3 className="d-flex align-items-center justify-content-between">
                        {t("search-postal-list-postal")}
                        <i className="total-postoffice">
                          {postOfficesSize}{" "}
                          {postOfficesSize >= 0 && textCountPostOffice}
                        </i>
                      </h3>
                    </div>
                    {Array.isArray(list.postOffice) &&
                    list.postOffice &&
                    list.postOffice.length > 0 ? (
                      <div
                        className="search-post-office-list-item-wrapper"
                        id="custom-scrollbar"
                      >
                        {list.postOffice.map((item: any) => {
                          return (
                            <div
                              className="search-post-office-content-item"
                              onClick={handleChangeIndexWindowInfo(item.code)}
                              key={item.code}
                            >
                              <div className="d-flex align-items-center search-post-office-content-item-title">
                                <svg
                                  width="12"
                                  height="16"
                                  viewBox="0 0 12 16"
                                  fill="none"
                                  xmlns="http://www.w3.org/2000/svg"
                                >
                                  <path
                                    d="M11.7785 14.9717L9.80094 10.7886C9.76148 10.7052 9.6931 10.6412 9.6111 10.6063L8.62731 10.1684L10.3319 7.31138C10.336 7.30452 10.3399 7.29758 10.3435 7.29049C10.6824 6.629 10.8543 5.91418 10.8543 5.16603C10.8543 3.90747 10.3607 2.72756 9.46458 1.84371C8.56857 0.960055 7.38152 0.48278 6.12219 0.500475C4.89911 0.517438 3.74751 1.00872 2.87947 1.88387C2.01152 2.75897 1.52953 3.91453 1.52226 5.13776C1.51781 5.88527 1.69444 6.62964 2.03294 7.29046C2.03657 7.29752 2.04041 7.3045 2.04448 7.31135L3.74909 10.1683L2.75961 10.6087C2.67867 10.6447 2.61334 10.7085 2.57548 10.7886L0.598034 14.9717C0.531852 15.1116 0.559772 15.2779 0.667995 15.3886C0.776188 15.4992 0.941774 15.5309 1.08319 15.468L3.56047 14.3651L6.03776 15.468C6.13359 15.5106 6.24301 15.5106 6.33884 15.468L8.81613 14.3651L11.2934 15.468C11.3418 15.4895 11.393 15.5 11.4439 15.5C11.5414 15.5 11.6374 15.4614 11.7086 15.3886C11.8167 15.2779 11.8446 15.1116 11.7785 14.9717ZM2.68649 6.94256C2.40143 6.38205 2.25879 5.7764 2.26256 5.14218C2.2751 3.02024 4.01118 1.27002 6.1325 1.24063C7.19205 1.22639 8.19099 1.62732 8.9448 2.37079C9.69884 3.11443 10.1141 4.10712 10.1141 5.16606C10.1141 5.79181 9.97142 6.38941 9.69005 6.94256L6.50613 12.2789C6.40892 12.4418 6.25148 12.4594 6.18829 12.4594C6.12506 12.4594 5.96762 12.4418 5.87044 12.2789L2.68649 6.94256ZM8.9666 13.6218C8.87077 13.5792 8.76134 13.5792 8.66551 13.6218L6.18823 14.7247L3.71094 13.6218C3.61511 13.5792 3.50569 13.5792 3.40986 13.6218L1.69371 14.3859L3.18603 11.2292L4.13109 10.8086L5.23464 12.6582C5.43691 12.9972 5.79339 13.1996 6.18817 13.1996C6.58294 13.1996 6.93943 12.9972 7.14169 12.6582L8.24525 10.8086L9.19039 11.2293L10.6826 14.3859L8.9666 13.6218Z"
                                    fill="#313131"
                                  />
                                  <path
                                    d="M6.18923 3.47656C5.25776 3.47656 4.5 4.23433 4.5 5.16576C4.5 6.0972 5.25776 6.85496 6.18923 6.85496C7.12066 6.85499 7.87843 6.0972 7.87843 5.16576C7.87843 4.23433 7.12066 3.47656 6.18923 3.47656ZM6.18923 6.11474C5.66596 6.11477 5.24021 5.68909 5.24021 5.16576C5.24021 4.64249 5.66593 4.21678 6.18923 4.21678C6.7125 4.21678 7.13821 4.64249 7.13821 5.16576C7.13821 5.68903 6.7125 6.11474 6.18923 6.11474Z"
                                    fill="#313131"
                                  />
                                </svg>
                                <h3>{item.name}</h3>
                              </div>
                              <div className="search-post-office-content-item-description">
                                <p>{item.address}</p>
                                <p>{item.phoneNumber}</p>
                                <p>{item.provinceName}</p>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    ) : (
                      <div className="not-found-office d-flex justify-content-center align-items-center">
                        {t("search-postal-not-found-text")}
                      </div>
                    )}
                  </div>
                ) : (
                  ""
                )}
              </Col>
              <Col md={7}>
                <Map
                  listPositions={list.maps}
                  userLocation={userPosition}
                  indexOffice={indexOffice}
                />
              </Col>
            </Row>
          </div>
        </div>
      </Container>
    </div>
  );
};

export async function getServerSideProps() {
  const resProvinceList = await getProvinceList();
  const provinceList = get(resProvinceList, "data.response.data", []);

  return { props: { listProvinces: provinceList } };
}

export default withTranslation()(SearchPostOffice);
