import { Fragment, useEffect } from "react";
import { withTranslation } from "i18n";
import { get } from "lodash";
import axios from "axios";
import { Modal } from "antd";
import { getCookieByName, getJsonLodash } from "./../utils/helper";
import Link from "next/link";
import { getHomePageConfig, getAllBanner } from "./../services/home.service";
import "styles/Home.module.css";
import loadable from "@loadable/component";
import { useAppContext } from "contexts/app";
import { INFO_REGISTER_PATH } from "configs/constants";
import { SeoMeta } from "components";
import { defaultDescription, defaultTitle } from "components/SeoMeta/SeoMeta";

const BannerMain = loadable(
  () => import("./../components/BannerMain/BannerMain")
);
const LookupPostalItem = loadable(
  () => import("./../components/Home/LookupPostalItem/LookupPostalItem")
);
const Service = loadable(() => import("./../components/Home/Service/Service"));
const DownloadEMS = loadable(
  () => import("./../components/Home/DownloadEMS/DownloadEMS")
);
const News = loadable(() => import("./../components/Home/News/News"));
const FastService = loadable(
  () => import("./../components/Home/FastService/FastService")
);
const Partner = loadable(() => import("./../components/Home/Partner/Partner"));

const Home: any = (props: any) => {
  const appContext = useAppContext();
  const { t } = props;

  function handleCancel() {
    appContext.setIsShowPopupRegister(false);
  }

  useEffect(() => {
    const timeout = setTimeout(() => {
      if (appContext.isShowPopupRegister === null) {
        appContext.setIsShowPopupRegister(true);
      }
    }, 3000);

    return () => {
      clearTimeout(timeout);
    };
  }, []);

  return (
    <Fragment>
      {!!appContext.isShowPopupRegister && (
        <Modal
          onCancel={handleCancel}
          closeIcon={<span className="close1"></span>}
          className="wr_popup_info_register"
          visible={!!appContext.isShowPopupRegister}
          footer={null}
          style={{ padding: "0px", top: "calc(50% - 300px)" }}
          width={650}
        >
          <Link as={INFO_REGISTER_PATH} href={INFO_REGISTER_PATH} shallow>
            <a onClick={(e) => appContext.setIsShowPopupRegister(false)}>
              <img className="no-select" src="/img/new_popup.jpg" alt="logo" />
            </a>
          </Link>
        </Modal>
      )}

      <SeoMeta title={defaultTitle} description={defaultDescription} />

      <BannerMain {...props} />
      <LookupPostalItem />
      <Service {...props} />
      <DownloadEMS {...props} />
      <News {...props} />
      <FastService {...props} />
      <Partner {...props} />
    </Fragment>
  );
};

export async function getServerSideProps(context: any) {
  const cookie = get(context, "req.headers.cookie", "");
  const langConvert = getCookieByName("langConvert", cookie);

  const languageI8 = get(context, "req.language", "vn");
  const langId = get(getJsonLodash(langConvert), `${languageI8}`, 1);

  // home config
  const data = await axios
    .all([getHomePageConfig(langId), getAllBanner(languageI8)])
    .then(function (data) {
      const homeConfigData = get(data, "[0].data.response", null);
      const listBanners = get(data, "[1].data.response", []);

      return {
        homeConfigData,
        listBanners,
        namespacesRequired: ["common"],
      };
    });

  return {
    props: data,
  };
}

export default withTranslation("common")(Home);
