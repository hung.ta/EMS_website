import React from "react";
import { useRouter } from "next/router";
import { get, isEmpty } from "lodash";
import { withTranslation } from "i18n";
import { getLangIdByName } from "./../../utils/helper";
import {
  swrGetNavigationByUrl,
  swrGetPostByCategoryId,
  swrGetPostChildByCategoryId,
  swrGetPostTop,
  swrGetCategoryByLangId,
  swrGetServiceCategory,
  getNavigationByUrl,
} from "./../../services/master-page.service";
import loadable from "@loadable/component";
import { useAppContext } from "contexts/app";
import { SeoMeta } from "components";

const NotFound = loadable(() => import("./../../pageComponents/PageNotFound"));
const NewsCategory = loadable(
  () => import("./../../pageComponents/NewsCategory")
);
const NewsSubCategory = loadable(
  () => import("./../../pageComponents/NewsSubCategory")
);
const NewsDetail = loadable(() => import("./../../pageComponents/NewsDetail"));
const SinglePage = loadable(() => import("./../../pageComponents/SinglePage"));
const ServiceDetail = loadable(
  () => import("./../../pageComponents/ServiceDetail")
);
const ServicePage = loadable(
  () => import("./../../pageComponents/ServicePage")
);
const UnderConstruction = loadable(
  () => import("./../../pageComponents/PageUnderConstruction")
);
const Recruitment = loadable(
  () => import("./../../pageComponents/Recruitment")
);
const RecruitmentDetail = loadable(
  () => import("./../../pageComponents/RecruitmentDetail")
);

type SeoMetaProps = {
  title?: string;
  description?: string;
  keywords?: string;
  url?: string;
  image?: string;
  type?: string;
};

const MasterPage = ({ i18n, t, ...props }) => {
  const router = useRouter();
  const appContext = useAppContext();
  const masterData = callMasterPage(
    router?.asPath.split("?")[0],
    appContext,
    i18n
  );
  const pageInfo = get(masterData, "pageInfo");
  const isRequestingPageInfo = get(masterData, "isRequestingPageInfo");

  const renderPage = () => {
    if (!isEmpty(pageInfo)) {
      if (pageInfo.IsPost) {
        // Recruitment detail or New detail
        const isRecruitment = get(pageInfo, "Post.isRecruitment");
        if (isRecruitment) {
          return <RecruitmentDetail {...masterData} />;
        }
        return <NewsDetail {...masterData} />;
      } else if (pageInfo.IsRecruitment && pageInfo.CategoryChild) {
        return <Recruitment {...masterData} />;
      } else if (pageInfo.IsRecruitment) {
        return <NotFound {...masterData} />;
      } else if (pageInfo.IsNews && pageInfo.CategoryChild) {
        return <NewsCategory {...masterData} />;
      } else if (pageInfo.IsNews) {
        return <NewsSubCategory {...masterData} />;
      } else if (pageInfo.IsServiceCategory) {
        return <ServicePage {...masterData} />;
      } else if (pageInfo.IsServicePost) {
        const serviceStatus = get(pageInfo, "Service.status");
        if (serviceStatus === 1) {
          return <UnderConstruction {...masterData} />;
        } else if (serviceStatus === 0) {
          return <NotFound {...masterData} />;
        }
        return <ServiceDetail {...masterData} />;
      } else {
        const pageStatus = get(pageInfo, "Page.status");
        if (pageStatus === 1) {
          return <UnderConstruction {...masterData} />;
        } else if (pageStatus === 0) {
          return <NotFound {...masterData} />;
        }
        return <SinglePage {...masterData} />;
      }
    }
    return !isRequestingPageInfo ? <NotFound /> : null;
  };

  const seoMeta = get(props, "navigationData");
  const renderSeoMeta: SeoMetaProps = React.useMemo(() => {
    if (!isEmpty(seoMeta)) {
      const {
        IsPost,
        IsRecruitment,
        CategoryChild,
        IsNews,
        IsServiceCategory,
        IsServicePost,
      } = seoMeta;

      const baseSeoMeta = {
        title: get(seoMeta, "Post.seoTitle")
          ? `EMS | ${get(seoMeta, "Post.seoTitle")}`
          : `EMS | ${get(seoMeta, "Post.title")}`,
        description:
          get(seoMeta, "Post.metaDescriptions") ??
          get(seoMeta, "Post.description"),
        url: get(seoMeta, "Post.url"),
        image:
          get(seoMeta, "Post.mediaLinkWebsite") ??
          get(seoMeta, "Post.mediaLinkFacebook"),
        keywords: get(seoMeta, "Post.metaKeywords"),
      };

      if (IsPost) {
        const isRecruitment = get(seoMeta, "Post.isRecruitment");
        //RecruitmentDetail
        if (isRecruitment) {
          // RecruitmentDetail
          return {
            ...baseSeoMeta,
            title: get(seoMeta, "Post.seoTitle")
              ? `EMS | ${get(seoMeta, "Post.seoTitle")}`
              : t("recruitment-seo-title"),
          };
        }
        // NewsDetail
        return {
          ...baseSeoMeta,
          type: "article",
        };
      }
      //Recruitment
      if (IsRecruitment && CategoryChild) {
        return {
          title: `EMS | ${get(seoMeta, "Category.name")}`,
          description: get(seoMeta, "Category.name"),
          url: get(seoMeta, "Category.url"),
        };
      }
      // NotFound
      if (IsRecruitment) {
        return {
          title: t("not-found-title"),
          description: t("not-found-title"),
        };
      }

      // NewsCategory
      if (IsNews && CategoryChild) {
        return {
          title: `EMS | ${get(seoMeta, "Category.name")}`,
          description: get(seoMeta, "Category.name"),
          url: get(seoMeta, "Category.url"),
        };
      }

      // NewsSubCategory
      if (IsNews) {
        return {
          title: `EMS | ${get(seoMeta, "Category.name")}`,
          description: get(seoMeta, "Category.name"),
          url: get(seoMeta, "Category.url"),
        };
      }

      //ServicePage
      if (IsServiceCategory) {
        return {
          title: `EMS | ${get(seoMeta, "Service.name")}`,
          description: get(seoMeta, "Service.description"),
          url: get(seoMeta, "Service.url"),
        };
      }

      // IsServicePost
      if (IsServicePost) {
        const serviceStatus = get(seoMeta, "Service.status");
        // UnderConstruction
        if (serviceStatus === 1) {
          return {
            title: t("under-construction-title"),
            description: t("under-construction-title"),
          };
        }
        // NotFound
        else if (serviceStatus === 0) {
          return {
            title: t("not-found-title"),
            description: t("not-found-title"),
          };
        }
        // ServiceDetail
        else {
          return {
            title: get(seoMeta, "Service.seoTitle")
              ? `EMS | ${get(seoMeta, "Service.seoTitle")}`
              : `EMS | ${get(seoMeta, "Service.title")}`,
            description:
              get(seoMeta, "Service.metaDescriptions") ??
              get(seoMeta, "Service.description"),
            url: get(seoMeta, "Service.url"),
            image:
              get(seoMeta, "Service.mediaLinkWebsite") ??
              get(seoMeta, "Service.mediaLinkFacebook"),
            keywords: get(seoMeta, "Service.metaKeywords"),
          };
        }
      }

      const pageStatus = get(seoMeta, "Page.status");
      // UnderConstruction
      if (pageStatus === 1) {
        return {
          title: t("under-construction-title"),
          description: t("under-construction-title"),
        };
      }
      // NotFound
      else if (pageStatus === 0) {
        return {
          title: t("not-found-title"),
          description: t("not-found-title"),
        };
      }
      // SinglePage
      return {
        title: get(seoMeta, "Page.seoTitle")
          ? `EMS | ${get(seoMeta, "Page.seoTitle")}`
          : `EMS | ${get(seoMeta, "Page.title")}`,
        description:
          get(seoMeta, "Page.metaDescriptions") ??
          get(seoMeta, "Page.description"),
        url: get(seoMeta, "Page.url"),
        image:
          get(seoMeta, "Page.mediaLinkWebsite") ??
          get(seoMeta, "Page.mediaLinkFacebook"),
        keywords: get(seoMeta, "Page.metaKeywords"),
        type: "article",
      };
    }

    return { title: t("not-found-title"), description: t("not-found-title") };
  }, [seoMeta]);

  return (
    <>
      <SeoMeta
        title={renderSeoMeta?.title}
        description={renderSeoMeta?.description}
        keywords={renderSeoMeta?.keywords}
        facebook_image={renderSeoMeta?.image}
        url={renderSeoMeta?.url}
        type={renderSeoMeta?.type}
      ></SeoMeta>

      {renderPage()}
    </>
  );
};

export async function getServerSideProps({ query, req }) {
  const pathUrl = query.pages.join("/");
  const langCode = req.language;

  const navigationInfo = await getNavigationByUrl(
    `/${encodeURI(pathUrl)}`,
    langCode
  );
  let navigationData = {};
  if (navigationInfo?.data?.code === 200) {
    navigationData = get(navigationInfo, "data.response");
  } else {
    navigationData = {};
  }

  return {
    props: {
      navigationData,
      namespacesRequired: ["common"],
    },
  };
}

const callMasterPage = (path, appContext, i18n) => {
  const langId = getLangIdByName(appContext, i18n);
  const language = i18n?.language;
  const { data: navigationResData, isValidating } = swrGetNavigationByUrl(
    path,
    language
  );
  const pageInfo = get(navigationResData, "data.response", {});
  const parentCategoryId = get(pageInfo, "Post.categoryId");
  const currentCategoryId = get(pageInfo, "Category.categoryId");
  let dataProps: Object = {};

  let isPostPage = false,
    isCategoryRecruitmentPage = false,
    isCategoryNewsPage = false,
    isNewsPage = false,
    isCategoryServicePage = false;
  if (pageInfo.IsPost) {
    isPostPage = true;
  } else if (pageInfo.IsRecruitment && pageInfo.CategoryChild) {
    isCategoryRecruitmentPage = true;
  } else if (pageInfo.IsNews && pageInfo.CategoryChild) {
    isCategoryNewsPage = true;
  } else if (pageInfo.IsNews) {
    isNewsPage = true;
  } else if (pageInfo.IsServiceCategory) {
    isCategoryServicePage = true;
  }

  // page bai viet, tin tuc
  // pageInfo.IsPost
  const { data: postResData } = swrGetPostByCategoryId(
    isPostPage,
    parentCategoryId,
    langId,
    10
  );
  const relativePostData = get(postResData, "data.response.data", []);
  // pageInfo.IsNews && pageInfo.CategoryChild
  const { data: categoryChildrenData } = swrGetPostChildByCategoryId(
    isCategoryNewsPage,
    currentCategoryId,
    langId
  );
  const { data: postsInTopParentData } = swrGetPostTop(
    isCategoryNewsPage,
    currentCategoryId,
    langId,
    true
  );
  const categoryChildren = get(categoryChildrenData, "data.response", []);
  let postsInTop;
  // pageInfo.IsNews
  const { data: listPostsData, mutate: mutateCategoryNews } =
    swrGetPostByCategoryId(isNewsPage, currentCategoryId, langId, 9);
  const { data: postsInTopData } = swrGetPostTop(
    isNewsPage,
    currentCategoryId,
    langId,
    false
  );
  if (isCategoryNewsPage) {
    postsInTop = get(postsInTopParentData, "data.response.data", []);
  } else if (isNewsPage) {
    postsInTop = get(postsInTopData, "data.response.data", []);
  }
  const listPosts = get(listPostsData, "data.response.data", []);
  const pagingData = {
    totalRecord: get(listPostsData, "data.response.totalRecord", 0),
    totalPage: get(listPostsData, "data.response.totalPage", 0),
    currentPage: 1,
  };

  // page dich vu
  // pageInfo.IsServiceCategory
  const { data: serviceGroupResData } = swrGetServiceCategory(
    isCategoryServicePage,
    langId
  );
  const resData = {
    serviceGroupData: get(serviceGroupResData, "data.response", []),
    namespacesRequired: ["common"],
  };

  // page tuyen dung
  // pageInfo.IsRecruitment && pageInfo.CategoryChild
  const { data: categoryRecruitmentData } = swrGetCategoryByLangId(
    isCategoryRecruitmentPage,
    currentCategoryId,
    langId
  );
  const listProvinces = get(
    categoryRecruitmentData,
    "data.response[0].categoryChild",
    []
  );

  dataProps = {
    langId,
    // data tin tuc
    relativePost: relativePostData,
    postsInTop,
    categoryChildren,
    pagingData: pagingData,
    listPosts,
    mutateCategoryNews,
    // data dich vu
    ...resData,
    // data tuyen dung
    listProvinces,
    isRequestingPageInfo: isValidating,
    pageInfo,
  };

  return dataProps;
};

export default withTranslation("common")(MasterPage);
