import Document, { Html, Head, Main, NextScript } from "next/document";
class MyDocument extends Document {
  static async getInitialProps(ctx: any) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    const liveChatScript = `
			var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq
			|| {widgetcode:"52836bc2821fcef054f17d0119d1142f8e85d6d116107614c3ae329d43235ae84f491d2f674f2b1f50d72b8611282694",
			values:{},ready:function(){}};var d=document;s=d.createElement("script");s.type="text/javascript";s.id="zsiqscript";s.defer=true;
			s.src="https://salesiq.zoho.com/widget";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);d.write("<div id='zsiqwidget'></div>");
		`;

    const contentLiveChat = `
			var zsFeedbackTabPref = {};
			zsFeedbackTabPref.tabTitle = "Phiếu phản hồi";
			zsFeedbackTabPref.tabColor = "#F8AF12";
			zsFeedbackTabPref.tabFontColor = "#FFFFFF";
			zsFeedbackTabPref.tabPosition = "Left";
			zsFeedbackTabPref.tabOffset = "215";
			zsFeedbackTabPref.display = "popout";
			zsFeedbackTabPref.srcDiv = "zsfeedbackwidgetdiv";
			zsFeedbackTabPref.defaultDomain = "https://desk.zoho.com";
			zsFeedbackTabPref.feedbackId = "ad213b0140b45dbdaeda7d40709bfb45afb6719abeecaa48";
			zsFeedbackTabPref.fbURL = "https://desk.zoho.com/support/fbw?formType=AdvancedWebForm&fbwId=ad213b0140b45dbdaeda7d40709bfb45afb6719abeecaa48&xnQsjsdp=MhDeogiRcNfbsy-V*kINXg$$&mode=showWidget&displayType=popout";
			zsFeedbackTabPref.jsStaticUrl = "https://js.zohostatic.com/support/fbw_v8/js";
			zsFeedbackTabPref.cssStaticUrl = "https://css.zohostatic.com/support/fbw_v8/css";

			var feedbackInitJs = document.createElement("script");
			feedbackInitJs.type = "text/javascript";
			feedbackInitJs.src = "https://js.zohostatic.com/support/fbw_v8/js/zsfeedbackinit.js";
			window.jQueryAndEncoderUrl = "https://js.zohostatic.com/support/app/js/jqueryandencoder.ffa5afd5124fbedceea9.js";
			window.loadingGifUrl = "https://img.zohostatic.com/support/app/images/loading.8f4d3630919d2f98bb85.gif";
			// document.head.appendChild(feedbackInitJs);
			var feedbackWidgetCss = document.createElement("link");
			feedbackWidgetCss.setAttribute("rel", "stylesheet");
			feedbackWidgetCss.setAttribute("type", "text/css");
			feedbackWidgetCss.setAttribute("href", "https://css.zohostatic.com/support/app/css/ZSFeedbackPopup.fe4fc024c068cf8f2f83.css");
			document.head.appendChild(feedbackWidgetCss);
		`;

    return (
      <Html>
        <Head>
          <script
            async
            src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML"
          />

          {/* Google tag (gtag.js) */}
          <script
            async
            src="https://www.googletagmanager.com/gtag/js?id=G-QYDFL1WMGM"
          ></script>
          <script
            dangerouslySetInnerHTML={{
              __html: `
			  window.dataLayer = window.dataLayer || [];
			  function gtag(){dataLayer.push(arguments);}
			  gtag('js', new Date());

			  gtag('config', 'G-QYDFL1WMGM');
						`,
            }}
          />
        </Head>
        <body>
          <Main />
          <NextScript />
          <script dangerouslySetInnerHTML={{ __html: liveChatScript }} />
          <script dangerouslySetInnerHTML={{ __html: contentLiveChat }} />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
