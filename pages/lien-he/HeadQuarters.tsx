import React from "react";
import { get } from "lodash";
import iconCompany from "../../public/img/contact/company.png";

function HeadQuarters({ quarter, t }) {
  return (
    <div className="address-contact-item">
      <div className="img-top-contact">
        <img src={iconCompany} alt="icon company" />
      </div>
      <div className="company-name-contact">{get(quarter, "name", "")}</div>
      <ul className="list-address">
        <li className="item-address">{get(quarter, "address", "")}</li>
        <li className="item-address">
          {t("contact-phone-text")}:{" "}
          <a href={"tel:" + get(quarter, "phone", "")}>
            {get(quarter, "phone", "")}
          </a>
        </li>
        <li className="item-address">
          {t("contact-email-text")}:{" "}
          <a href={"mailto:" + get(quarter, "email", "")}>
            {get(quarter, "email", "")}
          </a>
        </li>
      </ul>
    </div>
  );
}
export default HeadQuarters;
