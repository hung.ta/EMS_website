import React, { useState, useRef, useEffect } from "react";
import { Col, Container, Form, Row, Spinner } from "react-bootstrap";
import _, { get, isEmpty } from "lodash";
import { message } from "antd";
import { withTranslation } from "i18n";

import { BreadCrumb, SeoMeta } from "components";
import { contactUs } from "services/contact.service";
import { formContact } from "interfaces/contact.interface";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import HeadQuarters from "./HeadQuarters";
import SupportOnline from "./SupportOnline";

import imgLeft from "../../public/img/contact/img-contact1.png";
import iconEmail from "../../public/img/contact/email-send.png";
import Slider from "react-slick";

import "./Contact.module.css";
import { useAppContext } from "contexts/app";
import { fetchHeadQuarter } from "services/common.service";
import { getLangIdByName } from "utils/helper";

const initFormValue: formContact = {
  name: "",
  email: "",
  content: "",
  displayOrder: 0,
  phoneNumber: "",
};

const Contact: any = ({ t, i18n }) => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const slideRef: any = useRef({});
  const [formValues, setFormValues] = useState<formContact>(initFormValue);
  const [isSpinner, setIsSpinner] = useState<boolean>(false);

  // client fetching headquarter info
  const appContext = useAppContext();
  const langId = getLangIdByName(appContext, i18n);
  const { data: headQuartersData } = fetchHeadQuarter(langId);
  const headQuarters = get(headQuartersData, "data.response.data", []);

  const settingSlide = {
    dots: false,
    infinite: Array.isArray(headQuarters) && headQuarters.length >= 4,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    initialSlide: 0,
    autoplay: true,
    centerPadding: "50px",
    responsive: [
      {
        breakpoint: 1150,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 0,
          infinite: Array.isArray(headQuarters) && headQuarters.length >= 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 0,
        },
      },
    ],
  };

  const employeeNameList = _.compact(_.map(headQuarters, "employeeName"));
  const settingSlide2 = {
    dots: false,
    infinite: Array.isArray(employeeNameList) && employeeNameList.length >= 3,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    initialSlide: 0,
    autoplay: true,
    centerPadding: "50px",
    responsive: [
      {
        breakpoint: 1150,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          initialSlide: 0,
          infinite:
            Array.isArray(employeeNameList) && employeeNameList.length >= 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 0,
        },
      },
    ],
  };

  // handle submit form
  const handleSubmitForm = async (e: any) => {
    e.preventDefault();
    setIsSpinner(true);
    let bodyForm = Object.assign({}, formValues);
    const res = await contactUs(bodyForm);
    if (res) {
      message.success(t("contact-send-success-text"));
      setIsSpinner(false);
      setFormValues(initFormValue);
    } else {
      setIsSpinner(false);
    }
    setTimeout(() => {
      setIsSpinner(false);
      setFormValues(initFormValue);
    }, 1000);
  };

  // onchange form
  const onChangeForm = (e: any) => {
    const { name, value } = e.target;

    setFormValues({
      ...formValues,
      [name]: value,
    });
  };

  return (
    <div className="wr-contact">
      <SeoMeta title={t("contact-title-seo-text")} />
      <BreadCrumb>
        <Breadcrumb.Item active>
          <span>{t("contact-title-text")}</span>
        </Breadcrumb.Item>
      </BreadCrumb>
      <Container fluid={"lg"}>
        {/* contact address */}
        <div className="content-contact">
          <div className="address-contact">
            <Slider ref={slideRef} {...settingSlide}>
              {Array.isArray(headQuarters) &&
                headQuarters.map((quarter, key) => {
                  return <HeadQuarters t={t} quarter={quarter} key={key} />;
                })}
            </Slider>
          </div>

          {/* form contact */}
          <Row>
            <Col xs={12} lg={6}>
              <div className="contact__left-form">
                <img
                  className="contact__left-form-img"
                  src={imgLeft}
                  alt="img contact"
                />
                <div className="contact__left-form-title">
                  {t("contact-support-text")}
                </div>
                <div className="contact__left-form-personal">
                  <Slider {...settingSlide2}>
                    {Array.isArray(headQuarters) &&
                      headQuarters.map((quarter, key) => {
                        if (!isEmpty(quarter.employeeName)) {
                          return <SupportOnline key={key} quarter={quarter} />;
                        }
                      })}
                  </Slider>
                </div>
              </div>
            </Col>
            <Col xs={12} lg={6}>
              <div className="contact__right-form">
                <div className="contact__right-form-title">
                  <img
                    className="contact__right-form-title-icon"
                    src={iconEmail}
                    alt="icon email"
                  />
                  <div className="contact__right-form-title-title">
                    {t("contact-to-me-text")}
                  </div>
                </div>
                <Form className="form-contact" onSubmit={handleSubmitForm}>
                  <Form.Group>
                    <Form.Control
                      required
                      size="lg"
                      type="text"
                      placeholder={t("contact-your-name-text")}
                      className="input-custom"
                      name="name"
                      value={formValues.name}
                      onChange={onChangeForm}
                      autoComplete="off"
                    />
                  </Form.Group>
                  <Form.Group>
                    <Form.Control
                      required
                      size="lg"
                      type="email"
                      placeholder={t("contact-your-email-text")}
                      className="input-custom"
                      name="email"
                      value={formValues.email}
                      onChange={onChangeForm}
                      autoComplete="off"
                    />
                  </Form.Group>
                  <Form.Group>
                    <Form.Control
                      required
                      size="lg"
                      as="textarea"
                      placeholder={t("contact-your-message-text")}
                      className="input-custom area"
                      name="content"
                      value={formValues.content}
                      onChange={onChangeForm}
                      autoComplete="off"
                    />
                  </Form.Group>
                  <button type="submit">
                    {t("contact-send-request-text") + " "}
                    {isSpinner && (
                      <Spinner animation="border" className="ml-3" />
                    )}
                  </button>
                </Form>
              </div>
            </Col>
          </Row>
        </div>
      </Container>
    </div>
  );
};

export default withTranslation("common")(Contact);
