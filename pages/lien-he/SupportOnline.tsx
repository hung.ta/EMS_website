import React, { Fragment } from "react";
import { get, toString } from "lodash";
import iconZalo from "../../public/img/contact/icon-zalo.png";

function SupportOnline({ quarter }) {
  return (
    <Fragment>
      <a
        href={`http://zalo.me/${toString(get(quarter, "skyInfo")) ?? ""}`}
        target="_blank"
        className="link_skype"
      >
        <div className="personal-contact d-flex align-items-center">
          <img className="img-skype" src={iconZalo} alt="icon zalo" />
          <div>
            <div className="name-contact-skype">
              {get(quarter, "employeeName") ?? ""}
            </div>
            <p className="des-contact-skype">{get(quarter, "duty") ?? ""}</p>
          </div>
        </div>
      </a>
    </Fragment>
  );
}
export default SupportOnline;
