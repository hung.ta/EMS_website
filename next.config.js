const withImages = require('next-images');
const withFonts = require('next-fonts');
const withCSS = require('@zeit/next-css');
const path = require('path');

const { nextI18NextRewrites } = require('next-i18next/rewrites');

module.exports = withFonts(
	withCSS(
		withImages({
			rewrites: async () => nextI18NextRewrites(),
			publicRuntimeConfig: {},
			env: {
				SSO_URL: process.env.SSO_URL,
				APP_STORE: process.env.APP_STORE,
				GG_PLAY: process.env.GG_PLAY
			},
			webpack: (config) => {
				config.resolve.modules.push(path.resolve("./"));
				if (config.optimization.splitChunks) {
					config.optimization.splitChunks.cacheGroups.shared = {
						name: 'app-other',
						test: /\.css$/,
						chunks: 'all',
						enforce: true
					};
				}
				// config.resolve.modules.push(path.resolve('./'));
				
				return config;
			}
		})
	)
);
