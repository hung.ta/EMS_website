FROM node

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH

# install app dependencies
COPY package.json ./
COPY package-lock.json ./
RUN npm install 
# add app
COPY . ./

# start app
CMD ["npm", "run", "start"]