## Website EMS

## Get start

```bash
# clone the repo
$ git clone git@bitbucket.org:vmodev/ptl294-portal_website.git my-project

# go into app's directory
$ cd my-project

# install app's dependencies
$ npm install

```

### Basic usage

```bash
# start the project:
$ npm run dev
```


Navigate to [http://localhost:3000](http://localhost:3000). The app will automatically reload if you change any of the source files.

### Build

Run `build` to build the project. The build artifacts will be stored in the `.next/` directory.

```bash
# build the project:
$ npm run build
```
