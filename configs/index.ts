export const baseURL = process.env.BASE_URL_API || "https://admin.ems.com.vn";
export const EMS_API_URL = process.env.EMS_API_URL || "https://api.myems.vn";
export const ADMIN_SITE_URL =
  process.env.ADMIN_SITE_URL ||
  "http://ec2-54-254-62-199.ap-southeast-1.compute.amazonaws.com:4000";
export const API_GG_MAP =
  process.env.API_GG_MAP || "AIzaSyAopD0MUJx17QeB4z-0Z3MJV7cHpuNbiwg";
export const CX_GOOGLE_KEY = process.env.CX_GOOGLE_KEY || "c3b5be89ab484bed0";
export const DOMAIN_NAME = process.env.DOMAIN_NAME || "https://ems.com.vn";
export const defaultBannerImgUrl = "https://i.imgur.com/xLuYgb1.jpg";
export const img_facebook_default = "https://i.imgur.com/JL7knW3.png";
