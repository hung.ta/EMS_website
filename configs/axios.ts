/* eslint-disable func-names */
import axios from 'axios';
import { baseURL } from './index';

const config = {
	baseURL: baseURL,
	validateStatus(status: any) {
		return (
			(status >= 200 && status < 300) ||
			status === 400 ||
			status === 404 ||
			status === 405 ||
			status === 409 ||
			status === 429 ||
			status === 500 ||
			status === 503
		);
	}
};
const _axios = axios.create(config);

axios.interceptors.request.use(
	function(config) {
		return config;
	},
	function(error) {
		return Promise.reject(error);
	}
);

axios.interceptors.response.use(
	function(response) {
		return response;
	},
	function(error) {
		return Promise.reject(error);
	}
);

export default _axios;
