import axios, { AxiosRequestConfig } from "axios";
import useSWR, { mutate, ConfigInterface } from "swr";
import { baseURL } from "./index";

export const getRequest: AxiosRequestConfig = {
  baseURL,
  method: "GET",
};

const axiosFetcher = (url) => axios({ ...getRequest, url });

const configModified: ConfigInterface = {
  revalidateOnFocus: false,
  shouldRetryOnError: false,
  dedupingInterval: 60000,
  revalidateOnReconnect: false,
  refreshWhenOffline: false,
  refreshWhenHidden: false,
  refreshInterval: 0,
};

export const useSWRAxios = (path, options) => {
  return useSWR(path, axiosFetcher, {
    ...configModified,
    ...options,
  });
};

export const useSWRCustom = (path, customFetcher, options) => {
  return useSWR(path, customFetcher, {
    ...configModified,
    ...options,
  });
};

export const mutateSWR = (path, cb, isReval = true) => {
  return mutate(`${baseURL}${path}`, cb, isReval);
};
