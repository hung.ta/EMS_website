export const LIST_STATIC_PAGE = [
  "/lien-he",
  "/quan-he-co-dong",
  "/thong-tin-gui-hang-quoc-te",
  "/tim-kiem",
  "/tra-cuu",
  "/tra-cuu/tra-cuu-buu-cuc",
  "/tra-cuu/tra-cuu-buu-gui",
  "/tra-cuu/uoc-tinh-cuoc",
];

export const INFO_REGISTER_PATH = "/dang-ky-thong-tin";
