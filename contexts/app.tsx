import React, { useContext, useState, useEffect } from "react";
import { get } from "lodash";
import axios from "axios";

import { get as axiosGet } from "services/base.service";
export const AppContext = React.createContext(null);

export const AppProvider: any = ({ children, data = {} }) => {
  const [isShowHeaderSideBar, setIsShowHeaderSideBar] = useState(false);
  const [isShowPopupRegister, setIsShowPopupRegister] = useState(null);
  const [companyInfo, setCompanyInfo] = useState(get(data, "companyInfo"));
  const [menuInfo, setMenuInfo] = useState(get(data, "menuInfo"));

  useEffect(() => {
    let overflow = "";
    if (isShowHeaderSideBar) {
      overflow = "hidden";
    }
    document.getElementsByTagName("body") &&
      (document.getElementsByTagName("body")[0].style.overflow = overflow);
    document.getElementsByTagName("html") &&
      (document.getElementsByTagName("html")[0].style.overflow = overflow);
  }, [isShowHeaderSideBar]);

  const changeContextStateWithLanguages = async (langId, langCode) => {
    // home config
    const data = await axios
      .all([
        axiosGet(`/api/company?langId=${langId}`),
        axiosGet(`/api/menu/getmenubylangid?langId=${langId}`),
      ])
      .then(function (data) {
        const companyInfo = get(data, "[0].data.response.data.[0]", null);
        const menuInfo = get(data, "[1].data.response", []);
        setCompanyInfo(companyInfo);
        setMenuInfo(menuInfo);
      });
  };

  return (
    <AppContext.Provider
      value={{
        ...data,
        isShowPopupRegister,
        setIsShowPopupRegister,
        isShowHeaderSideBar,
        setIsShowHeaderSideBar,
        companyInfo,
        menuInfo,
        changeContextStateWithLanguages,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

export const useAppContext = () => useContext(AppContext);
