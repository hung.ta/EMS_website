import React, { useContext, useState } from "react";
import { useAccordionToggle, AccordionContext } from "react-bootstrap";
import iconDown from "public/img/icon/drop-down.png";
import iconDownMobile from "public/img/icon/drop-down-down-mobile.png";
import iconUp from "public/img/icon/drop-down-up-white.png";
import iconUpMobile from "public/img/icon/drop-down-up-mobile.png";

const ContextAwareToggle = ({ children, eventKey, callback }: any) => {
  const currentEventKey = useContext(AccordionContext);
  const isCurrentEventKey = currentEventKey === eventKey;
  const [isHover, setIsHover] = useState(false);

  const decoratedOnClick = useAccordionToggle(
    eventKey,
    () => callback && callback(eventKey)
  );

  return (
    <div
      className={`accordion-title d-flex justify-content-between align-items-center ${isCurrentEventKey ? "hover1" : ""
        }`}
      onClick={decoratedOnClick}
      onMouseOver={e => setIsHover(true)}
      onMouseOut={e => setIsHover(false)}
    >
      <div className="text-content">{children}</div>
      {isCurrentEventKey ? (
        <React.Fragment>
          <img src={iconUp} alt="" className="hidden-xs foo10" />
          <img src={iconUpMobile} alt="" className="visible-xs foo11" />
        </React.Fragment>
      ) : (
          <React.Fragment>
            {isHover ? (
              <React.Fragment>
                <img src={iconDown} alt="" className="hidden-xs foo20" />
                <img src={iconDownMobile} alt="" className="visible-xs foo12" />
              </React.Fragment>
            ) : (
                <React.Fragment>
                  <img src={iconDown} alt="" className="hidden-xs foo21" />
                  <img src={iconDownMobile} alt="" className="visible-xs foo13" />
                </React.Fragment>
              )}
          </React.Fragment>
        )}
    </div>
  );
};

export default ContextAwareToggle;
