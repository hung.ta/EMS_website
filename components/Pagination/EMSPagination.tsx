import React, { Fragment } from "react";

import "./EMSPagination.module.css";

const EMSPagination = ({ currentPage = 1, totalPage = 1, handleClickLink }) => {
  const showPagination = () => {
    let pageHtml = [];
    if (totalPage <= 1) return;

    if (currentPage > 1) {
      pageHtml.push(
        <li
          key={"li-0"}
          onClick={() => handleClickLink && handleClickLink(currentPage - 1)}
        >
          <a className="page-numbers">&lt;</a>
        </li>
      );
    }
    if (totalPage >= 2) {
      for (let i = 1; i <= totalPage; i++) {
        pageHtml.push(
          <Fragment key={i}>
            {currentPage == i ? (
              <li key={`li-${i}`}>
                <span aria-current="page" className="page-numbers current">
                  {i}
                </span>
              </li>
            ) : (
              <li
                key={`li-${i}`}
                onClick={() => handleClickLink && handleClickLink(i)}
              >
                <a className="page-numbers">{i}</a>
              </li>
            )}
          </Fragment>
        );
      }
    }
    if (currentPage < totalPage) {
      pageHtml.push(
        <li
          key={"li-100"}
          onClick={() => handleClickLink && handleClickLink(currentPage + 1)}
        >
          <a className="page-numbers">&gt;</a>
        </li>
      );
    }
    return pageHtml;
  };

  return <ul className="front-pagination">{showPagination()}</ul>;
};

export default EMSPagination;
