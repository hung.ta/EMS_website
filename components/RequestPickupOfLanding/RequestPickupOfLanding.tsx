import React from "react";
import Link from "next/link";
import "./RequestPickupOfLanding.module.css";
import { withTranslation } from "i18n";

const RequestPickupOfLanding = ({ t }) => {
  return (
    <div className="request-pickup-of-landing">
      <Link href="https://bill.ems.com.vn/pickup-request">
        <a target="_blank">{t("request_pickup")}</a>
      </Link>
    </div>
  );
};

RequestPickupOfLanding.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

export default withTranslation()(RequestPickupOfLanding);
