import { useEffect, useState } from "react";
import "./Fab.module.css";
import imgPlus from "public/img/icon/plus-mobile.png";
import imgMinus from "public/img/icon/minus-mobile.png";

function Fab({ isMobile }) {
  const [isActive, setIsActive] = useState(false);
  const divPhone = document.getElementById("phone_number");
  useEffect(() => {
    const divChat = document.getElementsByClassName("zsiq_flt_rel")[0] as any;
    if (isMobile) {
      divPhone && (divPhone.style.display = "none");
      divChat && (divChat.style.display = "none");
      setIsActive(false);
    } else {
      divPhone && (divPhone.style.display = "block");
      divChat && (divChat.style.display = "block");
    }
  }, [isMobile]);

  const handleOnClick = () => {
    const divChat = document.getElementsByClassName("zsiq_flt_rel")[0] as any;
    if (divChat) {
      setIsActive(!isActive);

      if (divPhone.style.display === "block") {
        divPhone.style.display = "none";
      } else {
        divPhone.style.display = "block";
      }

      if (divChat.style.display === "block") {
        divChat.style.display = "none";
      } else {
        divChat.style.display = "block";
      }
    }
  };

  return (
    <div
      className={`fab-container ${isMobile ? "fab-mobile" : "fab-deMobile"}`}
      onClick={handleOnClick}
    >
      {isActive ? (
        <img src={imgMinus} alt="icon fab" />
      ) : (
        <img src={imgPlus} alt="icon fab" />
      )}
    </div>
  );
}

export default Fab;
