import React, { Fragment } from "react";
import { withTranslation } from "i18n";

import { HeaderTop, HeaderBottom, HeaderSidebar } from "./components";
import "./Header.module.css";

const Header: any = () => {
  return (
    <Fragment>
      <HeaderTop />
      <HeaderBottom>
        <HeaderSidebar />
      </HeaderBottom>
    </Fragment>
  );
};

Header.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

export default withTranslation()(Header);
