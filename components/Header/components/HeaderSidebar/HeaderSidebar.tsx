import React, { useEffect, memo, Fragment } from "react";
import Link from "next/link";
import { get, size, toString } from "lodash";

import { withTranslation } from "i18n";
import { useAppContext } from "contexts/app";
import { LIST_STATIC_PAGE } from "configs/constants";
import { getHrefByPath } from "utils/helper";
import "./HeaderSidebar.module.css";

const HeaderSidebar: any = ({ t }) => {
  const appContext = useAppContext();
  const menuInfo = get(appContext, "menuInfo", {});

  useEffect(() => {
    // hide header sidebar click outside
    addEventListenerFunc("body", handleHideSideBar);
    // click redirect
    addEventListenerFunc(".content-sidebar a[href]", hideHeaderSidebar);
    addEventListenerFunc(
      ".content-sidebar .content-sidebar-label",
      handleToggleShowList
    );
    // toggle show
    addEventListenerFunc(
      ".content-sidebar .content-sidebar-label",
      handleToggleShowList
    );
    return () => {
      removeEventListenerFunc("body", handleHideSideBar);
      removeEventListenerFunc(".content-sidebar a[href]", hideHeaderSidebar);
      removeEventListenerFunc(
        ".content-sidebar .content-sidebar-label",
        handleToggleShowList
      );
      removeEventListenerFunc(
        ".content-sidebar .content-sidebar-label",
        handleToggleShowList
      );
    };
  }, []);

  const hideHeaderSidebar = (e) => {
    var menuLi = document.getElementsByClassName("menu-button-show")[0];
    if (menuLi && !menuLi.contains(e.target)) {
      appContext.setIsShowHeaderSideBar(false);
    }
  };

  const handleHideSideBar = (e) => {
    var sidebarEle = document.getElementById("content-sidebar");
    if (sidebarEle && sidebarEle.contains(e.target)) {
      // Clicked in box
    } else {
      hideHeaderSidebar(e);
    }
  };

  const handleToggleShowList = (event) => {
    const thisEle = event.target;
    const nodeName = toString(get(thisEle, "nodeName", "")).toLowerCase();

    if (nodeName == "img") {
      const parentElement = get(thisEle, "parentElement.parentElement");
      parentElement &&
        parentElement.nextSibling &&
        parentElement.nextSibling.classList.toggle("show");
      parentElement && parentElement.classList.toggle("show");
      return;
    } else if (nodeName == "span") {
      const parentElement = get(thisEle, "parentElement");
      parentElement &&
        parentElement.nextSibling &&
        parentElement.nextSibling.classList.toggle("show");
      parentElement && parentElement.classList.toggle("show");
      return;
    }
    thisEle &&
      thisEle.nextSibling &&
      thisEle.nextSibling.classList.toggle("show");
    thisEle && thisEle.classList.toggle("show");
  };

  const addEventListenerFunc = (querySel, callBack) => {
    var elements = document.querySelectorAll(querySel);
    for (var i = 0; i < elements.length; i++) {
      elements[i].addEventListener("click", callBack, false);
    }
  };
  const removeEventListenerFunc = (querySel, callBack) => {
    var elements = document.querySelectorAll(querySel);
    for (var i = 0; i < elements.length; i++) {
      elements[i].addEventListener("click", callBack, false);
    }
  };

  return (
    <div
      id="sidebar-header"
      className={`sidebar-header ${
        appContext.isShowHeaderSideBar ? "show" : ""
      }`}
    >
      <div className="faded-sidebar"></div>
      <div id="content-sidebar" className="content-sidebar">
        <ul>{generateMenu(menuInfo, 1)}</ul>
      </div>
    </div>
  );
};

function generateMenu(menuInfo, level) {
  if (!Array.isArray(menuInfo) || menuInfo.length == 0) return;
  if (level > 3) return;

  level++;
  return (
    <Fragment>
      {menuInfo.map((menu, menuIndex) => {
        const menuObject = get(menu, "menu", {});
        const menuChild = get(menu, "menuChild", []);
        const isHasChild = size(menuChild) > 0 && level <= 3;

        return (
          <li key={`content-sidebar__item-${menuIndex}`}>
            {isHasChild ? (
              <a className="content-sidebar-label">
                {get(menuObject, "name", "")}
                <span>
                  <img src="/img/icon/arrow.png" alt="icon arrow" />
                </span>
              </a>
            ) : (
              <Link
                as={toString(get(menuObject, "url", ""))}
                href={getHrefByPath(toString(get(menuObject, "url", "")))}
                // shallow
              >
                <a>
                  {get(menuObject, "name", "")}
                  <span>
                    <img src="/img/icon/arrow-right.png" alt="arrow-right" />
                  </span>
                </a>
              </Link>
            )}
            <div className="sidebar-sub-menu">
              {isHasChild && (
                <ul className="sidebar-sub-menu__ul">
                  {generateMenu(menuChild, level)}
                </ul>
              )}
            </div>
          </li>
        );
      })}
    </Fragment>
  );
}

HeaderSidebar.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

export default withTranslation()(memo(HeaderSidebar));
