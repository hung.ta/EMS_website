import React, { memo } from "react";
import { Container, Dropdown } from "react-bootstrap";

import { get, toString } from "lodash";

import { useAppContext } from "contexts/app";
import { withTranslation, i18n } from "i18n";
import "./HeaderTop.module.css";

const HeaderTop: any = ({ t }) => {
  const appContext = useAppContext();
  const companyInfo = get(appContext, "companyInfo", {});
  const languages = get(appContext, "languages", []);

  const changeLanguageWebsite = (langId, langCode, e) => {
    i18n.changeLanguage(langCode);
    appContext.changeContextStateWithLanguages(langId, langCode);
  };
  return (
    <div className="top-header">
      <Container fluid={"lg"}>
        <div className="wrapper-top-header">
          <div className="left-header">
            <span className="left-header__label">Hotline:&nbsp;</span>
            <span className="hotline1">{get(companyInfo, "hotline", "")}</span>
          </div>
          <div className="right-header">
            <div className="right-header__label">{t("change-locale")}:</div>
            <Dropdown>
              <Dropdown.Toggle
                id="dropdown-basic"
                className="dropdown-toggle wr_dropdown_language"
              >
                {t("language")}
                <i className="arrow-menu down"></i>
              </Dropdown.Toggle>
              <Dropdown.Menu>
                {Array.isArray(languages) &&
                  languages.map((item, key) => {
                    return (
                      <Dropdown.Item
                        key={key}
                        onClick={changeLanguageWebsite.bind(
                          this,
                          get(item, "id"),
                          toString(get(item, "code")).toLowerCase()
                        )}
                      >
                        {item.name}
                      </Dropdown.Item>
                    );
                  })}
              </Dropdown.Menu>
            </Dropdown>
            <div className="action-form">
              <a href={process.env.SSO_URL} target="_blank">
                {t("signin")}
              </a>
            </div>
          </div>
        </div>
      </Container>
    </div>
  );
};

HeaderTop.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

export default withTranslation()(memo(HeaderTop));
