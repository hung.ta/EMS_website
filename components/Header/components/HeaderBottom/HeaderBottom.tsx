import React, { memo } from "react";
import { Container } from "react-bootstrap";
import Link from "next/link";

import { get, toString, size } from "lodash";
import { useAppContext } from "contexts/app";
import { withTranslation } from "i18n";

import "./HeaderBottom.module.css";
import { baseURL } from "configs";
import { getHrefByPath } from "utils/helper";

const HeaderBottom: any = ({ children }) => {
  const appContext = useAppContext();
  const companyInfo = get(appContext, "companyInfo", {});
  const menuInfo = get(appContext, "menuInfo", {});

  const toggleHeaderSidebar = () => {
    appContext.setIsShowHeaderSideBar(!appContext.isShowHeaderSideBar);
  };

  return (
    <div className="bottom-header">
      {children}
      <Container fluid={"lg"}>
        <div className="wrapper-bottom-header">
          <div className="left-header">
            <Link as="/" href="/" shallow>
              <a>
                <img
                  className="no-select"
                  src={
                    baseURL + encodeURI(toString(get(companyInfo, "logo", "")))
                  }
                  alt="logo"
                />
              </a>
            </Link>
          </div>
          <div className="right-header">
            <ul className="menu">
              {Array.isArray(menuInfo) &&
                menuInfo.map((menu, menuKey) => {
                  return menuGenerateHtml(menu, menuKey);
                })}
              <li className="menu-button-search">
                <a href="/tim-kiem">
                  <svg
                    width="16"
                    height="18"
                    viewBox="0 0 16 18"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M13.3328 13.7294L10.6211 10.8799C11.3183 10.0425 11.7003 8.98884 11.7003 7.89196C11.7003 5.32919 9.63663 3.24414 7.10015 3.24414C4.56367 3.24414 2.5 5.32919 2.5 7.89196C2.5 10.4547 4.56367 12.5398 7.10015 12.5398C8.05238 12.5398 8.95981 12.2496 9.73563 11.6987L12.4679 14.5699C12.5821 14.6897 12.7357 14.7558 12.9003 14.7558C13.0561 14.7558 13.2039 14.6958 13.3162 14.5866C13.5546 14.3548 13.5622 13.9705 13.3328 13.7294ZM7.10015 4.45661C8.97501 4.45661 10.5003 5.99767 10.5003 7.89196C10.5003 9.78624 8.97501 11.3273 7.10015 11.3273C5.22529 11.3273 3.70004 9.78624 3.70004 7.89196C3.70004 5.99767 5.22529 4.45661 7.10015 4.45661Z"
                      fill="#313131"
                    />
                  </svg>
                </a>
              </li>
              <li
                className="menu-button-show d-lg-none"
                onClick={toggleHeaderSidebar}
              >
                <span>
                  <svg
                    width="16"
                    height="18"
                    viewBox="0 0 16 18"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M14.2209 14.1158H1.77648C1.6586 14.1158 1.54556 14.0668 1.46221 13.9796C1.37886 13.8923 1.33203 13.774 1.33203 13.6507C1.33203 13.5273 1.37886 13.409 1.46221 13.3218C1.54556 13.2345 1.6586 13.1855 1.77648 13.1855H14.2209C14.3388 13.1855 14.4518 13.2345 14.5352 13.3218C14.6185 13.409 14.6654 13.5273 14.6654 13.6507C14.6654 13.774 14.6185 13.8923 14.5352 13.9796C14.4518 14.0668 14.3388 14.1158 14.2209 14.1158Z"
                      fill="#313131"
                    />
                    <path
                      d="M14.2209 9.46344H1.77648C1.6586 9.46344 1.54556 9.41443 1.46221 9.32721C1.37886 9.23998 1.33203 9.12168 1.33203 8.99832C1.33203 8.87496 1.37886 8.75666 1.46221 8.66943C1.54556 8.58221 1.6586 8.5332 1.77648 8.5332H14.2209C14.3388 8.5332 14.4518 8.58221 14.5352 8.66943C14.6185 8.75666 14.6654 8.87496 14.6654 8.99832C14.6654 9.12168 14.6185 9.23998 14.5352 9.32721C14.4518 9.41443 14.3388 9.46344 14.2209 9.46344Z"
                      fill="#313131"
                    />
                    <path
                      d="M14.2209 4.81304H1.77648C1.6586 4.81304 1.54556 4.76404 1.46221 4.67682C1.37886 4.58959 1.33203 4.47129 1.33203 4.34793C1.33203 4.22457 1.37886 4.10627 1.46221 4.01904C1.54556 3.93182 1.6586 3.88281 1.77648 3.88281H14.2209C14.3388 3.88281 14.4518 3.93182 14.5352 4.01904C14.6185 4.10627 14.6654 4.22457 14.6654 4.34793C14.6654 4.47129 14.6185 4.58959 14.5352 4.67682C14.4518 4.76404 14.3388 4.81304 14.2209 4.81304Z"
                      fill="#313131"
                    />
                  </svg>
                </span>
              </li>
            </ul>
          </div>
        </div>
      </Container>
    </div>
  );
};

function menuGenerateHtml(menuItem: any, menuLiKey: any) {
  const menu = get(menuItem, "menu");

  const url = toString(get(menu, "url", "#"));
  const name = toString(get(menu, "name", ""));

  const menuChild = get(menuItem, "menuChild", []);
  const isHasChild = Array.isArray(menuChild) && menuChild.length > 0;
  const isHasLeastTwoChild = size(menuChild) < 3;
  let isHasChildOfChild = false;
  if (isHasChild) {
    menuChild.map((m: any) => {
      if (size(get(m, "menuChild", [])) > 0) {
        isHasChildOfChild = true;
      }
    });
  }

  return (
    <li
      key={`menu__item-${menuLiKey}`}
      className={
        isHasChildOfChild
          ? `menu__item--multiple-item ${
              isHasLeastTwoChild ? "menu__item--multiple-item-least-two" : ""
            }`
          : ""
      }
    >
      {url ? (
        <Link
          as={toString(url)}
          href={getHrefByPath(toString(url))}
          // shallow
        >
          <a>
            {name} {isHasChild && <i className="arrow down" />}
          </a>
        </Link>
      ) : (
        <span>
          {name} {isHasChild && <i className="arrow down" />}
        </span>
      )}
      {isHasChild && (
        <div className="sub-menu">
          {isHasChildOfChild ? (
            <Container>
              <div className="sub-menu-content d-flex justify-content-center">
                {menuChild.map((menu: any, menuKey: any) => {
                  const mChild = get(menu, "menuChild", []);
                  return (
                    <div
                      className="sub-menu-item sub-menu-item-multiple"
                      key={`sub-menu-content-${get(
                        menu,
                        "menu.name",
                        ""
                      )}-${menuKey}`}
                    >
                      <h3>
                        <Link
                          as={toString(get(menu, "menu.url", "/"))}
                          href={getHrefByPath(
                            toString(get(menu, "menu.url", "/"))
                          )}
                          // shallow
                        >
                          <a>{get(menu, "menu.name")}</a>
                        </Link>
                      </h3>
                      <div className="sub-menu-item-list d-flex flex-column">
                        {Array.isArray(mChild) &&
                          mChild.map((m: any, mKey: any) => {
                            return (
                              <Link
                                as={toString(get(m, "menu.url", "/"))}
                                key={`sub-menu-item-list--${mKey}`}
                                href={getHrefByPath(
                                  toString(get(m, "menu.url", "/"))
                                )}
                                // shallow
                              >
                                <a>{get(m, "menu.name")}</a>
                              </Link>
                            );
                          })}
                      </div>
                    </div>
                  );
                })}
              </div>
            </Container>
          ) : (
            <div className="sub-menu-content d-flex justify-content-center">
              <div className="sub-menu-item">
                <div className="sub-menu-item-list d-flex flex-column">
                  {menuChild.map((menu: any, key: any) => {
                    return (
                      <Link
                        as={toString(get(menu, "menu.url", "/"))}
                        key={`sub-menu-item-list--${key}`}
                        href={getHrefByPath(
                          toString(get(menu, "menu.url", "/"))
                        )}
                        // shallow
                      >
                        <a>{get(menu, "menu.name")}</a>
                      </Link>
                    );
                  })}
                </div>
              </div>
            </div>
          )}
        </div>
      )}
    </li>
  );
}

HeaderBottom.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

export default withTranslation()(memo(HeaderBottom));
