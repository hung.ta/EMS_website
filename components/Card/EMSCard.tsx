import React, { Fragment } from "react";
import Link from "next/link";
import { toString } from "lodash";

import "./EMSCard.module.css";
import ImageFallBack from "components/ImageFallBack";
import { getHrefByPath } from "utils/helper";

const EMSCard = ({
  imgSrc = "",
  title = "",
  href = "#",
  description = "",
  date = "",
  typeCate = "",
  seeMore = false,
}) => {
  return (
    <div className="ems-card wr-news-left text-center">
      <Link
        as={toString(href)}
        href={getHrefByPath(toString(href))}
        // shallow
      >
        <a>
          <ImageFallBack
            className="ems-card__img"
            src={imgSrc}
            defaultimg="/img/default1.png"
            alt={title}
          />
          <div className="ems-card__content text-left">
            <span className="ems-card__content--link">{title}</span>
            <p className="ems-card__content--description">{description}</p>
            <div className="ems-card__content--info">
              <div className="ems-card__content--info-left">
                <img src="/img/icon/calendar.png" alt="icon calendar" />
                <span>{date}</span>
              </div>
              <div className="ems-card__content--info-right">
                {seeMore ? (
                  <span>{seeMore}</span>
                ) : (
                  <Fragment>
                    <img src="/img/icon/tags.png" alt="icon tags" />
                    <span>{typeCate}</span>
                  </Fragment>
                )}
              </div>
            </div>
          </div>
        </a>
      </Link>
    </div>
  );
};

export default EMSCard;
