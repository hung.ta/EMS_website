import React from "react";
import Link from "next/link";
import "./ElectronicBillOfLanding.module.css";
import { withTranslation } from "i18n";

const ElectronicBillOfLanding = ({ t }) => {
  return (
    <div className="electronic-bill-of-landing">
      <Link href="https://bill.ems.com.vn/login">
        <a target="_blank">{t("create-electronic-bill-of-lading")}</a>
      </Link>
    </div>
  );
};

ElectronicBillOfLanding.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

export default withTranslation()(ElectronicBillOfLanding);
