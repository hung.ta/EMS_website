import React, { useEffect } from "react";
import { CX_GOOGLE_KEY } from "configs";

export const wrapper = (WrappedComponent: any) => (props: any) => {
  const {
    t,
    i18n: { language },
  } = props;

  useEffect(() => {
    var gcse = document.createElement("script");
    gcse.type = "text/javascript";
    gcse.async = true;
    gcse.src = "https://cse.google.com/cse.js?cx=" + CX_GOOGLE_KEY;
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(gcse, s);

    window.onload = function () {
      window.document.getElementById("gsc-i-id1") &&
        window.document
          .getElementById("gsc-i-id1")
          .setAttribute("placeholder", t("key-search"));
    };
  }, []);

  useEffect(() => {
    window.document.getElementById("gsc-i-id1") &&
      window.document
        .getElementById("gsc-i-id1")
        .setAttribute("placeholder", t("key-search"));
  }, [language]);

  return (
    <React.Fragment>
      <WrappedComponent {...props} />
    </React.Fragment>
  );
};

export default wrapper;
