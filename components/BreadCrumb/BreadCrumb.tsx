import React from "react";
import { Container } from "react-bootstrap";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import IconHome from "public/img/icon/ic-home.png";

import { withTranslation } from "i18n";
import "./BreadCrumb.module.css";
import LazyLoad from "react-lazyload";

const BreadCrumb = (props: any) => {
  return (
    <div
      style={props.style}
      className={`wrapper-breadcrumb ${props.bgwhite ? "bgwhite" : ""}`}
    >
      <Container fluid={"lg"}>
        <div className="d-flex align-items-center">
          <LazyLoad debounce={false} height="100%" width="auto">
            <img src={IconHome} alt="icon home" />
          </LazyLoad>
          <Breadcrumb>
            <Breadcrumb.Item href="/">
              {props.t("not-found-home-text")}
            </Breadcrumb.Item>
            {props.children}
          </Breadcrumb>
        </div>
      </Container>
    </div>
  );
};

BreadCrumb.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

export default withTranslation()(BreadCrumb);
