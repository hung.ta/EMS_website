import React from 'react';
import Table from 'react-bootstrap/Table';

const TableComponent = ({ data, fields, keys }) => {
    return (
        <Table responsive>
            <thead>
                <tr>
                    {fields.map(field => {
                        return <th>{field}</th>
                    })}
                </tr>
            </thead>
            <tbody>
                {Array.isArray(data) && data.map((item, index) => {
                    return (
                        <tr>
                            {keys.map(key => {
                                if (key === 'index') {
                                    return <td>{index + 1}</td>
                                } else {
                                    return <td>{item[key]}</td>
                                }
                            })}
                        </tr>
                    )
                })}
            </tbody>
        </Table>
    );
}

export default TableComponent;