import React, { useEffect, useState } from "react";
import { get } from "lodash";
import { compose, withProps } from "recompose";
import {
  withScriptjs,
  GoogleMap,
  withGoogleMap,
  Marker,
  InfoWindow
} from "react-google-maps";
import { API_GG_MAP } from "configs/index";

const Map = compose(
  withProps({
    googleMapURL: `https://maps.googleapis.com/maps/api/js?key=${API_GG_MAP}&v=3.exp&libraries=geometry,drawing,places`,
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `100%` }} />,
    mapElement: <div style={{ height: `100%`, minHeight: 500 }} />,
  }),
  withScriptjs,
  withGoogleMap
)((props: any) => {
  const convertStringToFloat = (str: any) => {
    return parseFloat(str);
  };
  const { listPositions, userLocation, indexOffice } = props;
  const [indexOfficeInfoWindow, setIndexOfficeInfoWindow] = useState(indexOffice);
  let googleRef = null;
  let googleMaps: any = (window as any).google;

  useEffect(() => {
    setIndexOfficeInfoWindow(indexOffice)
  }, [indexOffice])

  const handleChangeIndexShowInfoWindow = index => e => {
    setIndexOfficeInfoWindow(index)
  }
  const handleCloseIndexShowInfoWindow = () => {
    setIndexOfficeInfoWindow(null)
  }

  useEffect(() => {
    googleRef && fitBounds();
  }, [listPositions]);

  const fitBounds = () => {
    if (googleMaps && Array.isArray(listPositions) && listPositions.length > 0) {
      let bounds = new googleMaps.maps.LatLngBounds();
      listPositions.forEach(m => {
        const lat = parseFloat(get(m, `latitude`));
        const lng = parseFloat(get(m, `longitude`));
        if (lat && lng) {
          bounds.extend(new googleMaps.maps.LatLng(lat, lng));
        }
      });

      googleRef.fitBounds(bounds);
    }
  };

  return (
    <GoogleMap
      ref={(ref) => (googleRef = ref)}
      defaultZoom={13}
      defaultCenter={userLocation}
    >
      <Marker
        key={'00'}
        icon={
          {
            path: googleMaps.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
            fillColor: '#F7941E',
            strokeColor: '#F7941E',
            scale: 5,
          }
        }
        position={userLocation}
      />
      {Array.isArray(listPositions) && listPositions.map((item: any, index: any) => (
        <Marker
          onClick={handleChangeIndexShowInfoWindow(index)}
          key={index}
          position={{
            lat: convertStringToFloat(get(item, `latitude`)),
            lng: convertStringToFloat(get(item, `longitude`)),
          }}
        >
          {
            indexOfficeInfoWindow == item.code && <InfoWindow onCloseClick={handleCloseIndexShowInfoWindow}>
              <React.Fragment>
                <b>{get(item, 'name', '')}</b>
                <p>{get(item, 'address', '')}</p>
                <p>{get(item, 'phoneNumber', '')}</p>
                <p>{get(item, 'provinceName', '')}</p>
              </React.Fragment>
            </InfoWindow>
          }
        </Marker>
      ))}
    </GoogleMap>
  );
});

export default Map;
