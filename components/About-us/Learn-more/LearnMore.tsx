import React from "react";
import { Row } from "react-bootstrap";

import "./LearnMore.module.css";
import { withTranslation } from "i18n";

const LearnMore = ({ t, ...props }) => {
  return (
    <div className="wr-learn-more">
      <div className="title-learn-more text-uppercase">{t("learn-more")}</div>
      <Row>{props.children}</Row>
    </div>
  );
};

LearnMore.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

export default withTranslation()(LearnMore);
