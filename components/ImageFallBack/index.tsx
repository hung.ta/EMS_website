import React from "react";
import { useFallbackImageInSSR } from "hooks/useFallBackImage";

const ImageFallBack: any = (props: any) => {
  const fallbackImageProps = useFallbackImageInSSR(props.defaultimg);

  return <img {...props} {...fallbackImageProps} />;
};

export default ImageFallBack;
