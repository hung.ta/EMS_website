import React, { memo, useEffect, useState } from "react";
import { Container, Row, Col } from "react-bootstrap";
import CountUp from "react-countup";
import { get, find, isEmpty } from "lodash";

import { withTranslation } from "../../../i18n";
import { FadeInSection } from "./FadeInSection";
import "./FastService.module.css";
import { swrGetTotalStatistics } from "services/home.service";

const FastService: any = ({ homeConfigData, t, custosmClass }) => {
  const [mounted, setMounted] = useState(false);
  const { data: totalStatisticsResData } = swrGetTotalStatistics(mounted);
  const title = get(homeConfigData, "titles[3].content", "");

  const findStatistic = (type: any) => {
    let data = get(totalStatisticsResData, "data") ?? [];

    if (isEmpty(data)) {
      return "";
    }
    let number = get(find(data, ["ISTYPE", type]), "STATISTICS", 0);
    let isBillionNumber = false;
    if (number >= 1000000000) {
      isBillionNumber = true;
      number = number / 1000000000;
    }
    return (
      <React.Fragment>
        {isBillionNumber && <span>&gt;</span>}{" "}
        <CountUp
          separator="."
          end={number}
          decimal=","
          decimals={isBillionNumber ? 2 : 0}
        />{" "}
        {isBillionNumber && <span>{t("billion")}</span>}
      </React.Fragment>
    );
  };

  useEffect(() => {
    setMounted(true);
  }, []);

  return (
    <div className={`fast-service ${custosmClass}`}>
      <Container fluid={"lg"}>
        <Row>
          <Col>
            <div className="fast-service-title text-center">
              <h2>{title}</h2>
            </div>
          </Col>
        </Row>
        <Row className="align-items-center">
          <Col md={7} lg={8} xs={12} className="mt-auto">
            <div className="fast-service-img">
              <img src="/img/fast-service.png" alt="fast service" />
            </div>
          </Col>
          <Col md={5} lg={4} xs={12}>
            <FadeInSection>
              <div className="fast-service-list">
                <div className="fast-service-item">
                  <div className="fast-service-item-icon">
                    <img src="/img/icon/user.png" alt="icon user" />
                  </div>
                  <div className="fast-service-item-content">
                    <div>{findStatistic(2)}</div>
                    <p>{t("fast-services-believe")}</p>
                  </div>
                </div>
                <div className="fast-service-item">
                  <div className="fast-service-item-icon">
                    <img src="/img/icon/network.png" alt="icon network" />
                  </div>
                  <div className="fast-service-item-content">
                    <div>{findStatistic(1)}</div>
                    <p>{t("fast-services-delivered")}</p>
                  </div>
                </div>
                <div className="fast-service-item">
                  <div className="fast-service-item-icon">
                    <img
                      src="/img/icon/postal-amount.png"
                      alt="icon postal amount"
                    />
                  </div>
                  <div className="fast-service-item-content">
                    <div>{findStatistic(3)}</div>
                    <p>{t("fast-services-number-of-offices")}</p>
                  </div>
                </div>
              </div>
            </FadeInSection>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

FastService.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

export default withTranslation()(memo(FastService));
