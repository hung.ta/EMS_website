import React, { useEffect, useState, useRef } from "react";

export const FadeInSection = (props) => {
  const [isVisible, setVisible] = useState(false);
  const domRef = useRef();

  useEffect(() => {
    if ('IntersectionObserver' in window) {
      const observer = new IntersectionObserver((entries) => {
        entries.forEach((entry) => setVisible(entry.isIntersecting));
      });
      observer.observe(domRef.current);
    } else {
      setVisible(true);
    }
  }, []);

  return (
    <div className={`fade-in-section`} ref={domRef}>
      {isVisible ? props.children : null}
    </div>
  );
};
