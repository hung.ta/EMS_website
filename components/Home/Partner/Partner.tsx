import React, { memo, useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import Slider from "react-slick";
import { get } from "lodash";

import { withTranslation } from "../../../i18n";
import { baseURL } from "./../../../configs/index";

import "./Partner.module.css";
import { swrGetLinkWebsite } from "services/home.service";

const Partner: any = ({ homeConfigData }) => {
  const [mounted, setMounted] = useState(false);
  const { data } = swrGetLinkWebsite(mounted);
  const title = get(homeConfigData, "titles[4].content", "");
  const partnerData = get(data, "data.response.data") ?? [];

  const settingSlide = {
    dots: true,
    infinite: Array.isArray(partnerData) && partnerData.length >= 5,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    initialSlide: 0,
    arrows: false,
    autoplay: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          initialSlide: 0,
          infinite: Array.isArray(partnerData) && partnerData.length >= 3,
          dots: true,
        },
      },
      {
        breakpoint: 350,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 0,
          infinite: true,
        },
      },
    ],
  };

  useEffect(() => {
    setMounted(true);
  }, []);

  return (
    <div className="partner">
      <Container fluid={"lg"}>
        <div className="partner-title text-center">
          <h2>{title}</h2>
        </div>
        <Slider {...settingSlide}>
          {Array.isArray(partnerData) &&
            partnerData.map((item, key) => (
              <div key={key}>
                <div className="partner-item" key={key}>
                  <a href={item.url} target="_blank">
                    <img
                      src={baseURL + item.mediaLink}
                      alt={item.name}
                      title={item.name}
                    />
                  </a>
                </div>
              </div>
            ))}
        </Slider>
      </Container>
    </div>
  );
};

Partner.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

export default withTranslation()(memo(Partner));
