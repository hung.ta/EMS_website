import React, { memo } from "react";
import Link from "next/link";
import { Container, Row, Col } from "react-bootstrap";
import { get, toString, size } from "lodash";

import ImageFallBack from "./../../ImageFallBack";
import { baseURL } from "./../../../configs/index";
import { withTranslation } from "../../../i18n";
import "./Service.module.css";
import { getHrefByPath } from "utils/helper";

const Service: any = ({ homeConfigData, t }) => {
  const services = get(homeConfigData, "services", []);
  const title = get(homeConfigData, "titles[0].content", "");

  return (
    <>
      {Array.isArray(services) && size(services) > 0 ? (
        <div className="service-stand-out">
          <div className="service-title">
            <h2>{title}</h2>
          </div>
          <Container fluid={"lg"}>
            <Row className="d-flex service-items">
              {Array.isArray(services) &&
                services.map((item, key) => {
                  return (
                    <Col md={6} key={key}>
                      <Link
                        as={toString(get(item, "url", "#"))}
                        href={getHrefByPath(toString(get(item, "url", "#")))}
                        // shallow
                      >
                        <div className="service-item d-flex align-items-center">
                          <div className="service-item-icon">
                            <ImageFallBack
                              src={baseURL + encodeURI(get(item, "icon", ""))}
                              alt={`img ${toString(get(item, "title", ""))}`}
                              defaultimg="/img/icon/truck.png"
                            />
                          </div>
                          <div className="service-item-content">
                            <h3>
                              <a>{get(item, "title", "")}</a>
                            </h3>
                            <p>{get(item, "description", "")}</p>
                          </div>
                        </div>
                      </Link>
                    </Col>
                  );
                })}
            </Row>
            <div className="text-center">
              <Link
                as="/dich-vu"
                href="/[...pages]"
                // shallow
              >
                <a className="service-btn">{t("other-services")}</a>
              </Link>
            </div>
          </Container>
        </div>
      ) : null}
    </>
  );
};

Service.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

export default withTranslation()(memo(Service));
