import React, { useRef, memo } from "react";
import Link from "next/link";
import { Container } from "react-bootstrap";
import Slider from "react-slick";
import { get, toString, size } from "lodash";
import moment from "moment";

import { baseURL } from "./../../../configs/index";
import { withTranslation } from "../../../i18n";
import EMSCard from "./../../Card/";
import "./News.module.css";

const News: any = ({ homeConfigData, t }) => {
  const posts = get(homeConfigData, "posts", []);
  const title = get(homeConfigData, "titles[2].content", "");
  const slideRef: any = useRef({});

  const handleNext = () => {
    slideRef.current.slickNext();
  };

  const handlePrevious = () => {
    slideRef.current.slickPrev();
  };

  const settingSlide = {
    dots: false,
    infinite: Array.isArray(posts) && posts.length >= 3,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    initialSlide: 0,
    autoplay: true,
    centerPadding: "50px",
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          initialSlide: 0,
          infinite: Array.isArray(posts) && posts.length >= 3,
        },
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 0,
          infinite: Array.isArray(posts) && posts.length >= 2,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 0,
          infinite: Array.isArray(posts) && posts.length >= 2,
        },
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 0,
        },
      },
    ],
  };

  return (
    <>
      {Array.isArray(posts) && size(posts) > 0 ? (
        <div className="news-stand-out">
          <div className="news-title">
            <h2>{title}</h2>
          </div>
          <Container fluid={"lg"}>
            <div className="wrap-slide">
              <Slider ref={slideRef} {...settingSlide}>
                {Array.isArray(posts) &&
                  posts.map((item, key) => (
                    <div key={key}>
                      <EMSCard
                        imgSrc={
                          baseURL +
                          encodeURI(toString(get(item, "mediaLinkWebsite", "")))
                        }
                        href={get(item, "url", "")}
                        title={get(item, "title", "")}
                        description={get(item, "description", "")}
                        typeCate={get(item, "categoryName", "")}
                        date={
                          moment(get(item, "createdDate", null)) &&
                          moment(get(item, "createdDate", null)).format(
                            "DD/MM/YYYY"
                          )
                        }
                        seeMore={t("see-more")}
                      />
                    </div>
                  ))}
              </Slider>
              {size(posts) > 3 && (
                <>
                  <div
                    className="next-btn d-none d-lg-flex"
                    onClick={handleNext}
                  >
                    <img src="/img/icon/next.png" alt="" />
                  </div>
                  <div
                    className="prev-btn d-none d-lg-flex"
                    onClick={handlePrevious}
                  >
                    <img src="/img/icon/prev.png" alt="" />
                  </div>
                </>
              )}
            </div>
          </Container>
          <div className="text-center">
            <Link
              as="/tin-tuc"
              href="/[...pages]"
              // shallow
            >
              <a className="news-btn">{t("full-news")}</a>
            </Link>
          </div>
        </div>
      ) : null}
    </>
  );
};

News.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

export default withTranslation()(memo(News));
