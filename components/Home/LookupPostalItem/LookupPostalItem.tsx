import React, { useRef, useState } from "react";
import { Container, Row, Col } from "react-bootstrap";

import { withTranslation } from "../../../i18n";
import "./LookupPostalItem.module.css";
import Link from "next/link";
import { isEmpty } from "lodash";
import { useRouter } from "next/router";

const LookupPostalItem = ({ t, i18n: { language } }) => {
  const router = useRouter();
  const [codePostOffice, setCodePostOffice] = useState("");
  const refPostOffice = useRef(null);
  const urlPostOffice = `/tra-cuu/tra-cuu-buu-gui?code=${codePostOffice}`;

  const handleSearch = () => {
    if (isEmpty(codePostOffice)) {
      refPostOffice.current.focus();
    } else {
      router.push(urlPostOffice);
    }
  };

  return (
    <div className="lookup-postal">
      <Container fluid={"md"}>
        <Row>
          <Col md={5} xs={12}>
            <div className="lookup-postal-item-action">
              <h3>{t("search-post")}</h3>
              <p>{t("enter-code")}</p>
              <div className="d-flex">
                <input
                  type="text"
                  placeholder={t("placeholder-code")}
                  value={codePostOffice}
                  onChange={(e) => {
                    setCodePostOffice(e.target.value);
                  }}
                  ref={refPostOffice}
                />

                <div
                  className="btn-search d-flex justify-content-center align-items-center"
                  onClick={handleSearch}
                >
                  <img
                    src="/img/icon/search-white.png"
                    alt="icon search white"
                  />
                </div>
              </div>
            </div>
          </Col>
          <Col
            className={`d-flex flex-column ${
              language === "vn"
                ? "justify-content-end"
                : "justify-content-start"
            } hover-block`}
          >
            <Link
              as="/tra-cuu/uoc-tinh-cuoc"
              href="/tra-cuu/uoc-tinh-cuoc"
              // shallow
            >
              <a>
                <div className="lookup-postal-item">
                  <div className="amount">
                    <img src="/img/icon/estimate.png" alt="icon amount" />
                  </div>
                  <p>{t("calculate-postage")}</p>
                </div>
              </a>
            </Link>
          </Col>
          <Col
            className={`d-flex flex-column ${
              language === "vn"
                ? "justify-content-end"
                : "justify-content-start"
            } hover-block`}
          >
            <Link
              as="/tra-cuu/uoc-tinh-cuoc"
              href="/tra-cuu/tra-cuu-buu-cuc"
              // shallow
            >
              <a>
                <div className="lookup-postal-item">
                  <div className="search-postal">
                    <img
                      src="/img/icon/search-post.png"
                      alt="icon search postal"
                    />
                  </div>
                  <p>{t("search-post-office")}</p>
                </div>
              </a>
            </Link>
          </Col>
          <Col
            className={`d-flex flex-column ${
              language === "vn"
                ? "justify-content-end"
                : "justify-content-start"
            } hover-block`}
          >
            <Link
              as="/thong-tin-gui-hang-quoc-te"
              href="/thong-tin-gui-hang-quoc-te"
              // shallow
            >
              <a>
                <div className="lookup-postal-item">
                  <div className="scan">
                    <img
                      src="/img/icon/search-prohibited.png"
                      alt="icon scan"
                    />
                  </div>
                  <p>{t("search-prohibited")}</p>
                </div>
              </a>
            </Link>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

LookupPostalItem.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

export default withTranslation()(LookupPostalItem);
