import React from "react";
import { Container } from "react-bootstrap";

import { get } from "lodash";
import { withTranslation } from "../../../i18n";
import "./DownloadEMS.module.css";


const DownloadEMS: any = ({ homeConfigData, t }) => {
  const title = get(homeConfigData, "titles[1].content", "");
  return (
    <div
      className="download-ems"
      style={{ backgroundImage: `url('/img/download-bg.png')` }}
    >
      <Container fluid={"lg"}>
        <div className="download-ems-content">
          <h2>{title}</h2>
          <div className="download-ems-content-text">
            <p>{t("convenient")}</p>
            <p>{t("features")}</p>
            <p>{t("create-check")}</p>
          </div>
          <div className="download-ems-content-img">
            <a href={process.env.APP_STORE} target="_blank">
              <img src="/img/app-store.png" alt="app store" />
            </a>
            <a href={process.env.GG_PLAY} target="_blank">
              <img src="/img/google-play.png" alt="google play" />
            </a>
          </div>
          <div className="download-ems-content-phone">
            <img src="/img/mobile.png" alt="mobile" />
          </div>
        </div>
      </Container>
    </div>
  );
};

DownloadEMS.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

export default withTranslation()(DownloadEMS);
