import Head from "next/head";
import { DOMAIN_NAME, baseURL, img_facebook_default } from "configs/index";
import Iframe from "react-iframe";

export const defaultDescription =
  "EMS - Công ty giao nhận đầu tiên tại Việt Nam được thành lập với sứ mệnh phục vụ nhu cầu vận chuyển chuyên nghiệp của các đối tác Thương mại điện tử trên toàn quốc";

export const defaultTitle = "EMS - Tổng công ty chuyển phát nhanh bưu điện";
const defaultKeywords =
  "EMS,ems,Chuyển phát nhanh,CPN,CHUYỂN PHÁT NHANH,Bưu chính,bưu phẩm,bưu kiện,hàng hóa,vnpost,bưu điện,buu dien,thư nhanh,thư,bưu thiếp,buu thiep,chuyen phat nhanh";

// 5 props: title, description, keywords, facebook_image, url
const Meta = (props) => {
  let titleData = props.title ? props.title : defaultTitle;
  let seoTitleData = props.seoTitle ? props.seoTitle : titleData;

  const fbURL = props.facebook_image
    ? encodeURI(`${baseURL}${props.facebook_image}`)
    : img_facebook_default;

  const URL = props.url ? encodeURI(`${DOMAIN_NAME}${props.url}`) : DOMAIN_NAME;

  const description = props.description
    ? props.description
    : defaultDescription;

  const keywords = props.keywords ? props.keywords : defaultKeywords;
  const type = props.type ? props.type : "website";
  const GTM = `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-MXXHFB3');`;

  return (
    <>
      <Head>
        <script>{GTM}</script>
        <title>{seoTitleData}</title>
        <meta name="title" content={seoTitleData} />
        <meta name="description" content={description} />
        <meta name="keywords" content={keywords} />
        <meta property="og:type" content={type} />
        <meta property="og:url" content={URL} />
        <meta property="og:title" content={seoTitleData} />
        <meta property="og:description" content={description} />
        <meta property="og:image" content={fbURL} />
        <meta property="og:image:width" content="1200" />
        <meta property="og:image:height" content="628" />
        <meta property="twitter:card" content={fbURL} />
        <meta property="twitter:url" content={URL} />
        <meta property="twitter:title" content={seoTitleData} />
        <meta property="twitter:description" content={description} />
        <meta property="twitter:image" content={fbURL} />
        <link rel="canonical" href={URL} />
      </Head>
      <noscript>
        <Iframe
          url="https://www.googletagmanager.com/ns.html?id=GTM-MXXHFB3"
          width="0"
          height="0"
          id="myId"
          display="none"
          position="relative"
        />
      </noscript>
    </>
  );
};

export default Meta;
