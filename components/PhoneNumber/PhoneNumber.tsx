import React, { useEffect, useState } from "react";
import "./PhoneNumber.module.css";
import imgPhone from "public/img/icon/phone-footer.png";

function PhoneNumber() {
  const [isPhoneNumber, setIsPhoneNumber] = useState(false);
  const [isMobile, setIsMobile] = useState(false);

  const handleWindowSizeChange = () => {
    if (window.innerWidth <= 768) {
      setIsMobile(true);
    } else {
      setIsMobile(false);
    }
  };

  const addActiveClass = () => {
    setIsPhoneNumber(!isPhoneNumber);
  };
  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);

  return (
    <>
      {isMobile ? (
        <div className="phone-number" id="phone_number">
          <a href="tel:1900545433" title="1900 54 54 33">
            <img src={imgPhone} alt="icon phone footer" />
          </a>
        </div>
      ) : (
        <div
          className={`phone-number ${isPhoneNumber ? "phone-active" : ""}`}
          onClick={addActiveClass}
          id="phone_number"
        >
          {isPhoneNumber ? (
            <div>
              <span>1900 54 54 33</span>
            </div>
          ) : (
            <img src={imgPhone} alt="icon phone footer" />
          )}
        </div>
      )}
    </>
  );
}

export default PhoneNumber;
