import React from "react";
import { get, toString } from "lodash";
import Slider from "react-slick";

import { baseURL } from "./../../configs/index";
import { withTranslation } from "./../../i18n";

import "./BannerMain.module.css";
import Link from "next/link";
import { getHrefByPath } from "utils/helper";

const BannerMain: any = ({ listBanners = [] }) => {
  const slideSettings = {
    dots: true,
    fade: true,
    autoplay: true,
    infinite: true,
    speed: 1000,
    autoplaySpeed: 3000,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  let sliderRef: any;

  if (Array.isArray(listBanners) && listBanners.length === 0) {
    return (
      <div className="main-banner">
        <img
          className="d-block w-100"
          src="/img/banner_default.jpg"
          alt=""
          style={{ objectFit: "cover" }}
        />
      </div>
    );
  }

  return (
    <div className="main-banner">
      <Slider ref={(slider: any) => (sliderRef = slider)} {...slideSettings}>
        {Array.isArray(listBanners) &&
          listBanners.map((item, key) => {
            let checkUrl = item.banner.url;
            return checkUrl === null || checkUrl === "" ? (
              <div className="main-banner__slider-item" key={key}>
                <img
                  src={baseURL + get(item, "banner.mediaLink")}
                  alt={get(item, "banner.name")}
                  className="d-block w-100"
                />
              </div>
            ) : (
              <Link
                as={toString(get(item, "banner.url", ""))}
                href={getHrefByPath(toString(get(item, "banner.url", "")))}
                key={key}
                shallow
              >
                <a target={get(item, "banner.url") ? "_blank" : ""}>
                  <div className="main-banner__slider-item">
                    <img
                      src={baseURL + get(item, "banner.mediaLink")}
                      alt={get(item, "banner.name")}
                      className="d-block w-100"
                    />
                  </div>
                </a>
              </Link>
            );
          })}
      </Slider>
    </div>
  );
};

BannerMain.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

export default withTranslation()(BannerMain);
