import React, { Fragment, useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import { get } from "lodash";
import { useRouter } from "next/router";

import { useAppContext } from "contexts/app";
import { baseURL, defaultBannerImgUrl } from "configs";
import "./BannerSub.module.css";
import { INFO_REGISTER_PATH } from "configs/constants";

const BannerSub = (props: any) => {
  let valueTitle = document.querySelector(".breadcrumb-item.active");
  const checkTitle = get(
    props.pageProps,
    "dataProps.pageInfo.Service.name",
    ""
  );
  const checkTitle1 = get(props.pageProps, "dataProps.pageInfo.Page.name", "");

  const [showBanner, setShowBanner] = useState(false);
  const router = useRouter();
  const appContext = useAppContext();
  const menuInfo = get(appContext, "menuInfo", {});
  const asPath = get(router, "asPath", "");
  const [documentTitle, setDocumentTitle] = useState(
    valueTitle ? valueTitle.textContent : checkTitle ? checkTitle : checkTitle1
  );
  let currentMenuObject = null;
  findDeepCurrentMenu(menuInfo);

  useEffect(() => {
    // service-single .breadcrumb-item.active
    if (valueTitle) {
      setDocumentTitle(valueTitle.textContent);
    }
  });

  function findDeepCurrentMenu(menuInfo: any) {
    if (currentMenuObject) return;

    if (!Array.isArray(menuInfo) || menuInfo.length == 0) return;

    menuInfo.forEach((item) => {
      const menu = get(item, "menu");
      const menuUrl = get(item, "menu.url");
      const menuChild = get(item, "menuChild");

      if (menuUrl === asPath) {
        currentMenuObject = menu;
        return false;
      }
      findDeepCurrentMenu(menuChild);
    });

    return false;
  }

  const imgBanner = get(currentMenuObject, "subBanner");
  const titleBanner = get(currentMenuObject, "name");

  const checkImageDeleted = async () => {
    if (imgBanner) {
      setShowBanner(false);
      let linkClearcache = `${baseURL}${imgBanner}?clearcache=${String(
        Math.random()
      )}`;
      fetch(linkClearcache, { method: "HEAD" })
        .then((res) => {
          if (res.status !== 404) {
            setShowBanner(true);
          }
        })
        .catch(() => {
          setShowBanner(true);
        });
    }
  };

  useEffect(() => {
    checkImageDeleted();
  }, [router.route, router.pathname]);

  if (
    router.pathname === INFO_REGISTER_PATH &&
    router.route === INFO_REGISTER_PATH
  ) {
    return (
      <div className="sub-banner sub-banner_info_register">
        <img
          className="img_banner_info_register"
          src="/img/info-register/new_banner.jpg"
          alt="EMS Banner"
        />
        <div className="sub-banner-title">
          <Container>
            <span className="text-uppercase">
              {titleBanner ? titleBanner : documentTitle}
            </span>
          </Container>
        </div>
      </div>
    );
  }

  const bannerDefault = (
    <div
      className={props.isServiceDetail ? "" : "sub-banner"}
      style={{ display: props.isServiceDetail ? "none" : "block" }}
    >
      <img src={defaultBannerImgUrl} alt="EMS Banner" />
      <div className="sub-banner-title">
        <Container>
          <span className="text-uppercase">
            {titleBanner ? titleBanner : documentTitle}
          </span>
        </Container>
      </div>
    </div>
  );

  if (!showBanner) {
    if (props.isServiceDetail) {
      return null;
    } else {
      return bannerDefault;
    }
  }

  return (
    <Fragment>
      {imgBanner ? (
        <div className="sub-banner">
          <img src={`${baseURL}${imgBanner}`} alt="" />
          <div className="sub-banner-title">
            <Container>
              <span className="text-uppercase">{titleBanner}</span>
            </Container>
          </div>
        </div>
      ) : (
        bannerDefault
      )}
    </Fragment>
  );
};

export default BannerSub;
