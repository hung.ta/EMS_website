import React, { useRef } from "react";
import "./RelatedServicesBlock.css";
import { Col, Row } from "react-bootstrap";
import Slider from "react-slick";

import "styles/Services.module.css";
import IconEclip from "public/img/icon/ic-eclip.png";
import IconNext from "public/img/icon/next.png";
import IconPrev from "public/img/icon/prev.png";
import { baseURL } from "configs/index";
import Link from "next/link";
import _ from "lodash";
import ImageFallBack from "components/ImageFallBack";
import { getHrefByPath } from "utils/helper";

function RelatedServicesBlock({ t, data }) {
  const slideRef: any = useRef({});
  const settingSlide = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    initialSlide: 0,
    autoplay: true,
    centerPadding: "50px",
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          initialSlide: 0,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 880,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 0,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 0,
        },
      },
    ],
  };

  const handleNext = () => {
    slideRef.current.slickNext();
  };

  const handlePrevious = () => {
    slideRef.current.slickPrev();
  };

  if (_.size(data) === 0) return null;

  let slideContent = (
    <div className="wrap-slide">
      <Slider ref={slideRef} {...settingSlide}>
        {Array.isArray(data) &&
          data.map((o: any) => {
            return (
              <div key={_.get(o, "serviceCategoryId", "")}>
                <Link
                  as={_.toString(_.get(o, "url", "#"))}
                  href={getHrefByPath(_.toString(_.get(o, "url", "#")))}
                  // shallow
                >
                  <div className="service-item1">
                    <div className="service-item-img1">
                      <div className="ic-eclip1">
                        <img src={IconEclip} alt="" />
                      </div>
                      <div className="service-item-icon1">
                        <ImageFallBack
                          src={baseURL + encodeURI(_.get(o, "icon", ""))}
                          alt=""
                          defaultimg="/img/icon/truck.png"
                        />
                      </div>
                    </div>
                    <div className="service-item-title1">
                      <h3>{o.title}</h3>
                    </div>
                    <div className="service-item-description1">
                      <p>{_.get(o, "description", "")}</p>
                    </div>
                  </div>
                </Link>
              </div>
            );
          })}
      </Slider>
      <div className="next-btn" onClick={handleNext}>
        <img src={IconNext} alt="" />
      </div>
      <div className="prev-btn" onClick={handlePrevious}>
        <img src={IconPrev} alt="" />
      </div>
    </div>
  );

  let notSlideContent = (
    <Row className="wr_non_slide">
      {Array.isArray(data) &&
        data.map((o: any) => {
          return (
            <Col xs={12} sm={4} key={_.get(o, "serviceCategoryId", "")}>
              <Link
                as={_.toString(_.get(o, "url", "#"))}
                href={getHrefByPath(_.toString(_.get(o, "url", "#")))}
                // shallow
              >
                <div className="service-item1">
                  <div className="service-item-img1">
                    <div className="ic-eclip1">
                      <img src={IconEclip} alt="" />
                    </div>
                    <div className="service-item-icon1">
                      <ImageFallBack
                        src={baseURL + encodeURI(_.get(o, "icon", ""))}
                        alt=""
                        defaultimg="/img/icon/truck.png"
                      />
                    </div>
                  </div>
                  <div className="service-item-title1">
                    <h3>{o.title}</h3>
                  </div>
                  <div className="service-item-description1">
                    <p>{_.get(o, "description", "")}</p>
                  </div>
                </div>
              </Link>
            </Col>
          );
        })}
    </Row>
  );

  return (
    <div className="related-service">
      <h2>{t("related-services")}</h2>
      <div className="related-service-content">
        {_.size(data) > 3 ? slideContent : notSlideContent}
      </div>
    </div>
  );
}

export default RelatedServicesBlock;
