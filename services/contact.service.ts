import { post as axiosPost } from './base.service';
import { get as axiosGet } from './base.service';

const contactUs = (body: any) => {
	return axiosPost('/api/contact/addcontact', body);
};

const getHeadQuarter = (langId: number) => {
	return axiosGet(`/headquarter-info?langId=${langId}&sortDir=ASC`);
};

export { contactUs, getHeadQuarter };
