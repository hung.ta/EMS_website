import { EMS_API_URL } from "configs";
import { IRegisterParams } from "interfaces/register.interface";
import { get as axiosGet } from "./base.service";

const registerInformation = async (params: IRegisterParams) => {
  return axiosGet(`${EMS_API_URL}/Customer`, { params });
};

export { registerInformation };
