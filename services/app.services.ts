import { get as axiosGet } from "./base.service";
import { useSWRAxios } from "./../configs/swr";

const getAllLanguages = () => {
  return axiosGet("/api/language/getalllanguage");
};

const getCompany = (langId: number) => {
  return axiosGet(`/api/company?langId=${langId}`);
};

const getMenuById = (langId: number) => {
  return axiosGet(`/api/menu/getmenubylangid?langId=${langId}`);
};

const getConfiguration = () => {
  return axiosGet(`/api/configuration/getallconfiguration?name=null`);
};

const swrGetConfiguration = (options = {}) => {
  return useSWRAxios(
    `/api/configuration/getallconfiguration?name=null`,
    options
  );
};

export {
  getAllLanguages,
  getConfiguration,
  getCompany,
  getMenuById,
  swrGetConfiguration,
};
