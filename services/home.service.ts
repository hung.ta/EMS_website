import { get as axiosGet } from "./base.service";
import { EMS_API_URL } from "configs/index";
import { useSWRAxios } from "configs/swr";

const getHomePageConfig = (langId: number) => {
  return axiosGet(`/home-page-configure?languageId=${langId}`);
};

const swrGetHomePageConfig = (
  condition: boolean,
  langId: number,
  options = {}
) => {
  return useSWRAxios(
    () =>
      condition && langId ? `/home-page-configure?languageId=${langId}` : null,
    options
  );
};

const swrGetTotalStatistics = (condition: boolean, options = {}) => {
  return useSWRAxios(
    () => (condition ? `${EMS_API_URL}/api/TotalStatistics` : null),
    options
  );
};

const swrGetLinkWebsite = (condition: boolean, options = {}) => {
  return useSWRAxios(
    () =>
      condition ? "/api/linkwebsite/getalllinkwebsite?queryString=null" : null,
    options
  );
};

const getAllBanner = (langCode: string) => {
  return axiosGet(`/api/banner/getbannerbylang?langCode=${langCode}`);
};

export {
  getHomePageConfig,
  swrGetHomePageConfig,
  swrGetTotalStatistics,
  swrGetLinkWebsite,
  getAllBanner,
};
