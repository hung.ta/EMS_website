import { get as axiosGet } from "./base.service";
import { useSWRAxios } from "./../configs/swr";

const getNavigationByUrl = (path: any, langCode: string) => {
  return axiosGet(`/api/navigation/getbyurl?url=${path}&langCode=${langCode}`);
};

const swrGetNavigationByUrl = (path: any, langCode: string, options = {}) => {
  return useSWRAxios(
    () =>
      !!langCode && !!path
        ? `/api/navigation/getbyurl?url=${path}&langCode=${langCode}`
        : null,
    options
  );
};

const getPostByCategoryId = (
  parentCategoryId: number,
  langId: number,
  pageSize?: number
) => {
  return axiosGet(
    `/api/post/getpostbycategoryid?categoryId=${parentCategoryId}&langId=${langId}&pageIndex=1&pageSize=${pageSize}`
  );
};

const swrGetPostByCategoryId = (
  condition: boolean,
  parentCategoryId: number,
  langId: number,
  pageSize?: number,
  options = {}
) => {
  return useSWRAxios(
    () =>
      condition && langId && parentCategoryId
        ? `/api/post/getpostbycategoryid?categoryId=${parentCategoryId}&langId=${langId}&pageIndex=1&pageSize=${pageSize}`
        : null,
    options
  );
};

const getPostChildByCategoryId = (
  currentCategoryId: number,
  langId: number
) => {
  return axiosGet(
    `/api/category/getpostchildbycategoryid?categoryId=${currentCategoryId}&langId=${langId}`
  );
};

const swrGetPostChildByCategoryId = (
  condition: boolean,
  currentCategoryId: number,
  langId: number,
  options = {}
) => {
  return useSWRAxios(
    () =>
      condition && langId
        ? `/api/category/getpostchildbycategoryid?categoryId=${currentCategoryId}&langId=${langId}`
        : null,
    options
  );
};

const getPostTop = (
  currentCategoryId: number,
  langId: number,
  isChild: boolean
) => {
  return axiosGet(
    `/api/post/getposttop?categoryId=${currentCategoryId}&langId=${langId}&isChild=${isChild}&pageIndex=1&pageSize=5`
  );
};

const swrGetPostTop = (
  condition: boolean,
  currentCategoryId: number,
  langId: number,
  isChild: boolean,
  options = {}
) => {
  return useSWRAxios(
    () =>
      condition && langId && currentCategoryId
        ? `/api/post/getposttop?categoryId=${currentCategoryId}&langId=${langId}&isChild=${isChild}&pageIndex=1&pageSize=5`
        : null,
    options
  );
};

const getCategoryByLangId = (currentCategoryId: number, langId: number) => {
  return axiosGet(
    `/api/category/getcategorybylangId?categoryId=${currentCategoryId}&langId=${langId}`
  );
};

const swrGetCategoryByLangId = (
  condition: boolean,
  currentCategoryId: number,
  langId: number,
  options = {}
) => {
  return useSWRAxios(
    () =>
      condition && langId && currentCategoryId
        ? `/api/category/getcategorybylangId?categoryId=${currentCategoryId}&langId=${langId}`
        : null,
    options
  );
};

const getServiceCategory = (langId: number) => {
  return axiosGet(
    `api/servicecategory/getservicecategorybylangId?langId=${langId}`
  );
};

const swrGetServiceCategory = (
  condition: boolean,
  langId: number,
  options = {}
) => {
  return useSWRAxios(
    () =>
      condition && langId
        ? `api/servicecategory/getservicecategorybylangId?langId=${langId}`
        : null,
    options
  );
};

const getRecruitment = (
  textSearch: string,
  langId: number,
  provinceCode?: number
) => {
  if (provinceCode) {
    return axiosGet(
      `/api/post/getallpost?queryString=${textSearch}&userName=null&langId=${langId}&isPosted=true&isRecruitment=true&categoryId=${provinceCode}&pageSize=1000&status=3`
    );
  } else {
    return axiosGet(
      `/api/post/getallpost?queryString=${textSearch}&userName=null&langId=${langId}&isPosted=true&isRecruitment=true&pageSize=1000&status=3`
    );
  }
};

const getServiceById = (langId: number, serviceId: number) => {
  return axiosGet(
    `/api/service?langId=${langId}&status=-1&serviceId=${serviceId}`
  );
};

const getServiceByCategoryId = (langId: number, serviceCategoryId: number) => {
  return axiosGet(
    `/api/service?langId=${langId}&status=-1&serviceCategoryId=${serviceCategoryId}&sortDir=ASC`
  );
};

export {
  getNavigationByUrl,
  swrGetNavigationByUrl,
  getPostByCategoryId,
  swrGetPostByCategoryId,
  getPostChildByCategoryId,
  swrGetPostChildByCategoryId,
  getPostTop,
  swrGetPostTop,
  getCategoryByLangId,
  swrGetCategoryByLangId,
  getServiceCategory,
  swrGetServiceCategory,
  getRecruitment,
  getServiceById,
  getServiceByCategoryId,
};
