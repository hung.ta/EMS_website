import { useSWRAxios, useSWRCustom } from "configs/swr";

const getAllShareHolder = (langId: number, year?: string, options = {}) => {
  const url = `/api/categoryShareHolder/get-category-share-holder-by-lang?langId=${langId}${
    year ? `&year=${year}` : ""
  }`;
  return useSWRAxios(() => (langId ? url : null), options);
};

const checkFileShareHolder = (pathFile, fetcher, options = {}) => {
  return useSWRCustom(pathFile, fetcher, options);
};

export { getAllShareHolder, checkFileShareHolder };
