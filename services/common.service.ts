import { useSWRAxios } from "./../configs/swr";

const fetchingMenuInfo = (langId, options = {}) =>
  useSWRAxios(
    () => (langId ? `/api/menu/getmenubylangid?langId=${langId}` : null),
    options
  );

const fetchingCompanyInfo = (langId, options = {}) =>
  useSWRAxios(() => (langId ? `/api/company?langId=${langId}` : null), options);

const fetchHeadQuarter = (langId, options = {}) =>
  useSWRAxios(
    () => (langId ? `/headquarter-info?langId=${langId}&sortDir=ASC` : null),
    options
  );

export { fetchingMenuInfo, fetchingCompanyInfo, fetchHeadQuarter };
