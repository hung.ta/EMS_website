// import axios from '../configs/axios';

import axios from "configs/axios";

const get = (path: string, params = {}) => {
  return axios.get(path, params);
};

const post = (path: string, body = {}) => {
  return axios.post(path, body);
};

const put = (path: string, params = {}) => {
  return axios.put(path, params);
};

export { get, post, put };
