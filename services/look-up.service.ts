import { get as axiosGet } from "./base.service";
import { EMS_API_URL } from "configs/index";
import { EstimateCharge } from "interfaces/look-up.interface";

const getInternationalPackage = (country: string, langId: number) => {
  return axiosGet(
    `/api/internationalpackage?queryString=${country}&langId=${langId}`
  );
};

const getProvinceList = () => {
  return axiosGet(`/api/Province/getallprovince?pageSize=1000`);
};

const getDistrictList = () => {
  return axiosGet(`/api/districtCategory?pageSize=1000`);
};

const getDistrictByProvince = (provinceCode: string) => {
  return axiosGet(
    `/api/districtCategory/getdistrictbyprovice?provinceCode=${provinceCode}&pageSize=1000`
  );
};

const getCommuneByDistrict = (districtCode: string) => {
  return axiosGet(
    `/api/commune?queryString=${districtCode}&pageIndex=1&pageSize=99999`
  );
};

const getAllPostOffice = (
  provinceCode: string,
  districtCode: string,
  communeCode: any
) => {
  return axiosGet(
    `/api/postoffice/getallpostoffice?queryString=null&provinceCode=${provinceCode}&communeCode=${communeCode}&districtCode=${districtCode}&pageSize=1000&pageIndex=1`
  );
};

const getPostalItemsByCode = (codeOffice: string, langId: number) => {
  return axiosGet(
    `${EMS_API_URL}/TrackAndTraceItemCode?itemcode=${codeOffice}&language=${langId}`
  );
};

const getAllCountryList = () => {
  return axiosGet(`/api/nationalCategory?pageIndex=1&pageSize=1000`);
};

const getEstimateCharge = (estimate: EstimateCharge) => {
  return axiosGet(
    `${EMS_API_URL}/EmsDosmetic?CountryCode=${estimate.countryCode}&FromProvince=${estimate.fromProvince}&FromDistrict=${estimate.fromDistrict}&ToProvince=${estimate.toProvince}&ToDistrict=${estimate.toDistrict}&weight=${estimate.weight}&totalAmount=${estimate.totalAmount}&Istype=${estimate.isType}&language=${estimate.language}`
  );
};

export {
  getInternationalPackage,
  getProvinceList,
  getDistrictByProvince,
  getCommuneByDistrict,
  getAllPostOffice,
  getPostalItemsByCode,
  getAllCountryList,
  getDistrictList,
  getEstimateCharge,
};
