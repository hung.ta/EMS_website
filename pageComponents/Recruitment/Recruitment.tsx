import React, { useEffect, useState, useRef } from "react";
import Link from "next/link";
import {
  Breadcrumb,
  Button,
  Col,
  Container,
  Form,
  Row,
  Spinner,
} from "react-bootstrap";
import Slider from "react-slick";
import { withTranslation } from "i18n";
import _, { uniq, toString } from "lodash";
import { Select } from "antd";

import { BreadCrumb } from "components";
import { getRecruitment } from "services/master-page.service";

import { useRouter } from "next/router";
import { formatDate, getHrefByPath } from "utils/helper";
import { baseURL } from "configs";
import iconMapBlack from "./../../public/img/icon/ic-map-black.png";
import iconMapWhite from "./../../public/img/icon/ic-map-white.png";
import iconMapMobile from "./../../public/img/icon/icon-map-mb.png";
import iconCalendarBlack from "./../../public/img/icon/ic-calendar-black.png";
import iconCalendarWhite from "./../../public/img/icon/ic-calendar-white.png";
import iconCalendarMobile from "./../../public/img/icon/icon-calendar-mb.png";
import iconRightOrange from "./../../public/img/icon/ic-arrow-orange.png";
import iconRightWhite from "./../../public/img/icon/ic-arrow-white.png";
import "./Recruitment.module.css";

const { Option } = Select;

const settingSlide = {
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  initialSlide: 0,
  autoplay: true,
  centerPadding: "50px",
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 0,
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 0,
      },
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 0,
      },
    },
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 0,
      },
    },
  ],
};

const Recruitment: any = ({ t, langId, pageInfo, listProvinces }) => {
  const wrRecruitment = useRef(null);
  const router = useRouter();
  const [recruitments, setRecruitments] = useState([]);
  const [provinceCode, setProvinceCode] = useState(null);
  const [errorForm, setErrorForm] = useState("");
  const [textSearch, setTextSearch] = useState("");
  const [listImage, setListImage] = useState([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  useEffect(() => {
    getAllRecruitment();
  }, [provinceCode, langId]);

  const getAllRecruitment = async () => {
    const res = await getRecruitment(textSearch, langId, provinceCode);
    setIsLoading(false);
    const { data } = res.data.response;
    setRecruitments(data);
    getListSlide(data);
    if (data.length <= 0) {
      setErrorForm(t("error-search-recruitment"));
    } else {
      setErrorForm("");
    }
  };

  const handleSubmitForm = async (e: any) => {
    e.preventDefault();
    setIsLoading(true);
    await getAllRecruitment();
    setIsLoading(false);
  };

  const handleOnChangeProvince = (value: any) => {
    setProvinceCode(value);
  };

  const getListSlide = (data: []) => {
    let newArr = [];
    let urlImg = `${baseURL}`;
    data.filter((item: any) => {
      if (item.mediaLinkFacebook !== null) {
        newArr.push(urlImg + item.mediaLinkFacebook);
      } else if (item.mediaLinkWebsite !== null) {
        newArr.push(urlImg + item.mediaLinkWebsite);
      }
    });
    setListImage(uniq(newArr));

    return newArr;
  };

  const handleRedirect = (href: any) => {
    const currentWidth = _.get(wrRecruitment, "current.clientWidth", 9999);
    if (currentWidth < 767) {
      router.push(href);
    }
  };

  return (
    <div className="wr-recruitment" ref={wrRecruitment}>
      <BreadCrumb>
        <Breadcrumb.Item active>
          <span>{t("recruitment")}</span>
        </Breadcrumb.Item>
      </BreadCrumb>
      <Container>
        <div className="content-recruitment">
          <div className="wr-search">
            <div className="title-search text-uppercase">{t("job-search")}</div>
            <Form
              className="form-search-recruitment"
              onSubmit={handleSubmitForm}
            >
              <Row>
                <Col xl={6} md={6} sm={12} lg={6} className="input-text">
                  <Form.Group>
                    <Form.Control
                      size="lg"
                      type="text"
                      placeholder={t("job-name")}
                      className="input-custom"
                      value={textSearch}
                      onChange={(e) => setTextSearch(e.target.value)}
                    />
                  </Form.Group>
                </Col>
                <Col xl={3} md={6} sm={12} lg={3} className="input-select">
                  <Form.Group>
                    <Select
                      virtual={false}
                      showSearch
                      style={{ width: "100%" }}
                      placeholder={t("choose-province-city")}
                      onChange={handleOnChangeProvince}
                      value={provinceCode}
                      optionFilterProp="children"
                      filterOption={(input, option) =>
                        option.children
                          .toLowerCase()
                          .indexOf(input.toLowerCase()) >= 0
                      }
                    >
                      <Option value={null}>{t("choose-province-city")}</Option>
                      {Array.isArray(listProvinces) &&
                        listProvinces.map((province) => {
                          return (
                            <Option
                              key={province.category.categoryId}
                              value={province.category.categoryId}
                            >
                              {province.category.name}
                            </Option>
                          );
                        })}
                    </Select>
                  </Form.Group>
                </Col>
                <Col
                  xl={3}
                  md={12}
                  sm={12}
                  lg={3}
                  className="btn-search-recruitment"
                >
                  <Button type="submit" className="btn btn-warning btn-submit">
                    {t("search-title-text")}{" "}
                    {isLoading && (
                      <Spinner animation="border" className="ml-3" />
                    )}
                  </Button>
                </Col>
              </Row>
            </Form>
            {errorForm && (
              <div className="error-recruitment">
                *{t("error-search-recruitment")}
              </div>
            )}
          </div>

          {recruitments && recruitments.length > 0 && (
            <div className="content-result-recruitment">
              <Row>
                <Col sm={12} md={12} xl={9}>
                  <div className="title-search text-uppercase">
                    {t("job-list")}
                  </div>
                  <div className="list-result-recruitment">
                    {/* 1 */}
                    {recruitments &&
                      recruitments.map((recruitment, i) => {
                        return (
                          <li
                            className="item-result d-flex justify-content-between align-items-center"
                            key={i}
                            onClick={() =>
                              handleRedirect(
                                _.toString(_.get(recruitment, "url", "#"))
                              )
                            }
                          >
                            <div className="name-job ">
                              <div className="title-job">
                                {recruitment.title}
                              </div>
                              <div className="address-date d-flex align-items-center">
                                <div className="address d-flex align-items-center">
                                  <img
                                    src={iconMapBlack}
                                    alt="icon map black"
                                    className="icon-black"
                                  />
                                  <img
                                    src={iconMapWhite}
                                    alt="icon map white"
                                    className="icon-white"
                                  />
                                  <img
                                    src={iconMapMobile}
                                    alt="icon map mobile"
                                    className="icon-mobile"
                                  />
                                  <span>{recruitment.categoryName}</span>
                                </div>
                                <div className="date d-flex align-items-center">
                                  <img
                                    src={iconCalendarBlack}
                                    alt="icon calendar black"
                                    className="icon-black"
                                  />
                                  <img
                                    src={iconCalendarWhite}
                                    alt="icon calendar white"
                                    className="icon-white"
                                  />
                                  <img
                                    src={iconCalendarMobile}
                                    alt="icon calendar"
                                    className="icon-mobile"
                                  />
                                  <span>
                                    {formatDate(recruitment.datePost)}
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div className="learn-more d-flex align-items-center">
                              <Link
                                href={getHrefByPath(
                                  toString(_.get(recruitment, "url", "#"))
                                )}
                                as={toString(_.get(recruitment, "url", "#"))}
                                // shallow
                              >
                                <a className="learn-more-text">
                                  {t("recruitment-read-more")}
                                </a>
                              </Link>
                              <a
                                href={toString(_.get(recruitment, "url", "#"))}
                                className="learn-more-icon"
                              >
                                <img
                                  src={iconRightOrange}
                                  alt="icon right orage"
                                  className="icon-black-learnmore"
                                />
                                <img
                                  src={iconRightWhite}
                                  alt="icon right white"
                                  className="icon-white-learnmore"
                                />
                              </a>
                            </div>
                          </li>
                        );
                      })}
                  </div>
                </Col>
                <Col xl={3} className="img-right-block">
                  <Slider {...settingSlide}>
                    {listImage &&
                      listImage.map((img: any, i: any) => {
                        return (
                          <div className="img-right-recruitment" key={i}>
                            <img src={img} alt="img right recruitment" />
                          </div>
                        );
                      })}
                  </Slider>
                </Col>
              </Row>
            </div>
          )}
        </div>
      </Container>
    </div>
  );
};

export default withTranslation()(Recruitment);
