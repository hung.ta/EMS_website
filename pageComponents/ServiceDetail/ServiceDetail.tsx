import React, { useEffect, useState } from "react";
import { withTranslation } from "./../../i18n";
import { Container } from "react-bootstrap";
import { get } from "lodash";
import { Breadcrumb } from "react-bootstrap";

import "./ServiceDetail.module.css";
import { BreadCrumb } from "./../../components";
import RelatedServicesBlock from "./../../components/RelatedServicesBlock";
import { baseURL, defaultBannerImgUrl } from "./../../configs";
import {
  getServiceById,
  getServiceByCategoryId,
} from "./../../services/master-page.service";

const BannerSubService = ({ pageInfo }) => {
  const [showBanner, setShowBanner] = useState(false);

  const checkImageDeleted = async () => {
    let link = `${baseURL}${get(pageInfo, "Service.mediaLinkAvatar") ?? ""}`;
    await fetch(link, { method: "HEAD" })
      .then((res) => {
        if (res.status === 404) {
        } else {
          setShowBanner(true);
        }
      })
      .catch(() => {
        setShowBanner(true);
      });
  };

  useEffect(() => {
    checkImageDeleted();
  }, []);

  // if (!showBanner) return null;
  let urlImg = `${baseURL}${get(pageInfo, "Service.mediaLinkAvatar") ?? ""}`;
  let valueTitle = document.querySelector(".breadcrumb-item.active");
  return (
    <div className="sub-banner">
      <img src={showBanner ? urlImg : defaultBannerImgUrl} alt="" />

      <div className="sub-banner-title">
        <Container>
          <span className="text-uppercase">
            {get(pageInfo, "Service.title") ?? valueTitle.textContent}
          </span>
        </Container>
      </div>
    </div>
  );
};

const ServiceDetail: any = ({ pageInfo, t }) => {
  const [listRelatedServices, setListRelatedServices] = useState([]);
  useEffect(() => {
    document.body.classList.add("bg_grey");
    let el = document.querySelector(".wrapper-breadcrumb");
    if (el) {
      el.classList.add("bg_grey");
    }

    let tables = Array.from(document.querySelectorAll("table"));
    tables.forEach((table) => {
      table.outerHTML = '<div class="wr_table" >' + table.outerHTML + "</div>";
    });

    return () => {
      document.body.classList.remove("bg_grey");
      let el = document.querySelector(".wrapper-breadcrumb");
      if (el) {
        el.classList.remove("bg_grey");
      }
    };
  }, []);

  const getServiceDetail = async (serviceId: number) => {
    const { id } = pageInfo.Language;
    const responseData = await getServiceById(id, serviceId);

    const { serviceCategoryId } = responseData.data.response.data[0];

    const responseData2 = await getServiceByCategoryId(id, serviceCategoryId);

    let newListRelatedServices = responseData2.data.response.data.filter(
      (o: any) => o.serviceId !== serviceId
    );

    setListRelatedServices(newListRelatedServices);
  };

  useEffect(() => {
    const { serviceId } = pageInfo.Service;
    getServiceDetail(serviceId);
  }, [pageInfo.Language.id]);

  return (
    <div className="service-single">
      <BannerSubService pageInfo={pageInfo} />
      <BreadCrumb>
        <Breadcrumb.Item href="/dich-vu">{t("service")}</Breadcrumb.Item>
        <Breadcrumb.Item active>
          {get(pageInfo, "Service.title")}
        </Breadcrumb.Item>
      </BreadCrumb>
      <Container fluid={"lg"}>
        {false && <IconEMS></IconEMS>}
        <div
          className="wr_service_detail"
          dangerouslySetInnerHTML={{ __html: get(pageInfo, "Service.content") }}
        ></div>
        <RelatedServicesBlock
          t={t}
          data={listRelatedServices}
        ></RelatedServicesBlock>
      </Container>
    </div>
  );
};

ServiceDetail.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});
export default withTranslation()(ServiceDetail);

const IconEMS = () => (
  <div className="icon_ems">
    <svg
      width="195"
      height="32"
      viewBox="0 0 195 32"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M24.777 31.7114L25.807 27.4512H21.4336L24.777 31.7114Z"
        fill="#F7941E"
      />
      <path
        d="M20.0151 25.6309H26.259L27.1381 21.9893H17.1562L20.0151 25.6309Z"
        fill="#F7941E"
      />
      <path
        d="M15.7222 20.1695H27.5716L28.454 16.5278H12.8633L15.7222 20.1695Z"
        fill="#F7941E"
      />
      <path
        d="M11.426 14.7027H28.8873L29.7697 11.061H8.57031L11.426 14.7027Z"
        fill="#F7941E"
      />
      <path
        d="M7.15186 9.2394H30.2219L31.1043 5.59424H4.29297L7.15186 9.2394Z"
        fill="#F7941E"
      />
      <path
        d="M0 0.132324L2.85889 3.77399H31.5376L32.4168 0.132324H0Z"
        fill="#F7941E"
      />
      <path
        d="M194.006 11.061H105.914L105.031 14.7027H194.006V11.061Z"
        fill="#F7941E"
      />
      <path
        d="M193.989 5.59668H107.218L106.336 9.24183H193.989V5.59668Z"
        fill="#F7941E"
      />
      <path
        d="M194.007 0.132324H108.558L107.676 3.77399H194.007V0.132324Z"
        fill="#F7941E"
      />
      <path
        d="M47.9194 19.3144L49.7901 11.5418H42.9589L43.8381 7.90012H50.6692L52.5142 0.23584H37.6037L30.0859 31.4661H44.9964L46.9601 23.3021H40.1289L41.0915 19.3144H47.9194Z"
        fill="#0072BC"
      />
      <path
        d="M72.459 0.23584L66.122 13.4046L65.8524 0.23584H56.3068L48.7891 31.4661H56.2074L60.6513 13.761L60.0834 31.4661H63.2246L71.583 13.775L67.3605 31.4661H74.3392L81.857 0.23584H72.459Z"
        fill="#0072BC"
      />
      <path
        d="M101.014 9.65455L102.345 4.11865L98.5109 0.23584H89.8957L84.6753 4.20952L81.8902 15.7811L85.3555 19.4787H88.516V19.4752H91.2305L90.2583 23.5257H87.5438L87.932 21.9076H80.4142L79.1211 27.2897L83.4913 31.4661H92.0584L96.7398 27.4051L99.496 15.9593L95.6488 12.0835H93.0113H90.2968L91.2722 8.03642H93.9868L93.5953 9.65455H101.014Z"
        fill="#0072BC"
      />
      <path
        d="M104.91 16.5615H107.894L109.178 28.6678L115.274 16.5615H118.354L110.05 31.4742H107.028L104.91 16.5615Z"
        fill="#0072BC"
      />
      <path
        d="M116.141 31.4847L120.35 16.5615H123.354L119.141 31.4847H116.141Z"
        fill="#0072BC"
      />
      <path
        d="M121.883 31.4847L125.858 16.5615H134.66L134.076 18.8367H128.101L127.084 22.6496H132.94L132.317 24.9597H126.458L125.339 29.206H131.589L130.947 31.4847H121.883Z"
        fill="#0072BC"
      />
      <path
        d="M136.706 31.4846L140.277 18.8367H135.801L136.475 16.5615H147.676L147.355 17.6939L147.053 18.8367H143.277L139.706 31.4846H136.706Z"
        fill="#0072BC"
      />
      <path
        d="M148.381 31.4845H145.711L149.802 16.5718H153.633L155.558 28.9472L158.638 16.5718H161.228L157.124 31.4845H153.684L151.65 18.7141L148.381 31.4845Z"
        fill="#0072BC"
      />
      <path
        d="M159.086 31.4844L168.246 16.5718H171.891L172.902 31.4949H169.995L169.87 28.2726H164.123L162.285 31.4949L159.086 31.4844ZM169.501 18.8155L165.426 25.9555H169.79L169.501 18.8155Z"
        fill="#0072BC"
      />
      <path
        d="M177.398 31.4845H174.719L178.986 16.5718H183.254V27.8463L189.241 16.5753H193.875L190.043 31.4984H187.075L190.907 18.1515L183.584 31.488H180.883V18.3821L177.398 31.4845Z"
        fill="#0072BC"
      />
    </svg>
  </div>
);
