import { withTranslation } from "./../../i18n";
import React, { Fragment, useState } from "react";
import { Col, Container, Row, Breadcrumb } from "react-bootstrap";
import Link from "next/link";
import { get, toString } from "lodash";
import moment from "moment";
import Slider from "react-slick";

import { baseURL } from "./../../configs/index";
import EMSCard from "./../../components/Card";
import { BreadCrumb, ImageFallBack } from "./../../components";
import "./News.module.css";
import { getHrefByPath } from "utils/helper";

const News: any = ({ pageInfo, categoryChildren, postsInTop, t }) => {
  const [slideTo, setSlideTo] = useState(0);
  const category = get(pageInfo, "Category", {});
  let sliderRef: any;
  const [isPlay, setIsPlay] = useState(true);

  const handleGoToSlide = async (index, e) => {
    !!sliderRef && sliderRef.slickGoTo(index);
    !!sliderRef && sliderRef.slickPause();
    setSlideTo(index);
    setIsPlay(false);
  };

  const handleOnLeave = () => {
    setIsPlay(true);
    !!sliderRef && sliderRef.slickPlay();
  };

  const slideSettings = {
    dots: false,
    fade: true,
    autoplay: isPlay,
    infinite: true,
    speed: 300,
    autoplaySpeed: 3000,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    beforeChange: (current, next) => setSlideTo(next),
  };

  return (
    <Fragment>
      <BreadCrumb style={{ marginTop: "20px" }} bgwhite={true}>
        <Breadcrumb.Item active>{t("news-page-title")}</Breadcrumb.Item>
      </BreadCrumb>
      <div className="news">
        <Container fluid={"lg"}>
          <Row className="news-top">
            <Col md={12} lg={8}>
              <Slider ref={(slider) => (sliderRef = slider)} {...slideSettings}>
                {Array.isArray(postsInTop) &&
                  postsInTop.map((item, index) => {
                    return (
                      <div className="wr-news-left" key={index}>
                        <Link
                          as={toString(get(item, "url", "#"))}
                          href={getHrefByPath(toString(get(item, "url", "#")))}
                          // shallow
                        >
                          <a>
                            <div className="news-left">
                              <div className="img-top">
                                <ImageFallBack
                                  src={
                                    baseURL +
                                    encodeURI(
                                      toString(
                                        get(item, "mediaLinkWebsite", "")
                                      )
                                    )
                                  }
                                  defaultimg="/img/default1.png"
                                  alt={get(item, "title", "")}
                                />
                              </div>
                              <div className="title-news">
                                <Link
                                  as={toString(get(item, "url", "#"))}
                                  href={getHrefByPath(
                                    toString(get(item, "url", "#"))
                                  )}
                                  // shallow
                                >
                                  <a className="title-news--links">
                                    {get(item, "title", "")}
                                  </a>
                                </Link>
                              </div>
                              <div className="des-news">
                                {get(item, "description")}
                              </div>
                              <div className="show-name d-flex justify-content-between align-items-center">
                                <div className="subtitle-news align-items-center d-flex">
                                  <img
                                    src="/img/icon/tags.png"
                                    alt="icon tag"
                                  />
                                  <span>{get(item, "categoryName")}</span>
                                </div>
                                <div className="date-news align-items-center d-flex">
                                  <img
                                    src="/img/icon/calendar.png"
                                    alt="icon calendar"
                                  />
                                  <span>
                                    {moment(get(item, "datePost")) &&
                                      moment(get(item, "datePost")).format(
                                        "DD/MM/YYYY"
                                      )}
                                  </span>
                                </div>
                              </div>
                            </div>
                          </a>
                        </Link>
                      </div>
                    );
                  })}
              </Slider>
            </Col>
            <Col md={12} lg={4}>
              <div className="news-right">
                <div className="title-news">{t("news-feature")}</div>
                <ul className="list-news-right">
                  {Array.isArray(postsInTop) &&
                    postsInTop.map((item, index) => {
                      return (
                        <li
                          className={`item-news wr-news-left ${
                            slideTo == index ? "slide-to" : ""
                          }`}
                          key={index}
                          title={get(item, "title")}
                        >
                          <Link
                            as={toString(get(item, "url", "#"))}
                            href={getHrefByPath(
                              toString(get(item, "url", "#"))
                            )}
                            // shallow
                          >
                            <a
                              className="d-flex"
                              onMouseEnter={handleGoToSlide.bind(this, index)}
                              onMouseLeave={handleOnLeave}
                            >
                              <div className="item-news_img">
                                <ImageFallBack
                                  src={
                                    baseURL +
                                    encodeURI(
                                      toString(
                                        get(item, "mediaLinkWebsite", "")
                                      )
                                    )
                                  }
                                  defaultimg="/img/default1.png"
                                  alt={get(item, "title")}
                                />
                              </div>
                              <div className="item-news_content">
                                <div className="item-news_title">
                                  <Link
                                    as={toString(get(item, "url", "#"))}
                                    href={getHrefByPath(
                                      toString(get(item, "url", "#"))
                                    )}
                                    // shallow
                                  >
                                    <a>{get(item, "title")}</a>
                                  </Link>
                                </div>
                                <div className="show-name d-flex justify-content-between align-items-center">
                                  <div className="subtitle-news">
                                    <img
                                      src="/img/icon/tags.png"
                                      alt="icon tag"
                                    />
                                    <span>{get(item, "categoryName")}</span>
                                  </div>
                                </div>
                              </div>
                            </a>
                          </Link>
                        </li>
                      );
                    })}
                </ul>
              </div>
            </Col>
          </Row>

          {categoryChildren &&
            categoryChildren.map((catChildItem, index) => {
              const posts = get(catChildItem, "posts", []);
              const catChild = get(catChildItem, "category", {});
              const key = get(catChild, "id");
              if ((get(catChildItem, "posts") ?? []).length === 0) {
                return null;
              }
              return (
                <Row className="wr-news-item" key={index}>
                  <Col md={12}>
                    <div className="item-name-news d-flex align-items-center justify-content-between">
                      <div className="title-child-item">
                        {get(catChild, "name")}
                      </div>
                      <div className="learn-more">
                        <Link
                          as={toString(get(catChild, "url", "#"))}
                          href={getHrefByPath(
                            toString(get(catChild, "url", "#"))
                          )}
                          // shallow
                        >
                          <a>{t("see-more")}</a>
                        </Link>
                      </div>
                    </div>
                    <Row>
                      {Array.isArray(posts) &&
                        posts.map((p, i) => {
                          if (i > 2) return;
                          return (
                            <Col
                              md={6}
                              lg={4}
                              key={`${key}-${i}`}
                              className="wr-news-item--card"
                            >
                              <EMSCard
                                imgSrc={
                                  baseURL +
                                  encodeURI(
                                    toString(get(p, "post.mediaLinkWebsite"))
                                  )
                                }
                                href={get(p, "post.url")}
                                title={get(p, "post.title")}
                                description={get(p, "post.description")}
                                typeCate={get(p, "post.categoryName")}
                                date={
                                  moment(get(p, "datePost")) &&
                                  moment(get(p, "datePost")).format(
                                    "DD/MM/YYYY"
                                  )
                                }
                              />
                            </Col>
                          );
                        })}
                    </Row>
                  </Col>
                  {Array.isArray(posts) && posts.length > 0 && (
                    <div className="text-center col-md-12">
                      <Link
                        as={toString(get(catChild, "url", "#"))}
                        href={getHrefByPath(
                          toString(get(catChild, "url", "#"))
                        )}
                        // shallow
                      >
                        <a className="wr-news-item__button-more">
                          {t("see-more")}
                        </a>
                      </Link>
                    </div>
                  )}
                </Row>
              );
            })}
        </Container>
      </div>
    </Fragment>
  );
};

News.getInitialProps = async () => {
  return {
    namespacesRequired: ["common"],
  };
};

export default withTranslation()(News);
