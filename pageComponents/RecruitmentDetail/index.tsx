import React from "react";
import { withTranslation } from "i18n";
import { Breadcrumb, Container } from "react-bootstrap";
import { get } from "lodash";

import { BreadCrumb } from "components";
import "./../Recruitment/Recruitment.module.css";

const RecruitmentDetail: any = ({ t, pageInfo }) => {
  return (
    <div className="recruitment-detail">
      <BreadCrumb>
        <Breadcrumb.Item href="/tuyen-dung">
          {t("recruitment-title")}
        </Breadcrumb.Item>
      </BreadCrumb>
      <Container>
        <div className="content-recruitment">
          <div
            dangerouslySetInnerHTML={{ __html: get(pageInfo, "Post.content") }}
          ></div>
          <div className="contact-recruitment">
            {t("recruitment-please") + " "}
            <a href="mailto:tuyendung@ems.com.vn">tuyendung@ems.com.vn</a>
          </div>
        </div>
      </Container>
    </div>
  );
};

export default withTranslation()(RecruitmentDetail);
