import React, { Fragment } from "react";
import { NextSeo } from "next-seo";
import { withTranslation } from "./../../i18n";

import "./NotFound.module.css";

const NotFound: any = ({ t }) => {
  return (
    <Fragment>
      <NextSeo
        title={t("not-found-title")}
        description={t("not-found-title")}
      />
      <div className="page-not-found">
        <img src="/img/not-found/lost.png" alt="lost png" />
        <h2>{t("not-found-title")}</h2>
        <p>
          {t("not-found-before-please-text") + " "}
          <a href="/">{t("not-found-home-text")}</a>
          {" " + t("not-found-after-please-text")}
        </p>
      </div>
    </Fragment>
  );
};

NotFound.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});
export default withTranslation()(NotFound);
