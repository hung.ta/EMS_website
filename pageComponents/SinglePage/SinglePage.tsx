import React, { Fragment } from "react";

import { withTranslation } from "./../../i18n";
import { Container, Breadcrumb } from "react-bootstrap";
import _, { get } from "lodash";

import { BreadCrumb, SeoMeta } from "./../../components";
import "./SinglePage.module.css";
import "./LearnMore.module.css";
import "./BlockImageText.module.css";
import "./BlockImageText_2.module.css";
import "./BlockIntroduce.module.css";

const SinglePage: any = ({ pageInfo }) => {
  return (
    <Fragment>
      <BreadCrumb bgwhite={true}>
        <Breadcrumb.Item active>{get(pageInfo, "Page.name")}</Breadcrumb.Item>
      </BreadCrumb>
      <div className="page-single">
        <Container fluid={"lg"}>
          <div
            dangerouslySetInnerHTML={{ __html: get(pageInfo, "Page.content") }}
          ></div>
        </Container>
      </div>
    </Fragment>
  );
};

SinglePage.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});
export default withTranslation()(SinglePage);
