import React, { Fragment } from "react";
import { NextSeo } from "next-seo";
import { withTranslation } from "./../../i18n";

import "./UnderConstruction.module.css";

const UnderConstruction: any = ({ t }) => {
  return (
    <Fragment>
      <NextSeo
        title={t("under-construction-title")}
        description={t("under-construction-title")}
      />
      <div className="page-under-construction">
        <img src="/img/not-found/under.png" alt="img under construction" />
        <h2>{t("under-construction-title")}</h2>
        <p>
          {t("under-construction-before-please-text") + " "}
          <a href="/">{t("under-construction-home-text")}</a>
          {" " + t("under-construction-after-please-text")}
        </p>
      </div>
    </Fragment>
  );
};

UnderConstruction.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});
export default withTranslation()(UnderConstruction);
