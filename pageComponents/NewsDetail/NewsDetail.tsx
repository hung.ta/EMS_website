import { withTranslation } from "i18n";
import React, { useRef, Fragment } from "react";
import { Breadcrumb, Container } from "react-bootstrap";
import _, { get, toString } from "lodash";
import moment from "moment";
import Slider from "react-slick";

import { BreadCrumb } from "components";
import EMSCard from "components/Card";
import { baseURL } from "configs/index";

import "./NewsDetail.module.css";

const NewsDetail: any = ({ relativePost, pageInfo, t }) => {
  const slideRef: any = useRef({});

  const handleNext = () => {
    slideRef.current.slickNext();
  };

  const handlePrevious = () => {
    slideRef.current.slickPrev();
  };

  const checkItemLength = (length: any) => {
    return (
      Array.isArray(relativePost) &&
      relativePost.filter((item) => item.postId != get(pageInfo, "Post.postId"))
        .length > length
    );
  };
  const settingSlide = {
    dots: false,
    infinite: checkItemLength(3),
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    initialSlide: 0,
    autoplay: true,
    centerPadding: "50px",
    slidesPerRow: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          initialSlide: 0,
          infinite: checkItemLength(3),
        },
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          initialSlide: 0,
          infinite: checkItemLength(2),
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          initialSlide: 0,
          infinite: checkItemLength(2),
        },
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 0,
          infinite: true,
        },
      },
    ],
  };

  return (
    <Fragment>
      <BreadCrumb style={{ marginTop: "20px" }} bgwhite={true}>
        <Breadcrumb.Item href="/tin-tuc">
          {t("news-page-title")}
        </Breadcrumb.Item>
        {_.get(pageInfo, "Post.category.url") && (
          <Breadcrumb.Item
            href={_.get(pageInfo, "Post.category.url") ?? "/tin-tuc"}
          >
            {_.get(pageInfo, "Post.category.name") ?? ""}
          </Breadcrumb.Item>
        )}

        <Breadcrumb.Item active>{get(pageInfo, "Post.title")}</Breadcrumb.Item>
      </BreadCrumb>
      <div className="news-detail">
        <Container fluid={"lg"}>
          <article className="news-detail-article">
            {/* <h2 className="news-detail__title">
              {get(pageInfo, "Post.title")}
            </h2> */}
            <div
              dangerouslySetInnerHTML={{
                __html: get(pageInfo, "Post.content"),
              }}
            ></div>
          </article>
          <div className="news-detail__info">
            <div className="news-detail__info--date-create">
              <img src="/img/icon/calendar.png" alt="icon calendar" />
              <span>
                {get(pageInfo, "Post.datePost") &&
                  moment(get(pageInfo, "Post.datePost")).format("DD/MM/YYYY")}
              </span>
            </div>
            <div className="news-detail__info--category">
              <img src="/img/icon/tags.png" alt="icon tag" />
              <span>{get(pageInfo, "Post.category.name") ?? ""}</span>
            </div>
          </div>
          <div className="news-relative">
            <h2>{t("news-relative")}</h2>
            <div className="news-detail__slider">
              <Slider ref={slideRef} {...settingSlide}>
                {Array.isArray(relativePost) &&
                  relativePost.map((item, key) => {
                    return (
                      item.postId != get(pageInfo, "Post.postId") && (
                        <div key={key}>
                          <EMSCard
                            imgSrc={
                              baseURL +
                              encodeURI(toString(get(item, "mediaLinkWebsite")))
                            }
                            href={get(item, "url")}
                            title={get(item, "title")}
                            description={get(item, "description")}
                            typeCate={get(item, "categoryName")}
                            date={
                              moment(get(item, "createdDate")) &&
                              moment(get(item, "createdDate")).format(
                                "DD/MM/YYYY"
                              )
                            }
                          />
                        </div>
                      )
                    );
                  })}
              </Slider>
              {Array.isArray(relativePost) &&
                relativePost.filter(
                  (item) => item.postId != get(pageInfo, "Post.postId")
                ).length > 3 && (
                  <Fragment>
                    <div
                      className="next-btn d-none d-lg-flex"
                      onClick={handleNext}
                    >
                      <img src="/img/icon/next.png" alt="" />
                    </div>
                    <div
                      className="prev-btn d-none d-lg-flex"
                      onClick={handlePrevious}
                    >
                      <img src="/img/icon/prev.png" alt="" />
                    </div>
                  </Fragment>
                )}
            </div>
          </div>
        </Container>
      </div>
    </Fragment>
  );
};

export const getInitialProps = async ({ req }) => {
  return {
    namespacesRequired: ["common"],
  };
};

export default withTranslation()(NewsDetail);
