import React, { useState, useEffect } from "react";
import Link from "next/link";
import { Col, Container, Accordion, Row, Breadcrumb } from "react-bootstrap";
import _ from "lodash";
import { withTranslation } from "i18n";

import {
  ContextAwareToggle,
  SeoMeta,
  BreadCrumb,
  ImageFallBack,
} from "./../../components";
import { FastService } from "../../components/Home";
import { baseURL } from "./../../configs/index";
import {
  removeVietnameseTones,
  getOnlyLetterStr,
  getHrefByPath,
} from "./../../utils/helper";
import IconEclip from "../../public/img/icon/ic-eclip.png";

import "styles/Services.module.css";

const MainService: any = ({ t, pageInfo, serviceGroupData }) => {
  const [currentEventKey, setCurrentEventKey] = useState(null);

  useEffect(() => {
    let _timeout = setTimeout(() => {
      const serviceUrl = _.get(pageInfo, "Service.url", "");
      scrollToServiceCategory(serviceUrl);
    }, 300);
    return () => {
      clearTimeout(_timeout);
    };
  }, [pageInfo?.Service?.url]);

  function scrollToServiceCategory(serviceUrl) {
    const idServiceUrl = getOnlyLetterStr(serviceUrl);
    const element = document.getElementById(idServiceUrl);
    element && element.scrollIntoView({ behavior: "smooth", block: "center" });
    setCurrentEventKey(idServiceUrl);
  }

  const handleChangeCurrentEventKey = (eventKey) => (e) => {
    let newEventKey = getOnlyLetterStr(eventKey);
    if (currentEventKey == newEventKey) {
      newEventKey = null;
    }
    setCurrentEventKey(newEventKey);
  };

  return (
    <React.Fragment>
      <BreadCrumb>
        <Breadcrumb.Item active>{t("service-page-title")}</Breadcrumb.Item>
      </BreadCrumb>
      <div className="main-service">
        {Array.isArray(serviceGroupData) &&
          serviceGroupData.map((serviceGroup, index) => {
            const isHasChild =
              _.size(_.get(serviceGroup, "categoryChild", [])) > 0;
            const serviceGroupCategoryChild = _.get(
              serviceGroup,
              "categoryChild",
              []
            );
            const serviceUrlParent = _.get(serviceGroup, "category.url", "");
            const serviceGroupPosts = _.get(serviceGroup, "posts", []);
            if (isHasChild) {
              const hasPosts = (_.get(serviceGroup, "posts") ?? []).length > 0;
              return (
                <div
                  id="wr_service_category-item"
                  key={_.get(serviceGroup, "category.serviceCategoryId")}
                  className="wr_service_2_level nhom_dvu_have_child"
                >
                  <Container>
                    <div className="wr_title_nhom_dich_vu text-center">
                      <h3
                        className="text-uppercase"
                        id={getOnlyLetterStr(serviceUrlParent)}
                      >
                        {_.get(serviceGroup, "category.name")}
                      </h3>
                    </div>

                    {/*{}*/}
                    <Row className="justify-content-start">
                      {Array.isArray(serviceGroupPosts) &&
                        serviceGroupPosts.map((post, postIndex) => {
                          if (_.get(post, "status") === 0) {
                            return null;
                          }
                          if (!_.get(post, "postDetail.title")) {
                            return null;
                          }
                          const postDetailUrl = _.toString(
                            _.get(post, "postDetail.url", "#")
                          );

                          return (
                            <Col
                              md={6}
                              lg={4}
                              key={`wr_content_nhom_dich_vu_${postIndex}`}
                            >
                              <Link
                                as={postDetailUrl}
                                href={getHrefByPath(postDetailUrl)}
                                // shallow
                              >
                                <div className="service-item">
                                  <div className="service-item-img">
                                    <div className="ic-eclip">
                                      <img src={IconEclip} alt="icon eclipse" />
                                    </div>
                                    <div className="service-item-icon">
                                      <ImageFallBack
                                        src={
                                          baseURL +
                                          encodeURI(
                                            _.get(post, "postDetail.icon", "")
                                          )
                                        }
                                        alt=""
                                        defaultimg="/img/icon/truck.png"
                                      />
                                    </div>
                                  </div>
                                  <div className="service-item-title">
                                    <h3>{_.get(post, "postDetail.title")}</h3>
                                  </div>
                                  <div className="service-item-description">
                                    <p>
                                      {_.get(
                                        post,
                                        "postDetail.description",
                                        ""
                                      )}
                                    </p>
                                  </div>
                                </div>
                              </Link>
                            </Col>
                          );
                        })}
                    </Row>
                    {/*{}*/}
                    <div className="wr_content_nhom_dich_vu">
                      <Accordion activeKey={currentEventKey}>
                        {Array.isArray(serviceGroupCategoryChild) &&
                          serviceGroupCategoryChild.map(
                            (service, serviceIndex) => {
                              const servicePosts =
                                _.get(service, "posts") ?? [];
                              const serviceUrl = _.get(
                                service,
                                "category.url",
                                ""
                              );
                              {
                                if (
                                  servicePosts.filter((sv) => sv.status !== 0)
                                    .length === 0
                                ) {
                                  return null;
                                }
                              }
                              return (
                                <div
                                  key={`wr_content_nhom_dich_vu-${serviceIndex}`}
                                >
                                  <ContextAwareToggle
                                    callback={handleChangeCurrentEventKey(
                                      serviceUrl
                                    )}
                                    eventKey={removeVietnameseTones(
                                      _.get(service, "category.name", "")
                                    )}
                                  >
                                    <div
                                      className="accordion_title"
                                      id={getOnlyLetterStr(serviceUrl)}
                                    >
                                      {_.get(service, "category.name", "")}
                                    </div>
                                  </ContextAwareToggle>
                                  <Accordion.Collapse
                                    eventKey={getOnlyLetterStr(serviceUrl)}
                                  >
                                    <Row className="justify-content-start">
                                      {Array.isArray(servicePosts) &&
                                        servicePosts.map((post, postIndex) => {
                                          if (_.get(post, "status") === 0) {
                                            return null;
                                          }
                                          if (
                                            !_.get(post, "postDetail.title")
                                          ) {
                                            return null;
                                          }
                                          const postDetailUrl = _.toString(
                                            _.get(post, "postDetail.url", "#")
                                          );

                                          return (
                                            <Col
                                              md={6}
                                              xs={12}
                                              sm={6}
                                              lg={4}
                                              className="justify-content-start-item"
                                              key={`justify-content-start-item-${postIndex}`}
                                            >
                                              <Link
                                                as={postDetailUrl}
                                                href={getHrefByPath(
                                                  postDetailUrl
                                                )}
                                                // shallow
                                              >
                                                <div className="service-item">
                                                  <div className="service-item-img">
                                                    <div className="service-item-icon">
                                                      <ImageFallBack
                                                        src={
                                                          baseURL +
                                                          encodeURI(
                                                            _.get(
                                                              post,
                                                              "postDetail.icon",
                                                              ""
                                                            )
                                                          )
                                                        }
                                                        alt=""
                                                        defaultimg="/img/icon/truck.png"
                                                      />
                                                    </div>
                                                    <div className="ic-eclip">
                                                      <img
                                                        src={IconEclip}
                                                        alt="icon eclipse"
                                                      />
                                                    </div>
                                                  </div>
                                                  <div className="service-item-title">
                                                    <h3>
                                                      {_.get(
                                                        post,
                                                        "postDetail.title",
                                                        ""
                                                      )}
                                                    </h3>
                                                  </div>
                                                  <div className="service-item-description">
                                                    <p>
                                                      {_.get(
                                                        post,
                                                        "postDetail.description",
                                                        ""
                                                      )}
                                                    </p>
                                                  </div>
                                                </div>
                                              </Link>
                                            </Col>
                                          );
                                        })}
                                    </Row>
                                  </Accordion.Collapse>
                                </div>
                              );
                            }
                          )}
                      </Accordion>
                    </div>
                  </Container>
                </div>
              );
            } else {
              if (
                (_.get(serviceGroup, "posts") ?? [{ status: 0 }]).filter(
                  (o) => o.status !== 0
                ).length === 0
              ) {
                return null;
              }

              return (
                <div
                  key={index}
                  id="wr_service_category-item"
                  className="wr_service_1_level nhom_dvu_not_have_child"
                >
                  <Container>
                    <div className="wr_title_nhom_dich_vu text-center">
                      <h3
                        className="text-uppercase"
                        id={getOnlyLetterStr(serviceUrlParent)}
                      >
                        {_.get(serviceGroup, "category.name")}
                      </h3>
                    </div>
                    <div className="wr_content_nhom_dich_vu">
                      <Row className="justify-content-start">
                        {Array.isArray(serviceGroupPosts) &&
                          serviceGroupPosts.map((post, postIndex) => {
                            if (_.get(post, "status") === 0) {
                              return null;
                            }
                            if (!_.get(post, "postDetail.title")) {
                              return null;
                            }
                            const postDetailUrl = _.toString(
                              _.get(post, "postDetail.url", "#")
                            );

                            return (
                              <Col
                                md={6}
                                lg={4}
                                key={`wr_content_nhom_dich_vu_${postIndex}`}
                              >
                                <Link
                                  as={postDetailUrl}
                                  href={getHrefByPath(postDetailUrl)}
                                  // shallow
                                >
                                  <div className="service-item">
                                    <div className="service-item-img">
                                      <div className="ic-eclip">
                                        <img
                                          src={IconEclip}
                                          alt="icon eclipse"
                                        />
                                      </div>
                                      <div className="service-item-icon">
                                        <ImageFallBack
                                          src={
                                            baseURL +
                                            encodeURI(
                                              _.get(post, "postDetail.icon", "")
                                            )
                                          }
                                          alt=""
                                          defaultimg="/img/icon/truck.png"
                                        />
                                      </div>
                                    </div>
                                    <div className="service-item-title">
                                      <h3>{_.get(post, "postDetail.title")}</h3>
                                    </div>
                                    <div className="service-item-description">
                                      <p>
                                        {_.get(
                                          post,
                                          "postDetail.description",
                                          ""
                                        )}
                                      </p>
                                    </div>
                                  </div>
                                </Link>
                              </Col>
                            );
                          })}
                      </Row>
                    </div>
                  </Container>
                </div>
              );
            }
          })}

        <FastService />
      </div>
    </React.Fragment>
  );
};

export default withTranslation()(MainService);
