import React, { Fragment, useState } from "react";
import Link from "next/link";
import { Breadcrumb, Col, Container, Row } from "react-bootstrap";
import { BreadCrumb, ImageFallBack } from "components";
import { withTranslation } from "./../../i18n";
import { get, toString } from "lodash";
import moment from "moment";
import Slider from "react-slick";

import EMSPagination from "./../../components/Pagination";
import EMSCard from "./../../components/Card";
import { get as axiosGet } from "./../../services/base.service";
import { baseURL } from "./../../configs/index";
import "./NewsSubCategory.module.css";
import { getHrefByPath } from "utils/helper";

const NewsSubCategory: any = ({
  pageInfo,
  pagingData,
  listPosts,
  postsInTop,
  mutateCategoryNews,
  t,
}) => {
  const [slideTo, setSlideTo] = useState(0);
  const category = get(pageInfo, "Category", {});
  let sliderRef: any;

  const [currentPage, setCurrentPage] = useState(1);
  const [isPlay, setIsPlay] = useState(true);

  const handleGoToSlide = (index, e) => {
    !!sliderRef && sliderRef.slickGoTo(index);
    !!sliderRef && sliderRef.slickPause();
    setSlideTo(index);
    setIsPlay(false);
  };

  const handleOnLeave = () => {
    setIsPlay(true);
    !!sliderRef && sliderRef.slickPlay();
  };

  const getListPost = async (page) => {
    const langId = get(pageInfo, "Language.id");
    const currentCategoryId = get(pageInfo, "Category.categoryId");
    const responseData = await axiosGet(
      `/api/post/getpostbycategoryid?categoryId=${currentCategoryId}&langId=${langId}&pageIndex=${page}&pageSize=9`
    );
    setCurrentPage(page);
    mutateCategoryNews({ ...responseData }, false);
  };

  const slideSettings = {
    dots: false,
    fade: true,
    autoplay: isPlay,
    infinite: true,
    speed: 300,
    autoplaySpeed: 3000,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    beforeChange: (current, next) => setSlideTo(next),
  };

  return (
    <Fragment>
      <BreadCrumb style={{ marginTop: "20px" }} bgwhite={true}>
        <Breadcrumb.Item href="/tin-tuc">
          {t("news-page-title")}
        </Breadcrumb.Item>
        <Breadcrumb.Item active>{get(category, "name")}</Breadcrumb.Item>
      </BreadCrumb>
      <div className="news-page">
        <Container fluid={"lg"}>
          <Row>
            <Col>
              <h1 className="news-page_title">{get(category, "name")}</h1>
            </Col>
          </Row>
          <Row className="news-page_top">
            <Col lg={8} className="news-top_page_news">
              <Slider ref={(slider) => (sliderRef = slider)} {...slideSettings}>
                {Array.isArray(postsInTop) &&
                  postsInTop.map((item, index) => {
                    return (
                      <div key={index} className="wr-news-left">
                        <Link
                          as={toString(get(item, "url", "#"))}
                          href={getHrefByPath(toString(get(item, "url", "#")))}
                          // shallow
                        >
                          <a>
                            <div className="img-top">
                              <ImageFallBack
                                src={
                                  baseURL +
                                  encodeURI(
                                    toString(get(item, "mediaLinkWebsite"))
                                  )
                                }
                                defaultimg="/img/default1.png"
                                alt={get(item, "title")}
                              />
                            </div>
                            <div className="news-page_item_title">
                              <Link
                                as={toString(get(item, "url", "#"))}
                                href={getHrefByPath(
                                  toString(get(item, "url", "#"))
                                )}
                                // shallow
                              >
                                <a>{get(item, "title")}</a>
                              </Link>
                            </div>
                            <div className="news-page_item_des">
                              {get(item, "description")}
                            </div>
                            <div className="d-flex item-news-highlight__info align-items-center justify-content-between">
                              <div className="news-page_item_name">
                                <img src="/img/icon/tags.png" alt="icon tag" />
                                <span>{get(item, "categoryName")}</span>
                              </div>
                              <div className="news-page_item_name news-date-create">
                                <img
                                  src="/img/icon/calendar.png"
                                  className="calender-img"
                                  alt="icon calendar"
                                />
                                <span>
                                  {moment(get(item, "datePost")) &&
                                    moment(get(item, "datePost")).format(
                                      "DD/MM/YYYY"
                                    )}
                                </span>
                              </div>
                            </div>
                          </a>
                        </Link>
                      </div>
                    );
                  })}
              </Slider>
            </Col>
            <Col md={12} lg={4}>
              <div className="news-page_highlight">
                <h3 className="page_highlight__title">{t("news-feature")}</h3>
                <ul className="list-news-highlight">
                  {Array.isArray(postsInTop) &&
                    postsInTop.map((post, index) => {
                      return (
                        <li
                          className={`item-news-highlight wr-news-left ${
                            slideTo == index ? "slide-to" : ""
                          }`}
                          key={index}
                          title={get(post, "title")}
                        >
                          <Link
                            as={toString(get(post, "url", "#"))}
                            href={getHrefByPath(
                              toString(get(post, "url", "#"))
                            )}
                            // shallow
                          >
                            <a
                              className="d-flex"
                              onMouseEnter={handleGoToSlide.bind(this, index)}
                              onMouseLeave={handleOnLeave}
                            >
                              <div className="item-news-highlight__img">
                                <ImageFallBack
                                  src={
                                    baseURL +
                                    encodeURI(
                                      toString(get(post, "mediaLinkWebsite"))
                                    )
                                  }
                                  defaultimg="/img/default1.png"
                                  alt={get(post, "title")}
                                />
                              </div>
                              <div className="item-news-highlight__content">
                                <div className="item-news-highlight__title">
                                  <Link
                                    as={toString(get(post, "url", "#"))}
                                    href={getHrefByPath(
                                      toString(get(post, "url", "#"))
                                    )}
                                    // shallow
                                  >
                                    <a>{get(post, "title")}</a>
                                  </Link>
                                </div>
                                <div className="d-flex justify-content-between align-items-center">
                                  <div className="news-page_item_name">
                                    <img
                                      src="/img/icon/tags.png"
                                      alt="icon tag"
                                    />
                                    <span>{get(post, "categoryName")}</span>
                                  </div>
                                </div>
                              </div>
                            </a>
                          </Link>
                        </li>
                      );
                    })}
                </ul>
              </div>
            </Col>
          </Row>

          {/* list news */}
          <Row className="list-news">
            {Array.isArray(listPosts) &&
              listPosts.map((post, index) => {
                return (
                  <Col md={6} sm={6} lg={4} key={index}>
                    <EMSCard
                      imgSrc={
                        baseURL +
                        encodeURI(toString(get(post, "mediaLinkWebsite")))
                      }
                      href={get(post, "url")}
                      title={get(post, "title")}
                      description={get(post, "description")}
                      typeCate={get(post, "categoryName")}
                      date={
                        moment(get(post, "datePost")) &&
                        moment(get(post, "datePost")).format("DD/MM/YYYY")
                      }
                    />
                  </Col>
                );
              })}
            <div className="text-center news-pagination">
              <EMSPagination
                currentPage={currentPage}
                totalPage={get(pagingData, "totalPage")}
                handleClickLink={getListPost}
              />
            </div>
          </Row>
        </Container>
      </div>
    </Fragment>
  );
};

NewsSubCategory.getInitialProps = async () => {
  return {
    namespacesRequired: ["common"],
  };
};

export default withTranslation()(NewsSubCategory);
